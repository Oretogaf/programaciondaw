/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplodocu;

/**
 *
 * @author Mati
 * @version 1.0
 */
public class Punto {
    private int x;/** Coordenada x del punto */
    private int y;/** Coordenada y del punto */
    /**
     * Constructor por defecto inicializa las variables
     * x e y con el valor 0
     */
    public Punto(){
        x=0;
        y=0;
    }
    /**
     * Constructor con argumentos
     * @param x coordenada x del punto
     * @param y coordenada y del punto
     */
    public Punto(int x, int y){
        this.x= x;
        this.y= y;
    }
    /**
     * Devuelve la coordenada x o y solicitada
     * @param coor coordenada a devolver
     * @return coordenada x o y
     * @deprecated esta función esta obsoleta y se eliminará
     * en próximas versiones
     * @see dameX
     * @see dameY
     */
    public int dameCoordenada (String coor){
        int salida = -1;
        if (coor.equals("x")){
            salida=x;
        }
        if (coor.equals("y")){
            salida=y;
        }
        return salida;
    }
    /**
     * Devuelve la coordenada x del punto
     * @return coordenada x del punto
     */
    public int dameX(){
        return x;
    }
    /**
     * Cambia el valor de la coordenada x
     * @param x nuevo valor de x
     */
    public void ponX(int x){
        this.x = x;
    }
    /**
     * Devuelve la coordenada y del punto
     * @return y coordenada y del punto
     */
    public int dameY(){
        return y;
    }
    /**
     * Cambia el valor de la coordenada y
     * @param y nuevo valor de y
     */
    public void ponY(int y){
        this.y = y;
    }
}