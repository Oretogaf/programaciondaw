/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_pruebas_junit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mati
 */
public class MatesTest {
    
    public MatesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of suma method, of class Mates.
     */
    @Test
    public void testSuma() {
        System.out.println("suma");
        double num1 = 10.0;
        double num2 = 5.0;
        Mates instance = new Mates();
        double expResult = 15.0;
        double result = instance.suma(num1, num2);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of resta method, of class Mates.
     */
    @Test
    public void testResta() {
        System.out.println("resta");
        double num1 = 20.0;
        double num2 = 10.0;
        Mates instance = new Mates();
        double expResult = 10.0;
        double result = instance.resta(num1, num2);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of producto method, of class Mates.
     */
    @Test
    public void testProducto() {
        System.out.println("producto");
        double num1 = 10.0;
        double num2 = 2.0;
        Mates instance = new Mates();
        double expResult = 30.0;
        double result = instance.producto(num1, num2);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of division method, of class Mates.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        double dividendo = 20.0;
        double divisor = 5.0;
        Mates instance = new Mates();
        double expResult = 4.0;
        double result = instance.division(dividendo, divisor);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of main method, of class Mates.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        Mates.main(args);
    }
    
}
