/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package task1_hangman;

import java.util.*;  // Random
import java.io.*;    // Lectura de datos - excepción

/*
 * Hangman.java
 */
/**
  * Class that executes the hangman game, that consist of trying to guess a word,
  * with a maximum number of errors.
  * @version 2.0
  * @author Eva
  */
public class Hangman {
        public static Scanner uin = new Scanner(System.in);
        
	//************************** CONSTANTS ****************************//
	/** Maximum number of errors allowed*/
	public static final int INUNMAXERR = 8;
	/** Possible words */
	public static final String[] ASWORDS = {"hello", "ugly", "monster","head"};
	//** I create a space to better visualize the doll
	public static final String AWHITESPACE = ("               ");
        
        public char[] userWord;
	
	/**
	 * Main method of input to our game.
	 * @param args
	 */
	public static void main(String[] args)throws IOException{
		//I define an object of my own class to call non-static methods.
            Hangman ahorcado = new Hangman();
                //Returns array of chars as word solution.
            char[] palabra = ahorcado.randomWordSearch();
                //Returns array of chars as user word.
            ahorcado.userWord = ahorcado.createUserWord(palabra);          
                //Show: welcome to the game.
            ahorcado.welcomeGameShow();
		//Initialize the number of errors and successes to zero.
            int numError = 0;
            int success = 0;
            int numSuccess = 0;
            char car;

            do{
                    //I show the word to the user.
                ahorcado.wordUserDisplay(ahorcado.userWord);
                    //I ask the letter to the user.
                car = ahorcado.requestLetter();
                    //I check if the letter is in the word solution and keep the number of successes in this round.
                numSuccess = ahorcado.comprobarLetra(car, palabra, ahorcado.userWord);
                
                    //Increase the number of successes in this round to the general successes of the game.
                    //If the successes in this round are zero.
                if(numSuccess == 0){
                            //Increase the number of general errors.
                    numError++;
                            //Show the number of oportunities.
                    System.out.println("Te quedan " + (INUNMAXERR - numError) + " oportunidades más");
                            //Show the picture of a hangman.
                    ahorcado.showDrawing(numError);
                }else{
                    success += numSuccess;
                }


            //It is repeated while remaining errors and the number of successes does not match the length of the solution.
            }while(numError < INUNMAXERR && success < palabra.length );

            //If we left it is because the number of successes matches the length of the solution.
            if(success == ahorcado.userWord.length){
                    //Show the congratulatory message.
                ahorcado.wordUserDisplay(ahorcado.userWord);
                ahorcado.showCongratulatoryMessage();
            }else{
                System.out.print("La palabra era: ");
                ahorcado.wordUserDisplay(palabra);
                ahorcado.showGoodbyeMessage();
            }
            System.exit(0);

            //We leave the game.
		
	}
        
	/**
	 * Returns an array of chars with a random word.
	 * @return char[] with the random word found.
	 */
	public char[] randomWordSearch(){
            //First I select a random number from 0 to ASWORDS-1
	    Random rnd = new Random ();
            int randomNum = rnd.nextInt(3 - 0 + 1) + 0;
            return ASWORDS[randomNum].toCharArray();
	}

	/**
	 * Returns a new array of chars with so many
	 * hyphens as letters have the input array.
	 * @param inacWord
	 * @return acWordUsu
	 */
	public char[] createUserWord(char[] inacWord){
            char[] a = new char[inacWord.length];
            for (int i = 0; i < inacWord.length; i++) {
                a[i] = '-';
            }
            return a;
        }
        
	/**
         * Method that checks whether the letter entered is in the array introduced.
         * If there is such a letter, changes in the user word the hiphen for the letter
         * in the position found.
	 * @param incLetter
	 * @param inacWordSolution
	 * @param inacWordUsu
	 * @return el número de letras acertadas.
	 */
	public int comprobarLetra(char incLetter,char[] inacWordSolution, char[] inacWordUsu){
            int iNumHits = 0;
		//I check that the letter does not exist in the word of the user.
            for (int i = 0; i < inacWordSolution.length; i++) {
                if(incLetter == inacWordUsu[i]){
                    iNumHits++;
                }
            }
            if(iNumHits != 0){
                System.out.println("Ya has introducido esta letra");
                return 0;
            }
		//If you have not found in the user's word, then we look in the solution.
            for (int i = 0; i < inacWordSolution.length; i++) {
                if(incLetter == inacWordSolution[i]){
                    iNumHits++;
                    inacWordUsu[i] = incLetter;
                }
            }
            if(iNumHits != 0){
                System.out.println("Existen " + iNumHits + " letras en la palabra");
                return iNumHits;
            }else{
                System.out.println("No existe esta letra en la palabra");
                return 0;
            }
	}

	/**
	 * Method shows welcome screen.
	 */
	public void welcomeGameShow(){
            System.out.println("Bienvenido al Ahorcado");
	}
        
	/**
	 * Method shows goodby screen.
	 */
	public void showGoodbyeMessage(){
            System.out.println("Adios. Vuelve pronto");
	}
        
	/**
	 * Solicita una letra al usuario
	 * @return char with the letter specified by the user.
	 */
	public char requestLetter()throws IOException{
            System.out.println("Introduce una letra: ");
            return uin.next().charAt(0);
	}
        
	/**
	 * Show the picture depending on the errors that come as a parameter
	 * @param iniNumErr
	 */
	public void showDrawing(int iniNumErr){
            switch(iniNumErr){
                case 1:
                    System.out.print(AWHITESPACE + "  ___\n");
                    break;
                case 2:
                    System.out.print(AWHITESPACE + "  ___\n" +
                            AWHITESPACE + " | \n");
                    break;
                case 3:
                    System.out.print(AWHITESPACE + "  ___\n" +
                            AWHITESPACE + " | \n" +
                            AWHITESPACE + " O \n");
                    break;
                case 4:
                    System.out.print(AWHITESPACE + "  ___\n" +
                            AWHITESPACE + " | \n" +
                            AWHITESPACE + " O \n" +
                            AWHITESPACE + "- \n");
                    break;
                case 5:
                    System.out.print(AWHITESPACE + "  ___\n" +
                            AWHITESPACE + " | \n" +
                            AWHITESPACE + " O \n" +
                            AWHITESPACE + "- -\n");
                    break;
                case 6:
                    System.out.print(AWHITESPACE + "  ___\n" +
                            AWHITESPACE + " | \n" +
                            AWHITESPACE + " O \n" +
                            AWHITESPACE + "- -\n" +
                            AWHITESPACE + " | \n");
                    break;
                case 7:
                    System.out.print(AWHITESPACE + "  ___\n" +
                            AWHITESPACE + " | \n" +
                            AWHITESPACE + " O \n" +
                            AWHITESPACE + "- -\n" +
                            AWHITESPACE + " | \n" +
                            AWHITESPACE + "/ \n");
                    break;
                case 8:
                    System.out.print(AWHITESPACE + "  ___\n" +
                            AWHITESPACE + " | \n" +
                            AWHITESPACE + " O \n" +
                            AWHITESPACE + "- -\n" +
                            AWHITESPACE + " | \n" +
                            AWHITESPACE + "/ \\\n");
                    break;
                default:
                    System.out.println("Fatal Error");
                    System.exit(0);
                    break;
            }
	}
        
	/**
	 * Shows the array of char introduced as input.
	 * @param inacharWord
	 */
	public void wordUserDisplay(char[] inacharWord){
            System.out.println(String.valueOf(inacharWord));
	}
        
	/**
	 * Shows the congratulatory message if you have completely matched.
	 */
	public void showCongratulatoryMessage(){
            System.out.println("Enhorabuena, has ganado. Te estaremos esperando");
	}


}
