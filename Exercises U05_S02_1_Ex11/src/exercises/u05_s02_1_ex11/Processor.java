/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s02_1_ex11;

/**
 *
 * @author Mati
 */
public class Processor {
    //Data processor
    private String processorModel;
    private int processorSpeed;

    public Processor(){
        
    }
    public Processor(Processor other){
        if(other == null){
            System.out.println("Fatal error");
            System.exit(0);
        }
        processorModel = other.processorModel;
        processorSpeed = other.processorSpeed;
    }
    public Processor(int proSpeed, String proModel){
        processorModel = proModel;
        processorSpeed = proSpeed;
    }
    
    public void setProcessorModel(String newModel)
    {
        processorModel = newModel;
    }
    public void setProcessorSpeed(int newSpeed)
    {
        processorSpeed = newSpeed;
    }

    public String getProcessorModel()
    {
        return processorModel;
    }
    public int getProcessorSpeed()
    {
        return processorSpeed;
    }
    @Override
    public String toString(){
        return ("Modelo procesador "+processorModel+
                "\nVelocidad del procesador "+processorSpeed);
    }
    public boolean equals(Processor other){
        return ((processorModel.equalsIgnoreCase(other.processorModel))&&
                (processorSpeed == other.processorSpeed));
    }
}
