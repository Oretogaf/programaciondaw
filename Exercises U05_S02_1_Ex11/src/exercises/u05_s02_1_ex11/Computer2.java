/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s02_1_ex11;

/**
 *
 * @author Mati
 */
public class Computer2 {
    private Memory memo;
    private HardDrive hduro;
    private Processor proc;
    public Computer2(){
        memo = new Memory();
        hduro = new HardDrive();
        proc = new Processor();
    }
    public Computer2(Computer2 otherCom){
        if(otherCom == null){
            System.out.println("Fatal error");
            System.exit(0);
        }
        memo = otherCom.memo;
        hduro = otherCom.hduro;
        proc = otherCom.proc;
    }
    public Computer2(Memory memory,HardDrive harddrive,Processor processor){
        memo = memory;
        hduro = harddrive;
        proc = processor;
    }
    public boolean equals(Memory othermemo, HardDrive otherhduro, Processor otherproc){
        return memo.equals(othermemo)&&hduro.equals(otherhduro)&&proc.equals(otherproc);
    }
    @Override
    public String toString(){
        return memo.toString()+""+hduro.toString()+""+proc.toString();
    }
    public void setMemo(int memSize, String memType, int memSpeed){
        memo = new Memory(memSize, memType, memSpeed);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Memory mem = new Memory(4,"ddr3",800);
        Memory mem1 = new Memory(mem);
        System.out.println(mem.equals(mem1));
        Memory mem2 = new Memory();
        mem2.setMemorySize(2);
        mem2.setMemorySpeed(1066);
        mem2.setMemoryType("ddr2");
        System.out.println(mem2.getMemorySize());
        System.out.println(mem2.getMemorySpeed());
        System.out.println(mem2.getMemoryType());
        System.out.println(mem1.toString());
        
        System.out.println("");
        
        HardDrive hdd = new HardDrive(500, 7200);
        HardDrive hdd1 = new HardDrive(hdd);
        System.out.println(hdd.equals(hdd1));
        HardDrive hdd2 = new HardDrive();
        hdd2.setHardDriveSize(1000);
        hdd2.setNumberRevolutionsDisk(5400);
        System.out.println(hdd2.getHardDriveSize());
        System.out.println(hdd2.getNumberRevolutionsDisk());
        System.out.println(hdd2.toString());
        
        System.out.println("");
        
        Processor pro = new Processor(3, "i5-4460");
        Processor pro1 = new Processor(pro);
        System.out.println(pro.equals(pro1));
        Processor pro2 = new Processor();
        pro2.setProcessorModel("AMD FX-6350");
        pro2.setProcessorSpeed(4);
        System.out.println(pro2.getProcessorModel());
        System.out.println(pro2.getProcessorSpeed());
        System.out.println(pro2.toString());
        
        Computer2 comp2 = new Computer2();
        
        Computer2 comp21 = new Computer2(mem2, hdd2, pro2);
        Computer2 comp22 = new Computer2(comp21);
    }
    
}
