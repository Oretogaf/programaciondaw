/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s02_1_ex11;

/**
 *
 * @author Mati
 */
public class HardDrive {
    //Details of the hard drive
    private int hardDriveSize;
    private int numberRevolutionsDisk;
    
    public HardDrive(){
        
    }
    public HardDrive(int hardSize, int hardSpeed){
        hardDriveSize = hardSize;
        numberRevolutionsDisk = hardSpeed;
    }
    public HardDrive(HardDrive other){
        if(other == null){
            System.out.println("Fatal error");
            System.exit(0);
        }
        hardDriveSize = other.hardDriveSize;
        numberRevolutionsDisk = other.numberRevolutionsDisk;
    }
    
    public void setHardDriveSize(int newSize)
    {
        hardDriveSize = newSize;
    }
    public void setNumberRevolutionsDisk (int newValue)
    {
        numberRevolutionsDisk = newValue;
    }
    
    public int getHardDriveSize()
    {
        return hardDriveSize;
    }
    public int getNumberRevolutionsDisk ()
    {
        return numberRevolutionsDisk;
    }
    @Override
    public String toString(){
        return ("Tamaño disco duro "+hardDriveSize+
                "\nRevoluciones disco duro "+numberRevolutionsDisk);
    }
    public boolean equals(HardDrive other){
        return ((hardDriveSize == other.hardDriveSize)&&
                (numberRevolutionsDisk == other.numberRevolutionsDisk));
    }
}
