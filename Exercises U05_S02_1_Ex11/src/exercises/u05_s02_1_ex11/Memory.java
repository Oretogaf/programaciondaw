/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s02_1_ex11;

/**
 *
 * @author Mati
 */
public class Memory {
    //Memory data
    private int memorySize;
    private String memoryType;
    private int memorySpeed;
    
    public Memory(int memSize, String memType, int memSpeed){
        memorySize = memSize;
        memoryType = memType;
        memorySpeed = memSpeed;
    }
    public Memory(Memory other){
        if(other == null){
            System.out.println("Fatal error");
            System.exit(0);
        }
        memorySize = other.memorySize;
        memoryType = other.memoryType;
        memorySpeed = other.memorySpeed;
    }
    public Memory(){
        
    }
    
    public void setMemoryType(String newType)
    {
        memoryType = newType;
    }
    public void setMemorySpeed(int newSpeed)
    {
        memorySpeed = newSpeed;
    }
    public void setMemorySize(int newSize)
    {
        memorySize = newSize;
    }
    
    public String getMemoryType()
    {
        return memoryType;
    }
    public int getMemorySpeed()
    {
        return memorySpeed;
    }
    public int getMemorySize()
    {
        return memorySize;
    }
    @Override
    public String toString(){
        return ("Tipo de memoria "+memoryType+
                "\nTamaño de memoria "+memorySize+
                "\nVelocidad de memoria "+memorySpeed);
    }
    public boolean equals(Memory other){
        return ((memorySize == other.memorySize) &&
                (memoryType.equalsIgnoreCase(other.memoryType))&&
                (memorySpeed == other.memorySpeed));
    }
}
