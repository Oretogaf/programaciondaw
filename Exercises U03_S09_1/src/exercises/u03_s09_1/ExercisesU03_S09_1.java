/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s09_1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S09_1 {
public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int codigo;
        int vecesAlquilado;
        int masVecesAlquilado;
        int codMasAlquilado;
        int menosVecesAlquilado;
        int codMenosAlquilado;
        int numeroAlquilerMas20;
        int numeroAlquiler7Entre15;
        int alquilerTotal;
        int numeroPeliculas;
        //Declaramos todas las variables
        
        codigo = 0;
        masVecesAlquilado=Integer.MIN_VALUE;
        codMasAlquilado=0;
        menosVecesAlquilado=Integer.MAX_VALUE;
        codMenosAlquilado=0;
        numeroAlquilerMas20=0;
        numeroAlquiler7Entre15=0;
        alquilerTotal=0;
        numeroPeliculas=0;
        //Inicializamos a cero las variables cuando sea necesario
        
        do{
            System.out.println("Introduce el codigo de la pelicula");
            codigo=uin.nextInt();
        }while(codigo>999);
        //Pedimos el valor de la variable codigo e iniciamos el bucle while
        
        while(codigo>=0){
            System.out.println("Introduce el numero de veces que ha sdo alquilada la pelicula de codigo "+codigo);
            vecesAlquilado=uin.nextInt();
            //Pedimos el valor de la variable vecesAlquilado
            
            numeroPeliculas++;
            //Incrementamos el valor de la varible numeroPeliculas cada vez que hacemos el bucle while
            
            alquilerTotal+=vecesAlquilado;
            //Almacenamos en la variable alquilerTotal el numero total de veces que se alquilan todas las peliculas
            
            if(vecesAlquilado>masVecesAlquilado){
                masVecesAlquilado=vecesAlquilado;
                codMasAlquilado=codigo;
            }else{}
            //Comprobamos si vecesAlquilado es mayor que masVecesAlquilado
            
            if(vecesAlquilado<menosVecesAlquilado){
                menosVecesAlquilado=vecesAlquilado;
                codMenosAlquilado=codigo;
            }else{}
            //Comprobamos si vecesAlquilado es menor que menosVecesAlquilado
            
            if(vecesAlquilado>20){
                numeroAlquilerMas20++;
            }else{}
            //Incrementamos la variable numeroAlquilerMas20 si la variable vecesAlquilado es mayor que 20
            
            if((vecesAlquilado>=7)&&(vecesAlquilado<=15)){
                numeroAlquiler7Entre15++;
            }else{}
            //Incrementamos la variable numeroAlquiler7Entre15 si la variable vecesAlquilado esta comprendido entre 7 y 15
            
            do{
                System.out.println("Introduce el codigo de la pelicula");
                codigo=uin.nextInt();
            }while(codigo>999);
            //Pedimos el valor de la variable codigo y repetimos el bucle while con nuevos valores
        }
        System.out.println("La pelicula mas alquilada es la de codigo "+codMasAlquilado+" con "+masVecesAlquilado+" veces");
        System.out.println("La pelicula menos alquilada es la de codigo "+codMenosAlquilado+" con "+menosVecesAlquilado+" veces");
        System.out.println("Hay "+numeroAlquilerMas20+" que han sido alquiladas mas de 20 veces");
        System.out.println("Hay "+numeroAlquiler7Entre15+" que han sido alquiladas entre 7 y 15 veces");
        System.out.println("La media de alquiler es "+(alquilerTotal/numeroPeliculas));
        //Imprimimos los datos de las estadisticas por pantalla
    }
    
}
