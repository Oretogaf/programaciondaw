/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_2_ex13;

/**
 *
 * @author Mati
 */
public class ExercisesU05_S03_2_Ex13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s1 = "abc12de3f4";
        int aux = 0;
        for(int i = 0; i < s1.length(); i++){
            switch(s1.charAt(i)){
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '0':
                    aux += Integer.parseInt("" + s1.charAt(i));
                    break;
                default:
                    break;
            }
        }
        System.out.println(aux);
    }
    
}
