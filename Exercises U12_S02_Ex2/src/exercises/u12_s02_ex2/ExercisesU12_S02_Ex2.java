/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s02_ex2;

import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Mati
 */
public class ExercisesU12_S02_Ex2 {
    private int numBolas;
    private HashSet urna;

    public ExercisesU12_S02_Ex2() {
        urna = new HashSet();
    }
    public ExercisesU12_S02_Ex2(int numBolas) {
        this.numBolas = numBolas;
        urna = new HashSet();
    }
    public void introducirCadena(String cad){
        if(urna.size() <= numBolas){
            if(!urna.add(cad)){
                System.out.println("La cadena ya existe");
            }
        }
    }
    public int getNumBolas() {
        return urna.size();
    }
    @Override
    public String toString(){
        String ret = "";
        Iterator it = urna.iterator();
        if(!urna.isEmpty()){
            while(it.hasNext()){
                ret += it.next() + ", ";
            }
        }
        return ret;
    }
}
