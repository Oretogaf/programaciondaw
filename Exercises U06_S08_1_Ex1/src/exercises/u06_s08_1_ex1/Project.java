/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_1_ex1;

import java.util.Objects;

/**
 *
 * @author Mati
 */
public class Project {
    private String nameResponsible;
    private String nameProject;
    private float budgetAmount;
    private float raisedAmount;
    private int nDonations;
    public Project(String responsible, String nProject, float budget, float raised, int nDonations){
        nameResponsible = responsible;
        nameProject = nProject;
        budgetAmount = budget;
        raisedAmount = raised;
        this.nDonations = nDonations;
    }
    public Project(){
        budgetAmount = 0F;
        raisedAmount = 0F;
        this.nDonations = 0;
    }
    public void setNameResponsible(String resp){
        nameResponsible = resp;
    }
    public void setNameProject(String proj){
        nameProject = proj;
    }
    public void setBudgetAmount(float amo){
        budgetAmount = amo;
    }
    public void setRaisedAmount(float amo){
        raisedAmount = amo;
    }
    public void setNDonations(int num){
        nDonations = num;
    }
    public String getNameResponsible() {
        return nameResponsible;
    }
    public String getNameProject() {
        return nameProject;
    }
    public float getBudgetAmount() {
        return budgetAmount;
    }
    public float getRaisedAmount() {
        return raisedAmount;
    }
    public int getnDonations() {
        return nDonations;
    }
    @Override
    public String toString() {
        return "The project " + nameProject
                + " with the responsible " + nameResponsible 
                + ", has a budgetAmount=" + budgetAmount 
                + ", and a raisedAmount=" + raisedAmount 
                + ", with " + nDonations + " donations";
    }
    public boolean equals(Project other) {
        if (other == null) {
            return false;
        }
        return nameProject == other.nameProject
                && nameResponsible == other.nameResponsible
                && budgetAmount == other.budgetAmount
                && raisedAmount == other.raisedAmount
                && nDonations == other.nDonations;
    }
}
