/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_1_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Main {
    public static Project[] ngo = new Project[10];
    private static void menu() {
        int index;
        System.out.println("1.Introduce data\n"
                + "2.Do a donation\n"
                + "3.List of projects do not cover yet\n"
                + "4.Exit");
        index = uin.nextInt();
        switch(index){
            case 1:
                for (int i = 0; i < ngo.length; i++) {
                    System.out.println();
                    uin.nextLine();
                    IntroduceData(i);
                }
                System.out.println();
                break;
            case 2:
                System.out.println();
                makeDonation();
                System.out.println();
                break;
            case 3:
                System.out.println();
                imprimeNoFinanciados();
                System.out.println();
                break;
            case 4:
                System.out.println();
                System.out.println("Goodbye!!");
                System.exit(0);
                break;
            default:
                System.out.println("Fatal error");
                System.exit(0);
                break;
        }
        menu();
    }
    private static void IntroduceData(int i) {
        String namePro;
        String nameResp;
        float bugAmo;
        float raiAmo;
        int nDona;
        do{
            System.out.println("Insert the name of the project " + (i + 1));
            namePro = uin.nextLine();
            if(namePro.length() == 0){
                System.out.println("Invalid name");
            }
        }while(namePro.length() == 0);
        do{
            System.out.println("Insert the name of the Resonsible of the project " + (i + 1));
            nameResp = uin.nextLine();
            if(nameResp.length() == 0){
                System.out.println("Invalid name");
            }
        }while(nameResp.length() == 0);
        do{
            System.out.println("Insert the budget amount of the project " + (i + 1));
            bugAmo = uin.nextFloat();
            if(bugAmo < 0){
                System.out.println("Invalid amount");
            }
        }while(bugAmo < 0);
        do{
            System.out.println("Insert the raised amount of the project " + (i + 1));
            raiAmo = uin.nextFloat();
            if(raiAmo < 0){
                System.out.println("Invalid amount");
            }
        }while(raiAmo < 0);
        do{
            System.out.println("Insert the number of donations of the project " + (i + 1));
            nDona = uin.nextInt();
            if(nDona < 0){
                System.out.println("Invalid number of donations");
            }
        }while(nDona < 0);
        ngo[i] = new Project(nameResp, namePro, bugAmo, raiAmo, nDona);
    }
    private static void makeDonation(){
        uin.nextLine();
        boolean found = false;
        float amount;
        float maxDif = Float.MIN_VALUE;
        float dif;
        int i;
        String name;
        System.out.println("Introduce the name of the project");
        name = uin.next();
        for (i = 0; i < ngo.length; i++) {
            found = ngo[i].getNameProject().equals(name);
            if(found){
                break;
            }
        }
        if(i == ngo.length){
            System.out.println("Fatal Error");
            System.exit(0);
        }
        if(found){
            System.out.println("Introduce the amount of the donation");
            amount = uin.nextFloat();
            ngo[i].setNDonations(ngo[i].getnDonations() + 1);
            buscaMayorDiferencia(i, amount, maxDif);                
        }else{
            System.out.println("No existe ningun projecto con ese nombre");
        }
    }
    private static void imprimeNoFinanciados(){
        int cont = 0;
        for(Project proje : ngo){
            if(proje.getBudgetAmount() > proje.getRaisedAmount()){
                System.out.println(proje.toString());
                cont++;
            }
        }
        if(cont == 0){
            System.out.println("There aren't exist any project who needs financing");
        }
    }
    private static void buscaMayorDiferencia(int i, float amount, float maxDif) {
        float dif;
        ngo[i].setRaisedAmount(ngo[i].getRaisedAmount() + amount);
        if(ngo[i].getBudgetAmount() < ngo[i].getRaisedAmount()){
            amount = ngo[i].getRaisedAmount() - ngo[i].getBudgetAmount();
            ngo[i].setRaisedAmount(ngo[i].getBudgetAmount());
            for (int j = 0; j < ngo.length; j++) {
                dif = ngo[j].getBudgetAmount() - ngo[j].getRaisedAmount();
                if(dif > maxDif){
                    maxDif = dif;
                    i = j;
                }
            }
            ngo[i].setNDonations(ngo[i].getnDonations() + 1);
            buscaMayorDiferencia(i, amount, maxDif);
        }
    }
    public static Scanner uin = new Scanner(System.in);
    
    public static void main(String[] args){
        menu();
    }
}
