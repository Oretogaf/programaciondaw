/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s02_1_ex7;

/**
 *
 * @author Mati
 */
public class ExercisesU05_S02_1_Ex7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Integer name1 = new Integer(5);
        System.out.println(name1);
        Integer name2 = name1;
        System.out.println(name2);
        Integer name3 = name1;
        System.out.println(name3);
        Integer name4 = name1;
        System.out.println(name4);
        Integer name5 = name1;
        System.out.println(name5);
        
        name1 = new Integer(10);
        System.out.println(name1);
        System.out.println(name2);
        System.out.println(name3);
        System.out.println(name4);
        System.out.println(name5);
    }
    
}
