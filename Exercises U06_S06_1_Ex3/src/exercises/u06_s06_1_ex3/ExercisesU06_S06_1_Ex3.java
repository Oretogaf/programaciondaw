/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s06_1_ex3;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S06_1_Ex3 {
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float[][] M1 = new float[20][5];
        float[][] M2 = new float[20][5];
        float[][] M3 = new float[20][5];
        int i,j;
        for (i = 0; i < M1.length; i++) {
            for (j = 0; j < M1[i].length; j++) {
                do{
                    System.out.println("Introduce la nota del alumno " + (i + 1) + ", en la asignatura " + (j + 1));
                    M1[i][j] = uin.nextFloat();
                    if(M1[i][j] > 10 && M1[i][j] < 0){
                        System.out.println("Nota incorrecta");
                    }
                }while(M1[i][j] > 10 && M1[i][j] < 0);
            }
        }
        for (i = 0; i < M2.length; i++) {
            for (j = 0; j < M2[i].length; j++) {
                do{
                    System.out.println("Introduce la nota del alumno " + (i + 1) + ", en la asignatura " + (j + 1));
                    M2[i][j] = uin.nextFloat();
                    if(M2[i][j] <= 10 && M2[i][j] >= 0){
                        System.out.println("Nota incorrecta");
                    }
                }while(M2[i][j] <= 10 && M2[i][j] >= 0);
            }
        }
        for (i = 0; i < M3.length; i++) {
            for (j = 0; j < M3[i].length; j++) {
                if(M1[i][j] >= M2[i][j]){
                    M3[i][j] = M1[i][j];
                }else{
                    M3[i][j] = M2[i][j];
                }
            }
        }
    }
    
}
