/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s04_2_3_ex4;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S04_2_3_Ex4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float[] array = {26F,45F,6.7F,59F,56F,23F,2.83F,54F,12F,2.55F,3.67F,43F,34F,21F,34F,56F,76F,6.7F,56F,85F};
        int i;
        float aux = 0;
        for(i = 0; i < array.length; i++){
            if(array[i] % 2 == 0){
                aux += array[i];
            }
        }
        System.out.println("The sum of the even numbre is: " + aux);
    }
    
}
