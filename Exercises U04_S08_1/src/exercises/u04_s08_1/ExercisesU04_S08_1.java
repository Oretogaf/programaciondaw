/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s08_1;

import java.util.Scanner;
/**
 *
 * @author Mati
 */
public class ExercisesU04_S08_1 {
    public static String month;
    public static Scanner uin = new Scanner(System.in);
    public static void setMonth(String mon){
        month=mon;
    }
    public static int getMonth( )
    {
        if (month.equalsIgnoreCase("Enero"))
            return 1;
        else if (month.equalsIgnoreCase("Febrero"))
            return 2;
        else if (month.equalsIgnoreCase("Marzo"))
            return 3;
        else if (month.equalsIgnoreCase("Abril"))
            return 4;
        else if (month.equalsIgnoreCase("Mayo"))
            return 5;
        else if (month.equalsIgnoreCase("Junio"))
            return 6;
        else if (month.equalsIgnoreCase("Julio"))
            return 7;
        else if (month.equalsIgnoreCase("Agosto"))
            return 8;
        else if (month.equalsIgnoreCase("Septiembre"))
            return 9;
        else if (month.equalsIgnoreCase("Octubre"))
            return 10;
        else if (month.equalsIgnoreCase("Noviembre"))
            return 11;
        else if (month.equalsIgnoreCase("Diciembre"))
            return 12;
        else
        {
            System.out.println("Fatal Error");
            System.exit(0);
            return 0; //Needed to keep the compiler happy
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String mesLetra;
        int mesNum;
        int dia;
        int año;
        int total;
        int totalSuma=0;
        System.out.println("Introduce el mes de nacimiento");
        mesLetra=uin.next();
        System.out.println("Introduce el dia de nacimiento");
        dia=uin.nextInt();
        System.out.println("Introduce el año de nacimiento");
        año=uin.nextInt();
        setMonth(mesLetra);
        mesNum=getMonth();
        total=dia+mesNum+año;
        while(total>0){
            totalSuma += total % 10;
            total /= 10;
        }
        while(totalSuma>0){
            total += totalSuma % 10;
            totalSuma /= 10;
        }
        System.out.println();
        System.out.println(total);
    }
    
}
