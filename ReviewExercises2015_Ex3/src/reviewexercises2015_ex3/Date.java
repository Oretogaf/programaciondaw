/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewexercises2015_ex3;

/**
 *
 * @author NoxVX750
 */
public class Date {
    private int day;
    private int month;
    private int year;
    public Date(int d, int m, int y){
        day = d;
        month = m;
        year = y;
    }
    public Date(int d, String m ,int y){
        day = d;
        month = monthString(m);
        year = y;
    }
    private int monthString(String mon){
        String monUpperCase;
        monUpperCase = mon.toUpperCase();
        switch(monUpperCase){
            case "ENERO":
                return 1;
            case "FEBRERO":
                return 2;
            case "MARZO":
                return 3;
            case "ABRIL":
                return 4;
            case "MAYO":
                return 5;
            case "JUNIO":
                return 6;
            case "JULIO":
                return 7;
            case "AGOSTO":
                return 8;
            case "SEPTIEMBRE":
                return 9;
            case "OCTUBRE":
                return 10;
            case "NOVIEMBRE":
                return 11;
            case "DICIEMBRE":
                return 12;
            default:
                System.out.println("Fatal Error");
                System.exit(0);
                break;
        }
        System.out.println("Fatal Error");
        System.exit(0);
        return 0;
    }
    public int getDay(){
        return day;
    }
    public int getMonth(){
        return month;
    }
    public int getYear(){
        return year;
    }
    public void setDay(int da){
        day = da;
    }
    public void setMonth(int mo){
        month = mo;
    }
    public void setMonth(String mon){
        month = monthString(mon);
    }
    public void setYear(int ye){
        year = ye;
    }
    @Override
    public String toString(){
        return day + "/" + month + "/" + year;
    }
}
