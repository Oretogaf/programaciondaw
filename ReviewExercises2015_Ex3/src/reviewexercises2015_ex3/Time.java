/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewexercises2015_ex3;

/**
 *
 * @author NoxVX750
 */
public class Time {
    private int hour;
    private int minute;
    public Time(int our,int min){
        hour = our;
        minute = min;
    }
    public int getHour(){
        return hour;
    }
    public int getMinute(){
        return minute;
    }
    public void setHour(int hou){
        hour = hou;
    }
    public void setMinute(int minu){
        minute = minu;
    }
    @Override
    public String toString(){
        return hour + ":" + minute;
    }
}
