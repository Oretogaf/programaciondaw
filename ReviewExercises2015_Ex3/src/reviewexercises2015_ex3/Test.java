/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewexercises2015_ex3;

import java.util.Scanner;

/**
 *
 * @author NoxVX750
 */
public class Test {
    String nameCourse;
    String classroom;
    Date date;
    Time time;
    public Test(String nam, String cla, int d, int m, int y, int ho, int min){
        nameCourse = nam;
        classroom = cla;
        date = new Date(d, m, y);
        time = new Time(ho, min);
    }
    public void setNameCourse(String nam){
        nameCourse = nam;
    }
    public void setClassroom(String classR){
        classroom = classR;
    }
    public void setDate(int da, int mo, int ye){
        date.setDay(da);
        date.setMonth(mo);
        date.setYear(ye);
    }
    public void setDate(int da, String mon, int ye){
        date.setDay(da);
        date.setMonth(mon);
        date.setYear(ye);
    }
    public void setTime(int hou, int minu){
        time.setHour(hou);
        time.setMinute(minu);
    }
    public String getNameCourse(){
        return nameCourse;
    }
    public String getClassroom(){
        return classroom;
    }
    public Date getDate(){
        return date;
    }
    public Time getTime(){
        return time;
    }
    @Override
    public String toString(){
        return nameCourse + ", " + classroom + ", " + date.toString() + " " + time.toString();
    }
    public static Scanner uin = new Scanner (System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String name;
        String clas;
        int day;
        int month;
        int year;
        int hour;
        int minute;
        System.out.println("Introduce el nombre del curso");
        name = uin.next();
        System.out.println("Introduce el nombre de la clase");
        clas = uin.next();
        System.out.println("Introduce el dia");
        day = uin.nextInt();
        System.out.println("Introduce el mes");
        month = uin.nextInt();
        System.out.println("Introduce el año");
        year = uin.nextInt();
        System.out.println("Introduce la hora");
        hour = uin.nextInt();
        System.out.println("Introduec los minutos");
        minute = uin.nextInt();
        Test test1 = new Test(name, clas, day, month, year, hour, minute);
        System.out.println(test1.getNameCourse());
        System.out.println(test1.getClassroom());
        System.out.println(test1.getDate());
        System.out.println(test1.getTime());
        System.out.println("Introduce el nombre del curso");
        name = uin.next();
        System.out.println("Introduce el nombre de la clase");
        clas = uin.next();
        System.out.println("Introduce el dia");
        day = uin.nextInt();
        System.out.println("Introduce el mes");
        month = uin.nextInt();
        System.out.println("Introduce el año");
        year = uin.nextInt();
        System.out.println("Introduce la hora");
        hour = uin.nextInt();
        System.out.println("Introduec los minutos");
        minute = uin.nextInt();
        test1.setNameCourse(name);
        test1.setClassroom(clas);
        test1.setDate(day, month, year);
        test1.setTime(hour, minute);
        System.out.println(test1.toString());
    }
    
}
