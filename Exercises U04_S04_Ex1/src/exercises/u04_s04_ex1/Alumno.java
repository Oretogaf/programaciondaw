/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s04_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Alumno {
    
    private int notaTeoria;
    private int notaPractica;
    private String nombre;
    private int edad;
    private static float costeTitulo;
    public static Scanner uin = new Scanner(System.in);
    
    public Alumno(){
        
    }
    public Alumno(char pedir){
        pedirTodosLosDatos();
    }
    public Alumno(int edad){
        this.edad=edad;
        pedirNombre();
        uin.nextLine();
        pedirNotas();
    }
    
    public float calcularMedia(){
        float media;
        media=(notaTeoria+notaPractica)/2f;
        return media;
    }
    public int devNumCaractNombre(){
        return nombre.length();
    }
    public String getNombre(){
        return nombre;
    }
    public int getNotaTeoria(){
        return notaTeoria;
    }
    public int getNotaPractica(){
        return notaPractica;
    }
    public static void inicializarCosteTitulo(float coste){
        costeTitulo=coste;
    }
    public char inicialNombre(){
        return nombre.charAt(0);
    }
    public static void pedirCosteTitulo(){
        System.out.println("Introduce el coste del titulo");
        costeTitulo=uin.nextFloat();
    }
    public void pedirEdad(){
        do{
            System.out.print("Introduce la edad: ");
            edad=uin.nextInt();
        }while(edad<0);
    }
    public void pedirNombre(){
        System.out.print("Introduce el nombre: ");
        nombre=uin.next();
    }
    public void pedirNotas(){
        do{
            System.out.print("Introduce la nota de teoria: ");
            notaTeoria=uin.nextInt();
        }while((notaTeoria<0)&&(notaTeoria>10));
        do{
            System.out.print("Introduce la nota de practica: ");
            notaPractica=uin.nextInt();
        }while((notaPractica<0)&&(notaPractica>10));
    }
    public void pedirTodosLosDatos(){
        pedirNombre();
        uin.nextLine();
        pedirEdad();
        pedirNotas();
    }
    public void visualCosteTitulo(){
        System.out.println(costeTitulo);
    }
    public void visualNombre(){
        System.out.println("El estudiante llamado "+nombre);
    }
    public void visualEdad(){
        System.out.println("Tiene "+edad+" años");
    }
    public void visualNotas(){
        System.out.println("La nota de teoria es: "+notaTeoria);
        System.out.println("La nota de practica es: "+notaPractica);
    }
    public void visualTodosDatos(){
        visualNombre();
        visualEdad();
        visualNotas();
    }
    
    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        int edad;

        //1
        pedirCosteTitulo();
        
        System.out.println("");
        //2
        Alumno alumno1 = new Alumno('p');
        
        System.out.println("");
        //3
        System.out.println("Introduce la edad de los mellizos");
        edad=uin.nextInt();
        Alumno alumno2 = new Alumno(edad);
        Alumno alumno3 = new Alumno(edad);
        
        System.out.println("");
        //4
        Alumno alumno4 = new Alumno();
        alumno4.nombre=alumno1.nombre;
        alumno4.edad=alumno1.edad;
        alumno4.notaTeoria=alumno1.notaTeoria;
        alumno4.notaPractica=alumno1.notaPractica;
        
        System.out.println("");
        //5
        alumno1.visualTodosDatos();
        alumno2.visualTodosDatos();
        alumno3.visualTodosDatos();
        alumno4.visualTodosDatos();
        
        System.out.println("");
        //6
        pedirCosteTitulo();
        
        System.out.println("");
        //7
        alumno1.visualTodosDatos();
        alumno2.visualTodosDatos();
        alumno3.visualTodosDatos();
        alumno4.visualTodosDatos();
        
        System.out.println("");
        //8
        System.out.println(alumno1.inicialNombre());
        
        System.out.println("");
        //9
        System.out.println(alumno1.getNotaTeoria());
        System.out.println(alumno1.getNotaPractica());
        
        System.out.println("");
        //10
        alumno1.visualTodosDatos();
        alumno2.visualTodosDatos();
        alumno3.visualTodosDatos();
        alumno4.visualTodosDatos();
        
        System.out.println("");
        //11
        System.out.println(alumno1.calcularMedia());
        System.out.println(alumno2.calcularMedia());
        System.out.println(alumno3.calcularMedia());
        
        System.out.println("");
        //12
        System.out.println(alumno2.devNumCaractNombre());
        System.out.println(alumno3.devNumCaractNombre());
    }
    
}
