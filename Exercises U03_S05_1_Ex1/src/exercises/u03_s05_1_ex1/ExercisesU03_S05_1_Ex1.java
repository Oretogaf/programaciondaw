/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s05_1_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S05_1_Ex1 {
public static Scanner std = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int m=0;
        do{
            System.out.println("Introduce numero de mes");
            m=std.nextInt();
        }while((m<1)||(m>12));
        System.out.println("El mes es "+m);
    }
    
}
