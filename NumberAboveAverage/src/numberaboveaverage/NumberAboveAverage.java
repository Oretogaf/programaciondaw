/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numberaboveaverage;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class NumberAboveAverage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float[] temperature = new float[10];
        float tempMed;
        
        Scanner uin = new Scanner(System.in);
        
        for(int i = 0; i < temperature.length; i++){
            System.out.println("Introduce la temperatura del dia " + (i + 1));
            temperature[i] = uin.nextFloat();
        }
        tempMed = 0;
        for (float temp : temperature){
            tempMed += temp;
        }
        tempMed /= temperature.length;
        for(int i = 0; i < temperature.length; i++){
            if(temperature[i] < tempMed){
                System.out.println("El dia " + (i + 1) + " la temperatura fue menor que la media");
            }
        }
    }
    
}
