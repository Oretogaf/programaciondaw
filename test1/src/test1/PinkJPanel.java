/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package test1;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author Mati
 */
public class PinkJPanel extends JPanel {
    
    public PinkJPanel() {
        super();
        setBackground(Color.PINK);
    }
    public PinkJPanel(Color color){
        super();
        setBackground(color);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        
        JMenuBar jMenuBar = new JMenuBar();
        
        JMenuItem jMenuItem = new JMenuItem("Hello");
        JMenuItem jMenuItem1 = new JMenuItem("Bye");
        
        jMenuBar.add(jMenuItem);
        jMenuBar.add(jMenuItem1);
        
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        
        jFrame.add(jMenuBar);
        
        jFrame.setSize(screenSize.width, (screenSize.height - 40));
        
        jFrame.setDefaultCloseOperation(3);
        jFrame.setVisible(true);
        
        MenuItem aChoice = new MenuItem("Exit");
    }
    
}
