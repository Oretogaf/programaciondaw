/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test1;

import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import javax.swing.JFrame;

/**
 *
 * @author Mati
 */
public class MenuGUI extends JFrame {

    public MenuGUI(){
        super();
        MenuItem mItem = new MenuItem();
        Menu m = new Menu();
        m.add(mItem);
        MenuBar mBar = new MenuBar();
        mBar.add(m);
        setMenuBar(mBar);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        MenuGUI menuGUI = new MenuGUI();
    }
}
