/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s03_1_ex4;

import Documentos.Gestion_Economica.Factura;
import Documentos.RRHH.Contrato;

/**
 *
 * @author Mati
 */
public class ExercisesU07_S03_1_Ex4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Contrato contrato = new Contrato("Julio",15);
        Contrato contrato1 = new Contrato("Ruben",20);
        Factura factura = new Factura("Julio",100);
        Factura factura1 = new Factura("Ruben",100.10);
        contrato.imprimirInfo();
        System.out.println();
        factura.imprimirInfo();
        System.out.println();
        System.out.println();
        contrato1.imprimirInfo();
        System.out.println();
        factura1.imprimirInfo();
    }
    
}
