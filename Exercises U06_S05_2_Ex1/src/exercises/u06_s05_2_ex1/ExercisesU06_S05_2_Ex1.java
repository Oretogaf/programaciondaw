/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s05_2_ex1;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S05_2_Ex1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[][] array = new int[10][10];
        int r, c;
        for(r = 0; r < array.length; r++){
            for (c = 0; c < 10; c++) {
                if (r % 2 == 0) {
                    array[c][r] = 1;
                }else{
                    array[c][r] = 0;
                }
            }
        }
        for(r = 0; r < array.length; r++){
            for (c = 0; c < 10; c++) {
                System.out.print(array[c][r]);
            }
            System.out.println("");
        }
    }
    
}
