/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewexercises2015_ex4;

/**
 *
 * @author NoxVX750
 */
public class Employee {
    private String name;
    private String surname;
    private int numberID;
    private String address;
    private float salary;
    private int comision;
    private int age;
    public Employee(){
        
    }
    public Employee(String nam, String sur, int id, String addr, float sala, int ae){
        name = nam;
        surname = sur;
        numberID = id;
        address = addr;
        salary = sala;
        age = ae;
    }
    public void calculateComision(){
        if(age <= 50){
            comision = 7;
        }else{
            comision = 5;
        }
    }
    public float calculateFinalSalary(){
        return (salary * (1 + (comision / 100)));
    }
    public void setName(String nam){
        name = nam;
    }
    public void setSurname(String sur){
        surname = sur;
    }
    public void setNumberID(int id){
        numberID = id;
    }
    public void setAddress(String addr){
        address = addr;
    }
    public void setSalary(float sala){
        salary = sala;
    }
    public void setAge(int ae){
        age = ae;
    }
    public String getName(){
        return name;
    }
    public String getSurname(){
        return surname;
    }
    public int getNumberID(){
        return numberID;
    }
    public String getAddress(){
        return address;
    }
    public float getSalary(){
        return salary;
    }
    public int getComision(){
        return comision;
    }
    public int getAge(){
        return age;
    }
    @Override
    public String toString(){
        return (surname + ", " + name + " con ID " + numberID + " vive en " + address + " y cobra un salario de " + salary + " con una comision de " + comision);
    }
    public boolean equals(Employee other){
        if(other == null){
            System.out.println("Fatal error");
            System.exit(0);
        }
        return ((name.equalsIgnoreCase(other.name)) && (surname.equalsIgnoreCase(other.surname))
                    && (numberID == other.numberID) && (address.equalsIgnoreCase(other.address)) && (salary == other.salary)
                    && (comision == other.comision) && ( age == other.comision));
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
