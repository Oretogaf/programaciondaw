/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_4_ex2;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Library {
    private static Book[] library = new Book[2];
    private static float ganancias;
    public static Scanner uin = new Scanner(System.in);
    
    private static void menu(){
        int index;
        System.out.println("1.Vender un libro\n"
                + "2.Introducir datos de un libro\n"
                + "3.Ver información de una obra\n"
                + "4.Salir");
        index = uin.nextInt();
        System.out.println();
        switch(index){
            case 1:
                venderLibro();
                break;
            case 2:
                introducirDatos();
                break;
            case 3:
                imprimirGanancias();
                break;
            case 4:
                System.out.println();
                System.out.println("Goodbye!!");
                System.exit(0);
                break;
            default:
                System.out.println("Fatal error");
                System.exit(0);
                break;
        }
        System.out.println();
        menu();
    }
    private static void introducirDatos(){
        String title;
        String autor;
        float price;
        int numberOfUnits;
        boolean fou = false;
        System.out.println("Introduce el nombre del libro");
        title = uin.nextLine();
        for(Book boo : library){
            if(boo == null){
                System.out.println("Introduce el autor");
                autor = uin.nextLine();
                System.out.println("Introduce el precio");
                price = uin.nextFloat();
                System.out.println("Introduce el numero de unidades");
                numberOfUnits = uin.nextInt();
                boo = new Book(title, autor, price, numberOfUnits);
            } else {
                if(boo.getTitle().equals(title)){
                    System.out.println("Introduce el numero de ejemplares para reponer");
                    numberOfUnits = uin.nextInt();
                    boo.setNumberOfUnits(boo.getNumberOfUnits() + numberOfUnits);
                }
            }
        }
    }
    public static void imprimirGanancias(){
        System.out.println("Las ganancias obtenidas son de " + ganancias + " €");
    }
    public static void venderLibro(){
        int unidades;
        String name;
        System.out.println("Introduce el nombre del libro que desea comprar");
        name = uin.nextLine();
        for(int i = 0; i < library.length; i++){
            if(name.equals(library[i].getTitle())){
                System.out.println("Indique cuantas unidades desea comprar");
                unidades = uin.nextInt();
                if(unidades > library[i].getNumberOfUnits()){
                    System.out.println("No existe suficiente stock. El numero de unidades se reducira a " + library[i].getNumberOfUnits());
                    unidades = library[i].getNumberOfUnits();
                }
                System.out.println("El precio por unidad es " + library[i].getPrice());
                System.out.println("El total es de " + (library[i].getPrice() * unidades));
                library[i].setNumberOfUnits(library[i].getNumberOfUnits() - unidades);
                ganancias = library[i].getPrice() * unidades;
                System.out.println("Gracias por su compra");
            }
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        menu();
    }
    
}
