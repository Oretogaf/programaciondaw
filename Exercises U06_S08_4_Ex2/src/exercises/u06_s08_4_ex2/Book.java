/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_4_ex2;

/**
 *
 * @author Mati
 */
public class Book {
    private String title;
    private String autor;
    private float price;
    private int numberOfUnits;

    public Book(String title, String autor, float price, int numberOfUnits) {
        this.title = title;
        this.autor = autor;
        this.price = price;
        this.numberOfUnits = numberOfUnits;
    }
    
    public String getTitle() {
        return title;
    }
    public String getAutor() {
        return autor;
    }
    public float getPrice() {
        return price;
    }
    public int getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public void setAutor(String autor) {
        this.autor = autor;
    }
    public void setPrice(float price) {
        this.price = price;
    }
    public void setNumberOfUnits(int numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    @Override
    public String toString() {
        return "Book{" + "title=" + title + ", autor=" + autor + ", price=" + price + ", numberOfUnits=" + numberOfUnits + '}';
    }
    
}
