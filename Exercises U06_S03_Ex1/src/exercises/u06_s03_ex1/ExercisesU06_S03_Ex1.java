/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s03_ex1;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S03_Ex1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] intVec = new int[20];
        int posMax = -1;
        int max = Integer.MIN_VALUE;
        int i;
        for(i = 0;i < intVec.length; i++){
            intVec[i] = Integer.MIN_VALUE + (int)(Math.random() * Integer.MAX_VALUE);
        }
        for(i = 0; i < intVec.length; i++){
            if(intVec[i] > max){
                max = intVec[i];
                posMax = i;
            }
        }
        System.out.println("Numero maximo: " + max + "\ncon posicion: " + posMax);
    }
    
}
