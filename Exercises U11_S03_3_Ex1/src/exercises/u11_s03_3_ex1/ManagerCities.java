/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u11_s03_3_ex1;

import java.util.ArrayList;

/**
 *
 * @author Mati
 */
public class ManagerCities {
    ArrayList<City> list = new ArrayList();
    
    public void addCity(Object cit){
        if(cit instanceof City){
            list.add((City) cit);
        }else{
            System.out.println("El objeto no es de la clase City");
        }
    }
    
    public void removeCity(Object cit){
        if(cit instanceof City){
            if(list.contains((City) cit)){
                list.remove((City) cit);
            }else{
                System.out.println("No existe la ciudad");
            }
        }else{
            System.out.println("El objeto no es de la clase City");
        }
    }
    
    public void removeCity(String nam){
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getCityName().equalsIgnoreCase(nam)){
                list.remove(i);
                return;
            }
        }
        System.out.println("No existe una ciudad con ese nombre");
    }
    
    public void showNumberOfCities(){
        System.out.println(list.size());
    }
    
    public long calculateTotalPopulation(){
        long total = 0;
        for (City cit : list) {
            total += cit.getPopulation();
        }
        return total;
    }
    
    public void showCityWithMaxHabitants(){
        long maxHab = Long.MIN_VALUE;
        int ind = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getPopulation() > maxHab){
                maxHab = list.get(i).getPopulation();
                ind = i;
            }
        }
        try{
            System.out.println(list.get(ind));
        }
        catch (Exception ex){
            System.out.println("Se produjo un error al buscar la ciudad más habitada");
        }
    }
    
    public void showNameOfCitiesOfAProvince(int code){
        for (City city : list) {
            if(city.getCodeOfProvince() == code){
                System.out.println(city);
            }
        }
    }
    
}
