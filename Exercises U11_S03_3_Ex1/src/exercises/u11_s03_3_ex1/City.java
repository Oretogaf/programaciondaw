/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u11_s03_3_ex1;

/**
 *
 * @author Mati
 */
public class City {
    private String cityName;
    private long population;
    private int codeOfProvince;

    public City(String cityName, long population, int codeOfProvince) {
        this.cityName = cityName;
        this.population = population;
        this.codeOfProvince = codeOfProvince;
    }

    public String getCityName() {
        return cityName;
    }
    public long getPopulation() {
        return population;
    }
    public int getCodeOfProvince() {
        return codeOfProvince;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public void setPopulation(long population) {
        this.population = population;
    }
    public void setCodeOfProvince(int codeOfProvince) {
        this.codeOfProvince = codeOfProvince;
    }

    @Override
    public String toString() {
        return "City{" + "cityName=" + cityName + ", population=" + population + ", codeOfProvince=" + codeOfProvince + '}';
    }
}
