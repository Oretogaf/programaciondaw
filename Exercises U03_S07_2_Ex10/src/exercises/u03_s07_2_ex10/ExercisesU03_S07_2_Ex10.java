/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s07_2_ex10;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S07_2_Ex10 {
public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int numeroAlumnos;
        int numeroLibros;
        int numeroLibrosTotal=0;
        int numeroPaginasLibro;
        int numeroPaginasAlumno;
        int numeroPaginasTotal=0;
        int maxLibros=0;
        int maxPaginas=0;
        int i;
        int j;
        int s;
        do{
            System.out.println("Introduce el numero de alumnos");
            numeroAlumnos=uin.nextInt();
        }while(numeroAlumnos<1);
        for(i=1;i<=numeroAlumnos;i++){
            do{
            System.out.println("Introduce el numero de libros del alumno "+i);
            numeroLibros=uin.nextInt();
            }while(numeroLibros<1);
            numeroPaginasAlumno=0;
            if(numeroLibros>maxLibros){
                maxLibros=numeroLibros;
            }else{}
            numeroLibrosTotal+=numeroLibros;
            for(j=1;j<=numeroLibros;j++){
                do{
                    System.out.println("Introduce el numero de paginas del libro "+j+ " del alumno "+i);
                    numeroPaginasLibro=uin.nextInt();
                }while(numeroPaginasLibro<1);
                numeroPaginasAlumno+=numeroPaginasLibro;
                if(numeroPaginasAlumno>maxPaginas){
                    maxPaginas=numeroPaginasAlumno;
                }else{}
            }
            System.out.println("El numero de paginas leidas por el alumno "+i+" es "+numeroPaginasAlumno);
            numeroPaginasTotal+=numeroPaginasAlumno;
        }
        System.out.println("El numero total de libros leidos es "+numeroLibrosTotal);
        System.out.println("El numero maximo de paginas leidas es "+maxPaginas);
        System.out.println("El numero maximo de libros leidos es "+maxLibros);
        System.out.println("El numero total de paginas leidas por todos los alumnos es "+numeroPaginasTotal);
    }
    
}
