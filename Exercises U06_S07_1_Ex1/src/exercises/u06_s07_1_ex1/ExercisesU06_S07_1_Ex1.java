/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s07_1_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S07_1_Ex1 {
    public static float[][] notas = new float[3][5];
    public static Scanner uin = new Scanner(System.in);
    public static void leerNotas(){
        for (int i = 0; i < notas.length; i++) {
            for (int j = 0; j < notas[i].length; j++) {
                System.out.println("Introduce la nota de la asignatura "
                + (j + 1) + " en la evaluacion " + (i + 1));
                notas[i][j] = uin.nextFloat();
            }
        }
    }
    public static void media1Evaluacion(){
        float av = 0;
        int numAv = 0;
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < notas[i].length; j++) {
                av += notas[i][j];
                numAv++;
            }
        }
        av /= numAv;
        System.out.println(av);
    }
    public static void mediaEvaluacionX(){
        float av = 0;
        int numAv = 0;
        int eva;
        System.out.println("Introduce la evaluacion de la que se desea saber la media");
        eva = uin.nextInt();
        for (int i = (eva - 1); i < eva; i++) {
            for (int j = 0; j < notas[i].length; j++) {
                av += notas[i][j];
                numAv++;
            }
        }
        av /= numAv;
        System.out.println(av);
    }
    public static void mediaPorEvaluacion(){
        for (int i = 0; i < notas.length; i++) {
            float av = 0;
            int numAv = 0;
            for (int j = 0; j < notas[i].length; j++) {
                av += notas[i][j];
                numAv++;
            }
            av /= numAv;
            System.out.println("La media para la evaluacion "
                    + (i + 1) + " es " + av);
        }
    }
    public static void mediaAsignaturaX(){
        float av = 0;
        int numAv = 0;
        int asi;
        System.out.println("Introduce la asignatura de la "
                + "que se desa calcular la media");
        asi = uin.nextInt();
        for (int i = (asi - 1); i < asi; i++) {
            for (int j = 0; j < notas.length; j++) {
                av += notas[j][i];
                numAv++;
            }
        }
        av /= numAv;
        System.out.println("La media es " + av);
    }
    public static void mediaAsignaturasCurso(){
        int asi;
        for (int i = 0; i < notas[0].length; i++) {
            float av = 0;
            int numAv = 0;
            for (int j = 0; j < notas.length; j++) {
                av += notas[j][i];
                numAv++;
            }
            av /= numAv;
            System.out.println("La media de la asignatura "
                    + (i + 1) + " es " + av);
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        leerNotas();
        media1Evaluacion();
        mediaEvaluacionX();
        mediaPorEvaluacion();
        mediaAsignaturaX();
        mediaAsignaturasCurso();
    }
    
}
