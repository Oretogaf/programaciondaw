/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s04_ex1;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU12_S04_Ex1 {
    static HashMap<String, Float> alumnos = new HashMap();
    static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int numAlu;
        String nombre;
        float nota;
        System.out.println("Introduce el numero de alumnos");
        numAlu = uin.nextInt();
        System.out.println();
        
        for (int i = 0; i < numAlu; i++) {
            System.out.println("Introduce el nombre del alumno " + (i + 1));
            nombre = uin.next();
            System.out.println("Introduce la nota del alumno " + (i + 1));
            nota = uin.nextFloat();
            alumnos.put(nombre, nota);
            System.out.println();
        }
        
        for(Map.Entry<String, Float> entry : alumnos.entrySet()){
            System.out.println("Nombre: " + entry.getKey() + ", nota: " + entry.getValue());
        }
    }
    
}
