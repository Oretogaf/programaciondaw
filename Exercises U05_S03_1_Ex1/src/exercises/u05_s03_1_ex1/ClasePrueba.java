/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_1_ex1;

/**
 *
 * @author Mati
 */
public class ClasePrueba {
    public int x;
    public ClasePrueba(){
        x=23;
    }
    @Override
    public String toString(){
        return "Convertido a cadena: "+x;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ClasePrueba cp = new ClasePrueba();
        boolean b;
        String s1;
        String s2;
        String s3;
        b = true;
        s1 = "valor 1 = "+b;
        s2 = "valor 2 = "+45;
        s3 = "valor 3 = "+cp;
        System.out.println(s1);
        System.out.println("Longitud: "+s1.length());
        System.out.println(s2);
        System.out.println("Longitud: "+s2.length());
        System.out.println(s3);
        System.out.println("Longitud: "+s3.length());
    }
    
}
