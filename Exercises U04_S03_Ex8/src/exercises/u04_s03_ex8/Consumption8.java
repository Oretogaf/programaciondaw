/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s03_ex8;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Consumption8 {
    private int kilometros;
    private int litros;
    private int mediaVelocidad;
    private int precioOil;
    public static Scanner uin = new Scanner(System.in);
    public Consumption8(){
        kilometros=setKilometros();
        litros=setLitros();
        mediaVelocidad=setMediaVelocidad();
        precioOil=setPrecioOil();
    }
    public float getTime(){
        return (float)kilometros/mediaVelocidad;
    }
    public float mediaConsumo(){
        return (float)litros/kilometros;
    }
    public float consumoEuros(){
        return litros*precioOil;
    }
    public int setKilometros(){
        System.out.println("Introduce el numero de kilometros recorridos");
        return uin.nextInt();
    }
    public int setLitros(){
        System.out.println("Introduce el numero de litro consumidos");
        return uin.nextInt();
    }
    public int setMediaVelocidad(){
        System.out.println("Introduce la media de velocidad");
        return uin.nextInt();
    }
    public int setPrecioOil(){
        System.out.println("Introduce el precio de la gasolina");
        return uin.nextInt();
    }
    @Override
    public String toString(){
        return("Kilometros = " + kilometros
                + ", litros = " + litros
                + ", media de velocidad = " + mediaVelocidad
                + ", precio combustible = " + precioOil);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Consumption8 consum0 = new Consumption8();
        System.out.println("El tiempo tardado ha sido: "+consum0.getTime());
        System.out.println("El consumo ha sido: "+consum0.mediaConsumo());
        System.out.println("El dinero gastado ha sido: "+consum0.consumoEuros());
        
        Consumption8 consum1 = new Consumption8();
        
        if(consum0.equals(consum1)){
            System.out.println("Igual");
        }else{
            System.out.println("Diferente");
        }
        System.out.println(consum0.toString());
        System.out.println(consum1.toString());
    }
    
}
