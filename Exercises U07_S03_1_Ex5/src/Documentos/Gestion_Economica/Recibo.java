/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documentos.Gestion_Economica;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Mati
 */
public class Recibo {
    protected String concepto;
    protected Calendar fechaEmision;

    public Recibo(String concepto, int day, int month, int year) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(year,month,day);
        this.concepto = concepto;
        this.fechaEmision = calendar;
    }
    public Recibo(String concepto, Calendar fechaEmision) {
        this.concepto = concepto;
        this.fechaEmision = fechaEmision;
    }
    

    public String getConcepto() {
        return concepto;
    }
    public Calendar getFechaEmision() {
        return fechaEmision;
    }
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
    public void setFechaEmision(Calendar fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
    
    public void imprimirInfo(){
        System.out.println("El concepto es: " + concepto);
        System.out.println("La fecha es: " + fechaEmision.get(Calendar.DAY_OF_MONTH) + "/" + fechaEmision.get(Calendar.MONTH) + "/" + fechaEmision.get(Calendar.YEAR));
    }
}
