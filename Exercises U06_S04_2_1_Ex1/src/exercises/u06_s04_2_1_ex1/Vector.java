/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s04_2_1_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Vector {
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] array = new String[6];
        int i;
        for(i = 0; i < array.length; i++){
            System.out.println("Introduce la palabra de la posicion " + i);
            array[i] = uin.next();
        }
        System.out.println("");
        for(i = 0; i < array.length; i++){
            if(i % 2 == 0){
                System.out.print(array[i] + " ");
            }
        }
        System.out.println("");
        for(i = 0; i < array.length; i++){
            if(i % 2 == 1){
                System.out.print(array[i] + " ");
            }
        }
    }
    
}
