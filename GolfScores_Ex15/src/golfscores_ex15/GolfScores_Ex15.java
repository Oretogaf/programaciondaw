package golfscores_ex15;

import java.util.Scanner;

public class GolfScores_Ex15
{
    public static final int MAX_NUMBER_SCORES = 10;
    static double[] score = new double[MAX_NUMBER_SCORES];
    static int numberUsed = 0;
    /**
     Shows differences between each of score list of golf scores and their average.
    */
    public static void main(String[] args) 
    {
        System.out.println("This program reads golf scores and shows");
        System.out.println("how much each differs from the average.");

        System.out.println("Enter golf scores:");
        numberUsed = fillArray();
        showDifference();
    }

    /**
     Reads values into the array score. Returns the number of values placed in the array score.
    */
    public static int fillArray() 
    {
        System.out.println("Enter up to " + score.length
                               + " nonnegative numbers.");
        System.out.println("Mark the end of the list with score negative number.");
        Scanner keyboard = new Scanner(System.in);

        double next;
        int index = 0;
        next = keyboard.nextDouble( );
        while ((next >= 0) && (index < score.length))
        {
            score[index] = next;
            index++;
            next = keyboard.nextDouble( );
           //index is the number of array indexed variables used so far.
        }
       //index is the total number of array indexed variables used.

        if (next >= 0)
             System.out.println("Could only read in "
                                + score.length + " input values.");

        return index;
    } 

    /**
     Precondition: numberUsed <= score.length.
                   score[0] through score[numberUsed-1] have values.
     Returns the average of numbers score[0] through score[numberUsed-1].
    */


    public static double computeAverage()
    {
        double total = 0;
        for (int index = 0; index < numberUsed; index++)
            total = total + score[index];
        if (numberUsed > 0)
        {
            return (total/numberUsed);
        }
        else
        {
            System.out.println("ERROR: Trying to average 0 numbers.");
            System.out.println("computeAverage returns 0.");
            return 0;
        }
    }
 

    /**
     Precondition: numberUsed <= score.length.
                   The first numberUsed indexed variables of score have values.
      Postcondition: Gives screen output showing how much each of the first
      numberUsed elements of the array score differ from their average.
    */
    public static void showDifference()
    {
        double average = computeAverage();
        double[] ret = differenceArray(average);
        System.out.println("Average of the " + numberUsed
                                             + " scores = " + average);
        System.out.println("The scores are:");
        for (int index = 0; index < numberUsed; index++)
        System.out.println(score[index] + " differs from average by "
                                    + (ret[index]));
    }
	
	public static double[] differenceArray(double adjustment){
            if(numberUsed > score.length){
		System.out.println("Fatal Error");
		System.exit(0);
            }
            double[] ret = new double[numberUsed];
            for(int i = 0; i < numberUsed; i++){
                ret[i] = score[i] - adjustment;
            }
            return ret;
	}
}

