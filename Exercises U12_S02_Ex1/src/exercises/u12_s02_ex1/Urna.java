/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u12_s02_ex1;

import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Mati
 */
public class Urna {
    private int num;
    private HashSet urna;
    
    public Urna(int nume){
        urna = new HashSet();
        num = nume;
    }
    public Urna(){
        urna = new HashSet();
    }
    public void introducirBola(Integer bola){
        if(urna.size() <= num){
            if(!urna.add(bola)){
                System.out.println("El numero ya existe");
            }
        }
    }
    public int sacarBola(){
        Integer bola = null;
        Iterator it = urna.iterator();
        
        if(it.hasNext()){
            bola = (int) it.next();
            it.remove();
            return bola;
        }
        return bola;
    }
    public int getNumBolas(){
        return urna.size();
    }
    @Override
    public String toString(){
        Iterator it = urna.iterator();
        String res = "";
        if(!urna.isEmpty()){
            while(it.hasNext()){
                res += it.next() + ",";
            }
        }
        return res;
    }
}
