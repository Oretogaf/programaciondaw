/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s02_ex1;

/**
 *
 * @author Mati
 */
public class Main {
    public static void main(String[] args) {
        Urna urna = new Urna(10);
        urna.introducirBola(3);
        urna.introducirBola(0);
        urna.introducirBola(1);
        urna.introducirBola(5);
        urna.introducirBola(8);
        urna.introducirBola(2);
        urna.introducirBola(7);
        urna.introducirBola(4);
        urna.introducirBola(9);
        urna.introducirBola(6);
        
        System.out.println(urna.toString());
        System.out.println("Numero de bolas: " + urna.getNumBolas());
        
        System.out.println(urna.sacarBola());
        System.out.println("Numero de bolas: " + urna.getNumBolas());
        
        System.out.println(urna.toString());
    }
}
