/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_4_ex5;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Gallery {
    private static Picture[] gallery;
    public static Scanner uin = new Scanner(System.in);
    
    private static void iniciateArray(){
        int inicio;
        do{
            System.out.println("Introduce el numero de obras");
            inicio = uin.nextByte();
        }while(inicio < 0 || inicio > 80);
        gallery = new Picture[inicio];
        System.out.println();
    }
    private static void menu(){
        int index;
        System.out.println("1.Introducir datos de una obra\n"
                + "2.Ver información de una obra\n"
                + "3.Vender una obra\n"
                + "4.Salir");
        index = uin.nextInt();
        System.out.println();
        switch(index){
            case 1:
                introducirDatos();
                break;
            case 2:
                imprimirDatos();
                break;
            case 3:
                venderObra();
                break;
            case 4:
                System.out.println();
                System.out.println("Goodbye!!");
                System.exit(0);
                break;
            default:
                System.out.println("Fatal error");
                System.exit(0);
                break;
        }
        System.out.println();
        menu();
    }
    private static void introducirDatos(){
        int pictureCode;
        String title;
        String autor;
        int year;
        int width;
        int height;
        int numUnits;
        float price;
        for (int i = 0; i < gallery.length; i++) {
            uin.nextLine();
            System.out.println("Introduce el codigo de la obra " + (i + 1));
            pictureCode = uin.nextInt();
            uin.nextLine();
            System.out.println("Introduce el nombre de la obra");
            title = uin.nextLine();
            System.out.println("Introduce el autor");
            autor = uin.nextLine();
            System.out.println("Introduce el año");
            year = uin.nextInt();
            System.out.println("Introduce el ancho en centimetros");
            width = uin.nextInt();
            System.out.println("Introduce la altura en centimetros");
            height = uin.nextInt();
            System.out.println("Introduce el numero de ejemplares");
            numUnits = uin.nextInt();
            System.out.println("Introduce el precio");
            price = uin.nextFloat();
            gallery[i] = new Picture(pictureCode, title, autor, year, width, height, numUnits, price);
            System.out.println();
        }
    }
    private static void imprimirDatos(){
        int cod;
        System.out.println("Introduce el codigo de la obra que desea consultar");
        cod = uin.nextInt();
        for(Picture pic : gallery){
            if(pic.getPictureCode() == cod){
                System.out.println(pic.toString());
            }
        }
    }
    private static void venderObra(){
        int cod;
        char comp;
        System.out.println("Introduce el codigo de la obra que desea comprar");
        cod = uin.nextInt();
        for(Picture pic : gallery){
            if(pic.getPictureCode() == cod){
                if(pic.getNumUnits() > 0){
                    System.out.println("El precio de la obra son " + pic.getPrice() + "€, ¿Desea comprarlo?");
                    comp = uin.next().charAt(0);
                    comp = Character.toUpperCase(comp);
                    if(comp == 'S'){
                        System.out.println("Muchas gracias por su compra");
                        pic.setNumUnits(pic.getNumUnits() - 1);
                    }
                }else{
                    System.out.println("Lo sentimos. No nos quedan ejemplares de esa obra");
                }
            }
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        iniciateArray();
        menu();
    }
    
}
