/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s01_4_ex6;

/**
 *
 * @author Mati
 */
public class TitledEmployee extends SalariedEmployee {
    protected String title;

    public TitledEmployee(String name, String title) {
        super(name);
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    
    @Override
    public String getName(){
        return name + ", " + title;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TitledEmployee titledEmployee = new TitledEmployee("Julio", "Estudiante");
        System.out.println(titledEmployee.name);
        System.out.println(titledEmployee.title);
        System.out.println(titledEmployee.getName());
    }
    
}
