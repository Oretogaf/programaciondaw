/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s01_4_ex6;

/**
 *
 * @author Mati
 */
public class SalariedEmployee {
    protected String name;
    
    public SalariedEmployee() {
        
    }
    public SalariedEmployee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
}
