/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises_u04_s06_1_ex2;

/**
 *
 * @author Mati
 */
public class Counter {
    public int count;
    public Counter(){
        
    }
    public Counter(int cout){
        if(cout<0){
            count=0;
        }else{
            count=cout;
        }
    }
    public void resetCounter(){
        count=0;
    }
    public void increaseCount(){
        count++;
    }
    public void decreaseCount(){
        if(count==0){
            System.out.println("Fatal error");
            System.exit(0);
        }else{
            count--;
        }
    }
    @Override
    public String toString(){
        return "Count "+count;
    }
    public boolean equals(Counter otherCount){
        return ;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
