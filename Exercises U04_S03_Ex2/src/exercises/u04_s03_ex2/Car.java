/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s03_ex2;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Car {
    private int speed;
    public Car () {
        speed = 0;
    }
    public void Car(int spIn){
        speed=spIn;
    }
    public int getSpeed(){
        return(speed);
    }
    public void accelerate(int more){
        speed+=more;
    }
    public void brake(int less){
        speed-=less;
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int more;
        int less;
        int inicial;
        Car coche1 = new Car();
        Car coche2 = new Car();
        System.out.println("Introduce la velocidad inicial del coche 1");
        inicial=uin.nextInt();
        coche1.Car(inicial);
        System.out.println("Introduce el incremento de la velocidad del coche 1");
        more=uin.nextInt();
        coche1.accelerate(more);
        System.out.println(coche1.getSpeed());
        System.out.println("Introduce el decremento de la velocidad del coche 1");
        less=uin.nextInt();
        coche1.brake(less);
        System.out.println(coche1.getSpeed());
        System.out.println("Introduce la velocidad inicial del coche 2");
        inicial=uin.nextInt();
        coche2.Car(inicial);
        System.out.println("Introduce el incremento de la velocidad del coche 2");
        more=uin.nextInt();
        coche2.accelerate(more);
        System.out.println(coche2.getSpeed());
        System.out.println("Introduce el decremento de la velocidad del coche 1");
        less=uin.nextInt();
        coche2.brake(less);
        System.out.println(coche2.getSpeed());
    }
    
}
