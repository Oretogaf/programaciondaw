/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u07_s02_1_ex1;

/**
 *
 * @author Mati
 */
public class Empleado {
    private float sueldo;

    public Empleado() {
        
    }
 
    public Empleado(float sueldo) {
        this.sueldo = sueldo;
    }
    
    public float getSueldo() {
        return sueldo;
    }
}
