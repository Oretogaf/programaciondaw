/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u07_s02_1_ex1;

/**
 *
 * @author Mati
 */
public class Encargado extends Empleado{
    public Encargado() {
       super(100f);
    }
    
    @Override
    public float getSueldo(){
        return (super.getSueldo() * 1.1F);
    }
    
    public static void main(String[] args){
        Encargado enc = new Encargado();
        System.out.println(enc.getSueldo());
    }
}
