/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s01_1_ex1;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Mati
 */
public class ExercisesU12_S01_1_Ex1<T> {
    public boolean inSome(Collection<T> a, Collection<T> b,T c){
        return a.contains(c) || b.contains(c);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList();
        ArrayList<String> arrayList1 = new ArrayList();
        String hola = "hola";
        
        arrayList.add("hola");
        arrayList1.add("hola");
        
        ExercisesU12_S01_1_Ex1 exercisesU12_S01_1_Ex1 = new ExercisesU12_S01_1_Ex1();
        if(exercisesU12_S01_1_Ex1.inSome(arrayList, arrayList1, hola)){
            System.out.println("parameter c are in the collection a or b");
        }else{
            System.out.println("Parameter c are not in the collection a or b");
        }
    }
    
}
