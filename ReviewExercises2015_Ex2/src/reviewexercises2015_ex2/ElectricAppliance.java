/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewexercises2015_ex2;

/**
 *
 * @author NoxVX750
 */
public class ElectricAppliance {
    public int powerConsumition;
    public boolean connected;
    public static int totalConsumition;
    public ElectricAppliance(int consumition){
        powerConsumition = consumition;
        connected = false;
    }
    public void switchOn(){
        if(!connected){
            connected = true;
            totalConsumition += powerConsumition;
        }else{
            System.out.println("El aparato ya estaba encendido");
        }  
    }
    public void switchOff(){
        if(connected){
            connected = false;
            totalConsumition -= powerConsumition;
        }else{
            System.out.println("El aparato ya estaba apagado");
        }  
    }
    public static void printTotal(){
        System.out.println("la potencia total es: " + totalConsumition);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ElectricAppliance bulb = new ElectricAppliance(100);
        ElectricAppliance radiator = new ElectricAppliance(2000);
        ElectricAppliance iron = new ElectricAppliance(1200);
        printTotal();
        bulb.switchOn();
        iron.switchOn();
        printTotal();
        iron.switchOff();
        radiator.switchOn();
        printTotal();
    }
    
}
