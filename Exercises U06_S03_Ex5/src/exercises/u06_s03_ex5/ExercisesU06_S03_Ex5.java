/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exercises.u06_s03_ex5;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S03_Ex5 {

    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float[] tempMes;
        int diaMes;
        int i;
        float tempMax;
        float tempMin;
        float media;
        int indMax = -1;
        int indMin = -1;
        
        do{
            System.out.println("Introduzca el numero de dias del mes");
            diaMes = uin.nextInt();
        }while(diaMes < 28 || diaMes > 31);
        tempMes = new float[diaMes];
        tempMax = Float.MIN_VALUE;
        tempMin = Float.MAX_VALUE;
        media = 0;
        for(i = 0; i < tempMes.length; i++){
            System.out.println("Introduce la temperatura del dia "+ (i + 1));
            tempMes[i] = uin.nextFloat();
            if(tempMes[i] > tempMax){
                tempMax = tempMes[i];
                indMax = i;
            }else if(tempMes[i] < tempMin){
                tempMin = tempMes[i];
                indMin = i;
            }
            media += tempMes[i];
        }
        media /= tempMes.length;
        System.out.println("La temperatura media ha sido de " + media + " ºC");
        System.out.println("El dia mas caluroso ha sido el " + indMax + " con " + tempMax);
        System.out.println("El dia mas frio ha sido el " + indMin + " con " + tempMin);
    }
    
}
