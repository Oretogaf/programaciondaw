/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s06_1_ex2;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S06_1_Ex2 {
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float[][] notasClase = new float[20][5];
        int i,j,nAlu,not;
        //A. Leer las notas
        for (i = 0; i < notasClase.length; i++) {
            for (j = 0; j < notasClase[i].length; j++) {
                do{
                    System.out.println("Introduce la nota del alumno " + (i + 1) + ", en la asignatura " + (j + 1));
                    notasClase[i][j] = uin.nextFloat();
                    if(notasClase[i][j] <= 10 && notasClase[i][j] >= 0){
                        System.out.println("Nota incorrecta");
                    }
                }while(notasClase[i][j] <= 10 && notasClase[i][j] >= 0);
            }
        }
        
        //B. Mostrar las asignaturas aprobadas por alumno
        for (i = 0; i < notasClase.length; i++) {
            nAlu = 0;
            for (j = 0; j < notasClase[i].length; j++) {
                if(notasClase[i][j] >= 5){
                    nAlu++;
                }
            }
            if(nAlu == 1){
                System.out.println("El alumno " + (i + 1) + ", ha aprobado 1 asignatura");
            }else{
                System.out.println("El alumno " + (i + 1) + ", ha aprobado " + nAlu + " asignaturas");
            }
        }
        
        //C. Estudiantes sin ningun suspenso
        not = 0;
        for (i = 0; i < notasClase.length; i++) {
            nAlu = 0;
            for (j = 0; j < notasClase[i].length; j++) {
                if(notasClase[i][j] >= 5){
                    nAlu++;
                }
                if(nAlu == 5){
                    not++;
                }
            }
        }
        if(not == 1){
            System.out.println("Existe 1 alumno sin ninguna asignatura suspensa");
        }else{
            System.out.println("Existen " + not + " alumnos sin ninguna asignatuta suspensa");
        }
    }
    
}
