/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s03_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Bird {
    private int age;
    private String color;
    public Bird(){
        age=0;
        color="";
    }
    public void setAge(int a){
        age=a;
    }
    public void printAge(){
        System.out.println("Age: "+age);
    }
    public void setColor(String c){
        color=c;
    }
    public void printColor(){
        System.out.println("Color: "+color);
    }
    public static Scanner stdin= new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int a;
        String c;
        Bird b1=new Bird();
        System.out.println("Introduce la edad");
        a=stdin.nextInt();
        b1.setAge(a);
        b1.printAge();
        System.out.println("Introduce el color");
        c=stdin.next();
        b1.setColor(c);
        b1.printColor();
        
        Bird b2=new Bird();
        System.out.println("Introduce la edad");
        a=stdin.nextInt();
        b2.setAge(a);
        b2.printAge();
        System.out.println("Introduce el color");
        c=stdin.next();
        b2.setColor(c);
        b2.printColor();
    }
    
}
