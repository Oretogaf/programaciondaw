/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u10_s02_3_ex9;

import java.io.Serializable;

/**
 *
 * @author Mati
 */
public class Teacher extends Person implements Serializable{
    private int edad;

    public Teacher(int edad, String nombre, String apellidos) {
        super(nombre, apellidos);
        this.edad = edad;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Teacher other = (Teacher) obj;
        if (this.edad != other.edad) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Teacher{" + "edad=" + edad + '}' + super.toString();
    }
}
