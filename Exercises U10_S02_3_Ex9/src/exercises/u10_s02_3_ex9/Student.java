/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u10_s02_3_ex9;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Mati
 */
public class Student extends Person implements Serializable{
    private int numMatricula;
    private Date fechaMatricula;

    public Student(int numMatricula, Date fechaMatricula, String nombre, String apellidos) {
        super(nombre, apellidos);
        this.numMatricula = numMatricula;
        this.fechaMatricula = fechaMatricula;
    }

    public int getNumMatricula() {
        return numMatricula;
    }

    public void setNumMatricula(int numMatricula) {
        this.numMatricula = numMatricula;
    }

    public Date getFechaMatricula() {
        return fechaMatricula;
    }

    public void setFechaMatricula(Date fechaMatricula) {
        this.fechaMatricula = fechaMatricula;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Student other = (Student) obj;
        if (this.numMatricula != other.numMatricula) {
            return false;
        }
        if (!Objects.equals(this.fechaMatricula, other.fechaMatricula)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Student{" + "numMatricula=" + numMatricula + ", fechaMatricula=" + fechaMatricula + '}' + super.toString();
    }
    
}
