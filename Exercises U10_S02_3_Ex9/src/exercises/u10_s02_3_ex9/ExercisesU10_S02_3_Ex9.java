/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u10_s02_3_ex9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

/**
 *
 * @author Mati
 */
public class ExercisesU10_S02_3_Ex9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ObjectOutputStream ObjOut = null;
        ObjectInputStream ObjIn = null;
        
        Student st1 = new Student(001, new Date(115, 9, 10), "Julio", "Beltran");
        Student st2 = new Student(002, new Date(115, 9, 10), "Ruben", "Lopez");
        
        Teacher te1 = new Teacher(35, "Ramon", "Herrero");
        Teacher te2 = new Teacher(41, "Juanjo", "Garriges");
        
        Student st11 = null;
        Student st21 = null;
        
        Teacher te11 = null;
        Teacher te21 = null;
        
        try{
            ObjOut = new ObjectOutputStream(new FileOutputStream("salida.txt"));
            
            ObjOut.writeObject(st1);
            ObjOut.writeObject(st2);
            
            ObjOut.writeObject(te1);
            ObjOut.writeObject(te2);
            
            ObjIn = new ObjectInputStream(new FileInputStream("salida.txt"));
            
            try {
                st11 = (Student) ObjIn.readObject();
                st21 = (Student) ObjIn.readObject();
                
                te11 = (Teacher) ObjIn.readObject();
                te21 = (Teacher) ObjIn.readObject();
                
                System.out.println(st1);
                System.out.println(st11);
                System.out.println();
                
                System.out.println(st2);
                System.out.println(st21);
                System.out.println();
                
                System.out.println(te1);
                System.out.println(te11);
                System.out.println();
                
                System.out.println(te2);
                System.out.println(te21);
            } catch (ClassNotFoundException ex) {
                System.out.println("No existe la clase del objeto que intentas leer");
            }
        }
        catch (FileNotFoundException ex){
            System.out.println("No se pudo leer el fichero");
        }
        catch (IOException ex){
            System.out.println("Se produjo un error al intentear realizar operaciones de E/S");
        }
        finally
        {
            try {
                ObjOut.close();
            } catch (IOException ex) {
                System.out.println("Se produjo un error al intentear realizar operaciones de E/S");
            }
        }
    }
    
}
