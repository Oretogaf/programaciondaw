/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s03_1_ex11;

/**
 *
 * @author Mati
 */
public class CuentaLongitud {
    private int tamaño;
    private static int tamañoTotal;
    
    public CuentaLongitud(String cadena){
        tamaño = cadena.length();
        tamañoTotal += tamaño;
    }
    public int dameLongitud(){
        return tamaño;
    }
    public int dameLongitudTotal(){
        return tamañoTotal;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CuentaLongitud cuentaLongitud = new CuentaLongitud("Hola");
        System.out.println(cuentaLongitud.dameLongitud());
        System.out.println(cuentaLongitud.dameLongitudTotal());
        System.out.println("");
        
        CuentaLongitud cuentaLongitud1 = new CuentaLongitud("Ruben");
        System.out.println(cuentaLongitud1.dameLongitud());
        System.out.println(cuentaLongitud1.dameLongitudTotal());
        System.out.println("");
        
        CuentaLongitud cuentaLongitud2 = new CuentaLongitud("Yo");
        System.out.println(cuentaLongitud2.dameLongitud());
        System.out.println(cuentaLongitud2.dameLongitudTotal());
        System.out.println("");
        
        CuentaLongitud cuentaLongitud3 = new CuentaLongitud("Ser");
        System.out.println(cuentaLongitud3.dameLongitud());
        System.out.println(cuentaLongitud3.dameLongitudTotal());
        System.out.println("");
        
        CuentaLongitud cuentaLongitud4 = new CuentaLongitud("Julio");
        System.out.println(cuentaLongitud4.dameLongitud());
        System.out.println(cuentaLongitud4.dameLongitudTotal());
    }
    
}
