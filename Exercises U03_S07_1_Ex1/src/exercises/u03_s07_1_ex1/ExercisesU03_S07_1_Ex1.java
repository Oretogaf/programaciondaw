/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s07_1_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S07_1_Ex1 {
public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float nota=0;
        int apro=0;
        int susp=0;
        float suma=0;
        int n=0;
        float media;
        System.out.println("Introduce nota");
        nota=uin.nextInt();
        while(nota>=0){
            n++;
            if (nota>=5){
                apro++;
            }else if(nota>=0){
                susp++;
            }else{}
            suma+=nota;
            System.out.println("Introduce nota");
            nota=uin.nextInt();
        }
        if(n==0){
            System.out.println("No hay notas");
        }else{
            media=suma/n;
            System.out.println("El numero de asignaturas aprobar es "+apro);
            System.out.println("El numero de asignaturas suspendidas es "+susp);
            System.out.println("La media es "+media);
        }
    }
    
}
