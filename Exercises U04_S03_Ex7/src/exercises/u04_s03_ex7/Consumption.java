/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s03_ex7;

/**
 *
 * @author Mati
 */
public class Consumption {
    private int kilometros;
    private int litros;
    private int mediaVelocidad;
    private int precioOil;
    public Consumption(){
        kilometros=200;
        litros=5;
        mediaVelocidad=160;
        precioOil=15;
    }
    public float getTime(){
        return (float)kilometros/mediaVelocidad;
    }
    public float mediaConsumo(){
        return (float)litros/kilometros;
    }
    public float consumoEuros(){
        return litros*precioOil;
    }
    @Override
    public String toString(){
        return("Kilometros = " + kilometros
                + ", litros = " + litros
                + ", media de velocidad = " + mediaVelocidad
                + ", precio combustible = " + precioOil);
    }    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Consumption consum0 = new Consumption();
        System.out.println(consum0.getTime());
        System.out.println(consum0.mediaConsumo());
        System.out.println(consum0.consumoEuros());
        
        Consumption consum1 = new Consumption();
        if(consum0.equals(consum1)){
            System.out.println("Igual");
        }else{
            System.out.println("Diferente");
        }
        System.out.println(consum0.toString());
        System.out.println(consum1.toString());
    }
    
}
