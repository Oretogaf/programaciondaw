/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s01_10_ex27;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Pet {
    private String name;
    private int age;
    private double weight;
    
    private String petType;
    private final double SEDATIVEMGPERKGDOG = 0.03;
    private final double SEDATIVEMGPERKGCAT = 0.002;
    private final double SEDATIVEMGPERML = 10;
    private final double PAINKILLERMGPERKGDOG = 0.5;
    private final double PAINKILLERMGPERKGCAT = 0.25;
    private final double PAINKILLERMGPERML = 12;
    
    public Pet(){
        
    }
    public Pet(String inicialName, int inicialAge, double inicialWeight, String petType){
        if(inicialName.length()==0){
            System.out.println("Fatal error");
            System.exit(0);
        }else if(inicialAge<0){
            System.out.println("Fatal error");
            System.exit(0);
        }else if (inicialWeight<0){
            System.out.println("Fatal error");
            System.exit(0);
        }else if (petType.compareToIgnoreCase("cat")==1||petType.compareToIgnoreCase("dog")==1){
            System.out.println("Fatal error");
            System.exit(0);
        }
        name=inicialName;
        age=inicialAge;
        weight=inicialWeight;
        this.petType=petType;
    }
    public Pet(String inicialName){
        name=inicialName;
    }
    public Pet(int inicialAge){
        age=inicialAge;
    }
    public Pet(double inicialWeight){
        weight=inicialWeight;
    }
    public void set(String newName, int newAge, double newWeight, String petType){
        if(newName.length()==0){
            System.out.println("Fatal error");
            System.exit(0);
        }else if(newAge<0){
            System.out.println("Fatal error");
            System.exit(0);
        }else if (newWeight<0){
            System.out.println("Fatal error");
            System.exit(0);
        }else if (petType.compareToIgnoreCase("cat")==1||petType.compareToIgnoreCase("dog")==1){
            System.out.println("Fatal error");
            System.exit(0);
        }
        name=newName;
        age=newAge;
        weight=newWeight;
        this.petType=petType;
    }
    public void setName(String newName){
        if(newName.length()==0){
            System.out.println("Fatal error");
            System.exit(0);
        }
        name=newName;
    }
    public void setAge(int newAge){
        if(newAge<0){
            System.out.println("Fatal error");
            System.exit(0);
        }
        age=newAge;
    }
    public void setWeight(double newWeight){
        if(newWeight<0){
            System.out.println("Fatal error");
            System.exit(0);
        }
        weight=newWeight;
    }
    public String getName(){
        return name;
    }
    public int getAge(){
        return age;
    }
    public double getWeight(){
        return weight;
    }
    @Override
    public String toString(){
       return "La mascota se llama "+name+", su edad "+age+", y su peso "+weight;
    }
    public double sedativeDrug(){
        double doseMg;
        if (petType.equalsIgnoreCase("dog")){
            doseMg=SEDATIVEMGPERKGDOG*weight;
            return SEDATIVEMGPERML/doseMg;
        }else{
            doseMg=SEDATIVEMGPERKGCAT*weight;
            return SEDATIVEMGPERML/doseMg;
        }
    }
    public double painKillerDrug(){
        double doseMg;
        if (petType.equalsIgnoreCase("dog")){
            doseMg=PAINKILLERMGPERKGDOG*weight;
            return PAINKILLERMGPERML/doseMg;
        }else{
            doseMg=PAINKILLERMGPERKGCAT*weight;
            return PAINKILLERMGPERML/doseMg;
        }
    }
    
    public static Scanner uin =new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String name;
        int age;
        double weight;
        String typePet;
        //!
        Pet pet1 = new Pet();
        System.out.println("Introduce el nombre para la mascota 1");
        name=uin.next();
        System.out.println("Introduce la edad para la mascota 1");
        age=uin.nextInt();
        System.out.println("Introduce el peso de la mascota 1");
        weight=uin.nextDouble();
        System.out.println("Introduce el tipo de mascota");
        typePet=uin.next();
        pet1.set(name, age, weight,typePet);

        //!
        Pet pet2 = new Pet(name,age,weight,typePet);
        System.out.println("Introduce el nombre para la mascota 2");
        name=uin.next();
        System.out.println("Introduce la edad para la mascota 2");
        age=uin.nextInt();
        System.out.println("Introduce el peso de la mascota 2");
        weight=uin.nextDouble();
        pet2.set(name, age, weight,typePet);
        
        //!
        System.out.println("Introduce el nombre para la mascota 3");
        name=uin.next();
        Pet pet3 = new Pet(name);
        System.out.println("Introduce la edad para la mascota 3");
        age=uin.nextInt();
        System.out.println("Introduce el peso de la mascota 3");
        weight=uin.nextDouble();
        pet3.setAge(age);
        pet3.setWeight(weight);
        
        //!
        System.out.println("Introduce la edad para la mascota 4");
        age=uin.nextInt();
        Pet pet4 = new Pet(age);
        System.out.println("Introduce el nombre para la mascota 4");
        name=uin.next();
        System.out.println("Introduce el peso de la mascota 4");
        weight=uin.nextDouble();
        pet4.setName(name);
        pet4.setWeight(weight);
        
        //!
        System.out.println("Introduce el peso de la mascota 5");
        weight=uin.nextDouble();
        Pet pet5 = new Pet(weight);
        System.out.println("Introduce el nombre para la mascota 5");
        name=uin.next();
        System.out.println("Introduce la edad para la mascota 5");
        age=uin.nextInt();
        pet5.setName(name);
        pet5.setAge(age);
        
        System.out.println(pet1.toString());
        System.out.println(pet2.getAge());
        System.out.println(pet3.getName());
        System.out.println(pet4.getWeight());
    }
    
}
