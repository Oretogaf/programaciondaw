package pruebassocket;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.net.ssl.SSLSocket;
import sun.rmi.server.UnicastRef;

/**
 *
 * @author Mati
 */
public class PruebasSocketClient extends Thread{
    final String HOST = "localhost";
    final int PORT = 46012;
    Socket sc;
    PrintWriter mensaje;
    BufferedReader entrada;
    Scanner uin = new Scanner(System.in);
    String message;
    
    public void run(){
        PruebasSocketClient pruebasSocketClient = new PruebasSocketClient();
        pruebasSocketClient.initClient();
    }
    public void initClient(){
        try {
            sc = new Socket(HOST,PORT);
            mensaje = new PrintWriter(sc.getOutputStream(), true);
            entrada = new BufferedReader(new InputStreamReader(sc.getInputStream()));
            //System.out.println(entrada.readLine());
            while(true){
                message = uin.nextLine();
                if(message.equalsIgnoreCase("exit()")){
                    mensaje.println("EL CLIENTE SE HA DESCONECTADO/VALOR 935");
                    sc.close();
                    break;
                }
                mensaje.println(message);
            }
        }catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }
}
