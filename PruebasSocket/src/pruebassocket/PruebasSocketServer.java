package pruebassocket;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mati
 */
public class PruebasSocketServer extends Thread{
    final int PORT = 46012;
    ServerSocket sc;
    Socket so;
    public static List totalClientes = new ArrayList();
    
    public void run(){
        PruebasSocketServer pruebasSocketServer = new PruebasSocketServer();
        pruebasSocketServer.initServer();
    }
    public void initServer(){
        try {
            sc = new ServerSocket(PORT);
            System.out.println("Esperando nueva conexion");
            while(true){
                so = sc.accept();
                System.out.println("Se ha conectado un cliente");
                totalClientes.add(new User(so, totalClientes.size()));
                OutputStream outputStream = so.getOutputStream();
                DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
                dataOutputStream.writeUTF("HOLA SOY EL SERVER");
            }
        }catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }
}
