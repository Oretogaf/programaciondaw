/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package pruebassocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mati
 */
public class User extends Thread{
    Socket clientSocket;
    PrintWriter salida;
    BufferedReader entrada;
    int id;
    String mensajeRecibido;
    
    public User(Socket clientSocket,int idUser) {
        this.clientSocket = clientSocket;
        id = idUser;
        super.start();
    }
    public void run() {
        while (true) {
            try {
                entrada = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                mensajeRecibido = entrada.readLine();
                if(mensajeRecibido != null){
                    if(mensajeRecibido.equals("EL CLIENTE SE HA DESCONECTADO/VALOR 935")){
                        PruebasSocketServer.totalClientes.remove(id);
                        System.out.println("Se ha desconectado un cliente");
                        System.out.println("La lista de usuarios conectados es:");
                        System.out.println(PruebasSocketServer.totalClientes.toString());
                        System.out.println("");
                        System.out.println("");
                    }else{
                        System.out.println("El usuario " + id + " : " + mensajeRecibido);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void escribir(String mensaje){
        try {
            salida = new PrintWriter(clientSocket.getOutputStream(), true);
            salida.println(mensaje);
            salida.flush();
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
