/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Prueba;

/**
 *
 * @author Mati
 */
public class ObjetosAccesoAtributo {
    public static void main(String[] args) {
        AccesoAtributos accesoAtributos = new AccesoAtributos();
        accesoAtributos.puint = 1;
        System.out.println(accesoAtributos.puint);
        
        accesoAtributos.setPuint(22);
        System.out.println(accesoAtributos.getPuint());
        
        accesoAtributos.paint = 333;
        System.out.println(accesoAtributos.paint);
    }
}
