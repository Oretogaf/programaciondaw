/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s02_1_ex5;

/**
 *
 * @author Mati
 */
public class ExercisesU05_S02_1_Ex5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Integer objInt1 = 3;
        Integer objInt2 = 2;
        Integer objInt3;
        int valInt1;
        int valInt2;
        int valInt3;
        
        valInt1 = objInt1;
        valInt2 = objInt2;
        valInt3 = valInt1 + valInt2;
        objInt3 = valInt3;
    }
    
}
