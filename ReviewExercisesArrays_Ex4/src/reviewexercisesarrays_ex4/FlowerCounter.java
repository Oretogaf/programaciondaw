/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package reviewexercisesarrays_ex4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class FlowerCounter {
    public static final String[] flowerName = {"petunia", "pansy", "rose", "violet", "carnation"};
    public static final double[] flowerPrice = {0.5, 0.75, 1.50, 0.50, 0.80};
    public static final Scanner uin = new Scanner(System.in);
    
    public static int isFlower(String kind){
        for(int i = 0; i < flowerName.length; i++){
            if(flowerName[i].equalsIgnoreCase(kind)){
                return i;
            }
        }
        return -1;
    }
    public static void calculate(){
        int ind;
        int uni;
        double total;
        do{
            System.out.println("Introduce el nombre de la flor deseada");
            String kind = uin.next();
            ind = isFlower(kind);
        }while(ind == -1);
        System.out.println("Introduce la cantidad de flores que desea");
        uni = uin.nextInt();
        total = flowerPrice[ind] * uni;
        System.out.printf("El coste total es de %3.2f€\n",total);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        calculate();
    }
    
}
