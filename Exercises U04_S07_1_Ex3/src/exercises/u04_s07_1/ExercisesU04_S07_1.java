/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s07_1;

import java.util.Scanner;
import java.util.Random;
/**
 *
 * @author Mati
 */
public class ExercisesU04_S07_1 {
    static int intentos;
    static int numRd;
    static int numMax;
    static int numMin;
    
    public static Scanner uin = new Scanner(System.in);
    public static Random rd = new Random();
    
    
    static void numeroIntentos(){
        int numIntentos;
        do{
            System.out.println("Introduce el numero de intentos");
            numIntentos=uin.nextInt();
        }while(numIntentos<0);
        intentos=numIntentos;
    }
    static void generadorNumeroAleatorio(){
        int numAlea;
        do{
            numAlea = numMin + (rd.nextInt(numMax-numMin+1));
        }while(numAlea>numMin&&numAlea<numMax);
        numRd=numAlea;
    }
    static void numerosMinimosMaximos(){
        int numSup;
        int numInf;
        do{
            System.out.println("Introduce el numero inferior");
            numInf=uin.nextInt();
            System.out.println("Introduce el numero superior");
            numSup=uin.nextInt();
        }while(numInf>numSup);
        numMax=numSup;
        numMin=numInf;
    }
    static void averiguaNumeroAleatorio(){
        int i;
        int numUsuario;
        for(i=1;i<=intentos;i++){
            System.out.println("Intento " + i);
            do{
                System.out.println("Introduce el numero");
                numUsuario=uin.nextInt();
                if(numUsuario>numMin||numUsuario<numMax){
                    System.out.println("Numero no valido");
                }
            }while(numUsuario>numMin||numUsuario<numMax);
            if(numUsuario==numRd){
                System.out.println("¡¡Perfecto!! El numero era "+numRd);
                System.exit(0);
            }else if(numUsuario!=numRd&&i<intentos){
                System.out.println("Intentalo de nuevo");
                System.out.println();
            }else{
                System.out.println("Buena suerte la proxima vez");
            }
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        numerosMinimosMaximos();
        numeroIntentos();
        generadorNumeroAleatorio();
        averiguaNumeroAleatorio();
    }
    
}
