/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s03_ex4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Finance {
    private double euroDollar;
    public Finance(){
        euroDollar=1.36;
    }
    public Finance(double myEuroDollar){
        euroDollar=myEuroDollar;
    }
    public double eurosToDollars(double euros){
        return euros*euroDollar;
    }
    public double dollarsToEuros(double dollars){
        return dollars/euroDollar;
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double myEuroDollar;
        double euros;
        double dollars;
        System.out.println("Introduce el valor de conversión entre Euros y Dolares");
        myEuroDollar=uin.nextDouble();
        Finance converter1 = new Finance(myEuroDollar);
        System.out.println("Introduce el numero de Dolares para convertir");
        dollars=uin.nextDouble();
        System.out.println(converter1.dollarsToEuros(dollars));
        System.out.println("Introduce el numero de Euros para convertir");
        euros=uin.nextDouble();
        System.out.println(converter1.eurosToDollars(euros));
    }
    
}
