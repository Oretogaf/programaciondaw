/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u11_s02_ex3;

import java.util.ArrayList;

/**
 *
 * @author Mati
 */
public class Archivador {
    private ArrayList<Paciente> list = new ArrayList();

    public void guardarPaciente(Object pac){
        if(pac instanceof Paciente){
            list.add((Paciente) pac);
        }else{
            System.out.println("El objeto no es de la clase Paciente");
        }
    }
    
    public void eliminarPaciente(Object pac){
        if(pac instanceof Paciente){
            if(list.contains((Paciente) pac)){
                list.remove((Paciente) pac);
            }else{
                System.out.println("No existe el paciente");
            }
        }else{
            System.out.println("El objeto no es de la clase Paciente");
        }
    }

    @Override
    public String toString() {
        String ret = "";
        for (Paciente pacs : list) {
            ret += pacs.toString() + "\n";
        }
        return ret;
    }
    
}
