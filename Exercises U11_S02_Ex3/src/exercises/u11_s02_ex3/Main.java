/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u11_s02_ex3;

/**
 *
 * @author Mati
 */
public class Main {
    public static void main(String[] args) {
        Archivador arch = new Archivador();
        Paciente pac1 = new Paciente(001, "Julio", "Beltran Saiz", 18);
        Paciente pac2 = new Paciente(002, "Rubén", "Lopez Valero", 21);
        Paciente pac3 = new Paciente(003, "Fran", "Estreder", 20);
        arch.guardarPaciente(pac1);
        arch.guardarPaciente(pac2);
        arch.guardarPaciente(pac3);
        System.out.println("Impresión del archivador antes de eliminar un paciente");
        System.out.println(arch);
        
        arch.eliminarPaciente(pac1);
        System.out.println("Impresión del archivador despues de eliminar un paciente");
        System.out.println(arch);
    }
}
