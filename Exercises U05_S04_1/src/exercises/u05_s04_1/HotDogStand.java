/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s04_1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class HotDogStand {
    private int hotDogStandID;
    private int hotDogsSold;
    static int totalSold;
    
    public HotDogStand(){
        
    }
    public HotDogStand(int standID, int sold){
        hotDogStandID = standID;
        hotDogsSold = sold;
        totalSold += sold;
    }
    public void justSold(){
        hotDogsSold++;
        totalSold++;
    }
    public int getHotDogsSold(){
        return hotDogsSold;
    }
    public static int getTotalSold(){
        return totalSold;
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int standID, sold;
        HotDogStand hd1 = new HotDogStand();
        System.out.println("Introduce el ID del puesto");
        standID = uin.nextInt();
        System.out.println("Introduce el numero vendido");
        sold = uin.nextInt();
        HotDogStand hd2 = new HotDogStand(standID, sold);
        
        System.out.println("Introduce el ID del puesto");
        standID = uin.nextInt();
        System.out.println("Introduce el numero vendido");
        sold = uin.nextInt();
        HotDogStand hd3 = new HotDogStand(standID, sold);
        
        System.out.println("Introduce el ID del puesto");
        standID = uin.nextInt();
        System.out.println("Introduce el numero vendido");
        sold = uin.nextInt();
        HotDogStand hd4 = new HotDogStand(standID, sold);
        
        hd2.justSold();
        hd2.justSold();
        
        hd3.justSold();
        hd3.justSold();
        hd3.justSold();
        hd3.justSold();
        hd3.justSold();
        
        hd4.justSold();
        hd4.justSold();
        hd4.justSold();
        hd4.justSold();
        hd4.justSold();
        hd4.justSold();
        hd4.justSold();
        hd4.justSold();
        hd4.justSold();
        hd4.justSold();
        hd4.justSold();
        
        System.out.println(hd1.getHotDogsSold());
        System.out.println(hd2.getHotDogsSold());
        System.out.println(hd3.getHotDogsSold());
        System.out.println(hd4.getHotDogsSold());
        System.out.println(getTotalSold());
    }
    
}
