/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_2_ex6;

/**
 *
 * @author Mati
 */
public class ExercisesU05_S03_2_Ex6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s1 = "Uno dosuno";
        s1 = s1 + "mas";
        s1 = s1.replace('o', 'e');
        s1 = s1.toUpperCase();
        System.out.println(s1);
        
        String s2 = " Uno dosuno ";
        s2 = s2.trim();
        System.out.println(s2);
    }
    
}
