/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s05_3_ex6;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S05_3_Ex6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[][][] v = new int[3][3][3];
        int i,j,k,a = 1;
        for(i = 0; i < v.length; i++){
            for(j = 0; j < v[i].length; j++){
                for(k = 0; k < v[i][j].length; k++){
                    v[i][j][k] = a;
                    a++;
                }
            }
        }
        a = 0;
        for(i = 0; i < v.length; i++){
            for(j = 0; j < v[i].length; j++){
                for(k = 0; k < v[i][j].length; k++){
                    a += v[i][j][k];
                }
            }
        }
        System.out.println("La suma del vector v[3][3][3] es: " + a);
    }
    
}
