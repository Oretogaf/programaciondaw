/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_1_ex3;

/**
 *
 * @author Mati
 */
public class Cadena2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s1, s2, s3, s5;
        String s4 = new String("uno");
        s1 = new String ("Uno dos uno");
        s2 = new String("Uno dos unO");
        s3 = new String("Uno");
        s5 = new String ("Uno");
        System.out.println(s1 + ".equals(" + s2 + ") = " + s1.equals(s2));
        System.out.println(s2 + ".equals(" + s3 + ") = " + s2.equals(s3));
        System.out.println(s3 + ".equals(" + s4 + ") = " + s3.equals(s4));
        System.out.println(s4 + ".equals(" + s5 + ") = " + s4.equals(s5));
        System.out.println(s5 + ".equals(" + s1 + ") = " + s5.equals(s1));
        System.out.println("");
        
        System.out.println(s1 + ".equalsIgnoreCase(" + s2 + ") = " + s1.equalsIgnoreCase(s2));
        System.out.println(s2 + ".equalsIgnoreCase(" + s3 + ") = " + s2.equalsIgnoreCase(s3));
        System.out.println(s3 + ".equalsIgnoreCase(" + s4 + ") = " + s3.equalsIgnoreCase(s4));
        System.out.println(s4 + ".equalsIgnoreCase(" + s5 + ") = " + s4.equalsIgnoreCase(s5));
        System.out.println(s5 + ".equalsIgnoreCase(" + s1 + ") = " + s5.equalsIgnoreCase(s1));
        System.out.println("");
        
        System.out.println(s1 + ".startsWith(" + s2 + ") = " + s1.startsWith(s2));
        System.out.println(s2 + ".startsWith(" + s3 + ") = " + s2.startsWith(s3));
        System.out.println(s3 + ".startsWith(" + s4 + ") = " + s3.startsWith(s4));
        System.out.println(s4 + ".startsWith(" + s5 + ") = " + s4.startsWith(s5));
        System.out.println(s5 + ".startsWith(" + s1 + ") = " + s5.startsWith(s1));
        System.out.println("");
        
        System.out.println(s1 + ".endsWith(" + s2 + ") = " + s1.endsWith(s2));
        System.out.println(s2 + ".endsWith(" + s3 + ") = " + s2.endsWith(s3));
        System.out.println(s3 + ".endsWith(" + s4 + ") = " + s3.endsWith(s4));
        System.out.println(s4 + ".endsWith(" + s5 + ") = " + s4.endsWith(s5));
        System.out.println(s5 + ".endsWith(" + s1 + ") = " + s5.endsWith(s1));
        System.out.println("");
        
        System.out.println(s1 + ".compareTo(" + s2 + ") = " + s1.compareTo(s2));
        System.out.println(s2 + ".compareTo(" + s3 + ") = " + s2.compareTo(s3));
        System.out.println(s3 + ".compareTo(" + s4 + ") = " + s3.compareTo(s4));
        System.out.println(s4 + ".compareTo(" + s5 + ") = " + s4.compareTo(s5));
        System.out.println(s5 + ".compareTo(" + s1 + ") = " + s5.compareTo(s1));
        System.out.println("");
        
        System.out.println(s1 + " == " + s2 + " = " + (s1 == s2));
        System.out.println(s2 + " == " + s3 + " = " + (s2 == s3));
        System.out.println(s3 + " == " + s4 + " = " + (s3 == s4));
        System.out.println(s4 + " == " + s5 + " = " + (s4 == s5));
        System.out.println(s5 + " == " + s1 + " = " + (s5 == s1));
        System.out.println("");
    }
    
}
