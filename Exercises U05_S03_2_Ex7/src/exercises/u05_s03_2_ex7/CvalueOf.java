/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_2_ex7;

/**
 *
 * @author Mati
 */
public class CvalueOf {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s1;
        int i;
        char datos[];
        boolean log;
        char c;
        float f;
        datos = new char[4];
        for (i=0; i<4; i++)
            datos[i] = 'a';
        //Convierte el vector datos a tipo String e imprimelo por pantalla
        s1 = String.valueOf(datos);
        log = true;
        //Convierte la variable booleana log a String e imprime su valor por pantalla
        s1 = String.valueOf(log);
        c ='a';
        //Convierte el char c a String e imprime por pantalla el resultado
        s1 = String.valueOf(c);
        i = 123;
        //Convierte la variable entera i a String y muestra su valor por pantalla
        s1 = String.valueOf(i);
        f = (float)54.7;
        //Conviere la variable de tipo float a String e imprime su valor por pantalla
        s1 = String.valueOf(f);
    }
    
}
