/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewexercisesarrays_ex6;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ReviewExercisesArrays_Ex6 {
    static String[] kindOfCandy = {"Mints", "Chocolates with nuts", "Chewy chocolates", "Dark chocolate creams", "Sugar-free suckers"};
    static Scanner uin = new Scanner(System.in);
    
    static int[] combineOrders(int[] order1, int[] order2) {
        int[] order = new int[5];
        for (int i = 0; i < order.length; i++) {
            order[i] = order1[i] + order2[i];
        }
        return order;
    }
    static int[] makeOrder(){
        int[] order = new int[5];
        for (int i = 0; i < kindOfCandy.length; i++) {
            do{
                System.out.println("Introduce el numero de cajas de " + kindOfCandy[i]);
                order[i] = uin.nextInt();
            }while(order[i] < 0);
        }
        return order;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] order1 = new int[5];
        int[] order2 = new int[5];
        int[] orderT = new int[5];
        
        order1 = makeOrder();
        System.out.println();
        order2 = makeOrder();
        
        orderT = combineOrders(order1, order2);
        System.out.print("{");
        for (int i = 0; i < orderT.length; i++) {
            if(i == 0){
                System.out.print(orderT[i]);
            }
            else{
                System.out.print(", " + orderT[i]);
            }
        }
        System.out.println("}");
    }
    
}
