/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package powerfailureexception;

/**
 *
 * @author Mati
 */
public class PowerFailureException extends Exception{
    private String message;
    public PowerFailureException(){
        super("Power Failure!");
        message = "Power Failure!";
    }
    public PowerFailureException(String message){
        super(message);
        this.message = message;
    }
    public void setMessage(String message){
        this.message = message;
    }
    @Override
    public String getMessage(){
        return message;
    }
}
