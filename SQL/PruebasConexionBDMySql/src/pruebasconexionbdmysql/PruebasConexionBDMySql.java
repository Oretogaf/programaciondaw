/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package pruebasconexionbdmysql;

import com.mysql.jdbc.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Oretogaf
 */
public class PruebasConexionBDMySql {
    
    public static void main(String[] args) {
        String url = "jdbc:mysql://127.0.0.1:3306/test";
        String username = "root";
        String password = "";
        
        System.out.println("Connecting database...");
        System.out.println("Database connected!");
        for (int i = 0; i <= 1000; i++) {
            try(Connection conn = (Connection) DriverManager.getConnection(url, username, password)) {
                String query = "insert into julionumero (campo) values (?)";
                PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
                preparedStmt.setString(1, Integer.toString(i));
                preparedStmt.execute();
                conn.close();
            } catch (SQLException e){
                throw new IllegalStateException("Cannot connect the database!", e);
            }
        }
        
        try (Connection conn = (Connection) DriverManager.getConnection(url, username, password)) {
            System.out.println("Database connected!");
            String query = "select * from julionumero";
            try (Statement st = (Statement) conn.createStatement()) {
                ResultSet rs = st.executeQuery(query);
                while(rs.next()){
                    System.out.println(rs.getString(1));
                }
            }
            conn.close();
        } catch (Exception e) {
            throw new IllegalStateException("Cannot connect the database!", e);
        }
    }
}
