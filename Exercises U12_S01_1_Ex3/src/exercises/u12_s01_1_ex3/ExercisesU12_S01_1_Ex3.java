/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s01_1_ex3;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Mati
 */
public class ExercisesU12_S01_1_Ex3 {
    public static boolean noNull(Set<?> a){
        boolean enc = false;
        Iterator it = a.iterator();
        while (it.hasNext()) {
            if(it.next() == null){
                it.remove();
                enc = true;
            }
        }
        return enc;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HashSet<String> hashSet = new HashSet();
        hashSet.add(null);
        hashSet.add("Julio");
        
        System.out.println(hashSet);
        
        noNull(hashSet);

        System.out.println(hashSet);
    }
    
}
