/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package refactorizando_con_netbeans;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mati
 */
public class FechaTest {
    
    public FechaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valida method, of class Fecha.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Fecha instance = new Fecha(20,6,2008);
        Fecha instance1 = new Fecha(21,0,3000);
        Fecha instance2 = new Fecha(21,13,3000);
        Fecha instance3 = new Fecha(0,11,2000);
        Fecha instance4 = new Fecha(32,11,2000);
        Fecha instance5 = new Fecha(31,11,2000);
        Fecha instance6 = new Fecha(31,12,2000);
        Fecha instance7 = new Fecha(30,2,2008);
        Fecha instance8 = new Fecha(29,2,2008);
        Fecha instance9 = new Fecha(29,2,2000);
        Fecha instance10 = new Fecha(29,2,2007);
        Fecha instance11 = new Fecha(29,2,1900);
        
        boolean expResult = true;
        boolean expResult1 = false;
        boolean expResult2 = false;
        boolean expResult3 = false;
        boolean expResult4 = false;
        boolean expResult5 = false;
        boolean expResult6 = true;
        boolean expResult7 = false;
        boolean expResult8 = true;
        boolean expResult9 = true;
        boolean expResult10 = false;
        boolean expResult11 = false;
        
        boolean result = instance.valida();
        boolean result1 = instance1.valida();
        boolean result2 = instance2.valida();
        boolean result3 = instance3.valida();
        boolean result4 = instance4.valida();
        boolean result5 = instance5.valida();
        boolean result6 = instance6.valida();
        boolean result7 = instance7.valida();
        boolean result8 = instance8.valida();
        boolean result9 = instance9.valida();
        boolean result10 = instance10.valida();
        boolean result11 = instance11.valida();
        
        assertEquals(expResult, result);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
        assertEquals(expResult6, result6);
        assertEquals(expResult7, result7);
        assertEquals(expResult8, result8);
        assertEquals(expResult9, result9);
        assertEquals(expResult10, result10);
        assertEquals(expResult11, result11);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of main method, of class Fecha.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        Fecha.main(args);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
