/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package refactorizando_con_netbeans;

/**
 *
 * @author Mati
 */
public class Fecha {
    private int dia;
    private int mes;
    private int anio;
    public Fecha(int dia, int mes, int anio){
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
    }
    public boolean valida(){
        if(dia < 1 || dia > 31){
            return false;
        }
        if(mes < 1 || mes > 12){
            return false;
        }
        int diasMes = dias_del_mes();
        if(dia > diasMes){
            return false;
        }else{
            return true;
        }
    }

    private int dias_del_mes() {
        int diasMes = 0;
        switch(mes){
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                diasMes = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                diasMes = 30;
                break;
            case 2:
                diasMes = es_bisiesto();
                break;
        }
        return diasMes;
    }

    private int es_bisiesto() {
        int diasMes;
        if((anio % 400 == 0) || ((anio % 4 == 0) && (anio % 100 != 0))){
            diasMes = 29;
        }else{
            diasMes = 28;
        }
        return diasMes;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
