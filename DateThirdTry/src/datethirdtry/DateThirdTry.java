/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datethirdtry;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class DateThirdTry {
    private int day;
    private String month;
    private int numberMonth;
    private int year;
    
    Scanner uin = new Scanner(System.in);
    
    public void setDate(int d, int m, int y){
        day=d;
        month=monthString(m);
        year=y;
    }
    public String monthString(int numberMonth){
        switch(numberMonth){
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                System.out.println("Fatal Error");
                System.exit(0);
        }
        return "fatal error";
    }
    public void advanceYear(int aY){
        year+=aY;
    }
    
    
    //METODOS COPIADOS DE LA CLASE DateSecondTry
    public void writeOutput(){
        System.out.println(month+" "+day+", "+year);
    }
    public void readInput(){
        Scanner stdin = new Scanner(System.in);
        System.out.println("Introduce de month, day and year");
        month=stdin.next();
        day=stdin.nextInt();
        year=stdin.nextInt();
    }
    public int getDay(){
        return day;
    }
    public int getYear(){
        return year;
    }
    public int getMonth(){
        if(month.equalsIgnoreCase("january")){
            numberMonth=1;
            return 1;
        }else if(month.equalsIgnoreCase("february")){
            numberMonth=2;
            return 2;
        }else if(month.equalsIgnoreCase("march")){
            numberMonth=3;
            return 3;
        }else if(month.equalsIgnoreCase("april")){
            numberMonth=4;
            return 4;
        }else if(month.equalsIgnoreCase("may")){
            numberMonth=5;
            return 5;
        }else if(month.equalsIgnoreCase("june")){
            numberMonth=6;
            return 6;
        }else if(month.equalsIgnoreCase("july")){
            numberMonth=7;
            return 7;
        }else if(month.equalsIgnoreCase("august")){
            numberMonth=8;
            return 8;
        }else if(month.equalsIgnoreCase("september")){
            numberMonth=9;
            return 9;
        }else if(month.equalsIgnoreCase("october")){
            numberMonth=10;
            return 10;
        }else if(month.equalsIgnoreCase("november")){
            numberMonth=11;
            return 11;
        }else if(month.equalsIgnoreCase("december")){
            numberMonth=12;
            return 12;
        }else{
            System.out.println("Fatal error");
            System.exit(0);
            return 0;
        }
    }
    public int getNextYear(){
        return year+1;
    }
    public void happyGreeting(){
        int i;
        for(i=1;i<=day;i++){
            System.out.println("Happy Days!");
        }
    }
    //FIN COPIA METODOS DateSecondTry
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
