/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s03_2_ex17;

/**
 *
 * @author Mati
 */
public class TarjetaDeVisita extends Object {
    private String nombre = "Julio";
    private String empresa = "IES Juan de Garay";
    private String puerto = "Estudiante";
    private String telefono = "685434835";
    private String direccion = "Maestro Miguel Galan";

    public TarjetaDeVisita() {
        
    }

    @Override
    public String toString() {
        return "TarjetaDeVisita{" + "nombre=" + nombre + ", empresa=" + empresa + ", puerto=" + puerto + ", telefono=" + telefono + ", direccion=" + direccion + '}';
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
