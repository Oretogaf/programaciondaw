/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u07_s02_1_ex4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Alumno extends Persona{
    protected float nota;
    protected String curso;
    
    public Alumno(float nota, String curso, String nombre, int edad) {
        super(nombre, edad);
        this.nota = nota;
        this.curso = curso;
    }
    
    
    public float getNota() {
        return nota;
    }
    public String getCurso() {
        return curso;
    }
    public void setNota(float nota) {
        this.nota = nota;
    }
    public void setCurso(String curso) {
        this.curso = curso;
    }

    @Override
    public String toString() {
        return super.toString() + "\nAlumno{" + "curso=" + curso + ", nota=" + nota + '}';
    }
    
}
