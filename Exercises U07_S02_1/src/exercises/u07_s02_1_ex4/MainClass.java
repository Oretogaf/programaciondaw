/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u07_s02_1_ex4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class MainClass {
    public static Scanner uin = new Scanner(System.in);
    public static void main(String[] args) {
        String nombre;
        int edad;
        float nota;
        String curso;
        String titulo;
        int experiencia;
        
        System.out.println("Introduce el nombre del profesor");
        nombre = uin.nextLine();
        System.out.println("Introduce el titulo");
        titulo = uin.nextLine();
        System.out.println("Introduce la edad");
        edad = uin.nextInt();
        System.out.println("Introduce los años de experiencia");
        experiencia = uin.nextInt();
        Profesor profesor = new Profesor(titulo, experiencia, nombre, edad);
        
        System.out.println();
        uin.nextLine();
        
        System.out.println("Introduce el nombre del alumno");
        nombre = uin.nextLine();
        System.out.println("Introduce el curso");
        curso = uin.nextLine();
        System.out.println("Introduce la edad");
        edad = uin.nextInt();
        System.out.println("Introduce la nota");
        nota = uin.nextFloat();
        Alumno alumno = new Alumno(nota, curso, nombre, edad);
        
        System.out.println(profesor);
        System.out.println();
        System.out.println(alumno);
    }
}
