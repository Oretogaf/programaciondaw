/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s02_1_ex4;

/**
 *
 * @author Mati
 */
public class Profesor extends Persona{
    protected String titulo;
    protected int experiencia;

    public Profesor(String titulo, int experiencia, String nombre, int edad) {
        super(nombre, edad);
        this.titulo = titulo;
        this.experiencia = experiencia;
    }

    public String getTitulo() {
        return titulo;
    }
    public int getexperiencia() {
        return experiencia;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public void setexperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    @Override
    public String toString() {
        return super.toString() + "\nProfesor{" + "titulo=" + titulo + ", experiencia=" + experiencia + '}';
    }
        
}
