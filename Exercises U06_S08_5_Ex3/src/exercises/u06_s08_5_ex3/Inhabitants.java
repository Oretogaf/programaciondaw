/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_5_ex3;

/**
 *
 * @author Mati
 */
public class Inhabitants {
    private String name;
    private String breed;
    private int category;
    private String character;

    public Inhabitants(String name, String breed, int category, String character) {
        this.name = name;
        if(probeBreed(breed)){
            this.breed = breed;            
        }else{
            System.out.println("Fatal error");
            System.exit(0);
        }
        this.category = category;
        if(probeCharacter(character)){
            this.character = character;            
        }else{
            System.out.println("Fatal error");
            System.exit(0);
        }
    }

    public String getName() {
        return name;
    }
    public String getBreed() {
        return breed;
    }
    public int getCategory() {
        return category;
    }
    public String getCharacter() {
        return character;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setBreed(String breed) {
        if(probeBreed(breed)){
            this.breed = breed;            
        }else{
            System.out.println("Fatal error");
            System.exit(0);
        }
    }
    public void setCategory(int category) {
        this.category = category;
    }
    public void setCharacter(String character) {
        if(probeCharacter(character)){
            this.character = character;            
        }else{
            System.out.println("Fatal error");
            System.exit(0);
        }
    }
    
    public static boolean probeBreed(String newBreed){
        return newBreed.equalsIgnoreCase("dwarf") ||
                newBreed.equalsIgnoreCase("hobbit") ||
                newBreed.equalsIgnoreCase("elf") ||
                newBreed.equalsIgnoreCase("orcs");
    }
    public static boolean probeCharacter(String newCharacter){
        return newCharacter.equalsIgnoreCase("good") ||
                newCharacter.equalsIgnoreCase("bad");
    }

    @Override
    public String toString() {
        return "Inhabitants{" + "name=" + name + ", breed=" + breed + ", category=" + category + ", character=" + character + '}';
    }
    
}
