/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_5_ex3;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Census {
    public static Inhabitants[] census;
    public static Scanner uin = new Scanner(System.in);
    
    public static void iniciateArray(){
        int number;
        do{
            System.out.println("Introduce el numero de habitantes");
            number = uin.nextInt();
            if(number > 1000000 || number < 0){
                System.out.println("El numero introducido no es correcto");
            }
        }while(number > 1000000 || number < 0);
        census = new Inhabitants[number];
    }
    private static void menu(){
        int index;
        System.out.println("1.Introducir datos de un habitante\n"
                + "2.Ver información de los habitantes de una raza\n"
                + "3.Ver el habitante supremo de una raza\n"
                + "4.Salir");
        index = uin.nextInt();
        System.out.println();
        switch(index){
            case 1:
                introducirDatos();
                break;
            case 2:
                imprimirDatos();
                break;
            case 3:
                verJefeRaza();
                break;
            case 4:
                System.out.println();
                System.out.println("Goodbye!!");
                System.exit(0);
                break;
            default:
                System.out.println("Fatal error");
                System.exit(0);
                break;
        }
        System.out.println();
        menu();
    }
    private static void introducirDatos(){
        String name;
        String breed;
        int category;
        String character;
        for (int i = 0; i < census.length; i++) {
            System.out.println("Introduce el nombre del habitante " + (i + 1));
            name = uin.next();
            do{
                System.out.println("Introduce la raza del habitante " + (i + 1));
                breed = uin.next();
                if(!Inhabitants.probeBreed(breed)){
                    System.out.println("No existe la raza introducida");
                }
            }while(!Inhabitants.probeBreed(breed));
            System.out.println("Introduce el rango del habitante " + (i + 1));
            category = uin.nextInt();
            do{
                System.out.println("Introduce el caracter del habitante " + (i + 1));
                character = uin.next();
                if(!Inhabitants.probeCharacter(character)){
                    System.out.println("El caracter introducido no es correcto");
                }
            }while(!Inhabitants.probeCharacter(character));
        }
    }
    private static void imprimirDatos(){
        String breed;
        do{
            System.out.println("Introduce el nombre de la raza");   
            breed = uin.next();
        }while(!Inhabitants.probeBreed(breed));
        for (Inhabitants habBreed : census) {
            if(habBreed.getBreed() == breed){
                habBreed.toString();
            }
        }
    }
    private static void verJefeRaza(){
        String breed;
        int indexMax = Integer.MIN_VALUE;
        int catMax = Integer.MIN_VALUE;
        do{
            System.out.println("Introduce el nombre de la raza");   
            breed = uin.next();
        }while(!Inhabitants.probeBreed(breed));
        for (int i = 0; i < census.length; i++) {
            if(census[i].getBreed() == breed){
                if(census[i].getCategory() > catMax){
                    catMax = census[i].getCategory();
                    indexMax = i;
                }
            }
        }
        System.out.println(census[indexMax].toString());
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        iniciateArray();
        menu();
    }
    
}
