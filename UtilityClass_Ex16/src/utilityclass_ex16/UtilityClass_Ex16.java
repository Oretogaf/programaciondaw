/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilityclass_ex16;


public class UtilityClass_Ex16
{
    /**
     Returns the largest of any number of int values.
    */
   public static int max(int... arg)
    {
        if (arg.length == 0)
        {
           System.out.println("Fatal Error: maximum of zero values.");
           System.exit(0);
        }

        int largest = arg[0];
        for (int ar : arg)
            if (ar > largest)
                largest = ar;
        return largest;
    }
}

