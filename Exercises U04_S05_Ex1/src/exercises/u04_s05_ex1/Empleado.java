/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s05_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Empleado{
    private String nombre;
    private float sueldo;
    public Empleado(){
        
    }
    public Empleado(String nom, float salario){
        nombre=nom;
        sueldo=salario;
    }
    public void pedirDatosEmpleado(){
        System.out.println("Introduce el nombre del empleado");
        nombre=uin.next();
        System.out.println("Introduce el sueldo del empleado");
        sueldo=uin.nextFloat();
    }
    public void visuDatosEmpleado(){
        System.out.println("El nombre del empleado es "+nombre+" y su sueldo "+sueldo);
    }
    public void asignarSueldo(float s){
        sueldo=s;
    }
    public float devolverSueldo(){
        return sueldo;
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String nom;
        float salario;
        Empleado emple1 = new Empleado();
        emple1.pedirDatosEmpleado();
        System.out.println("");
        System.out.println("Introduce el nombre del empleado");
        nom=uin.next();
        System.out.println("Introduce el sueldo");
        salario=uin.nextFloat();
        Empleado emple2 = new Empleado(nom,salario);
        System.out.println("");
        emple1.visuDatosEmpleado();
        System.out.println("");
        emple2.visuDatosEmpleado();
        System.out.println("");
        System.out.println("Introduce el salario del empleado 2");
        salario=uin.nextFloat();
        emple2.asignarSueldo(salario);
        System.out.println("");
        System.out.println(emple2.devolverSueldo());
        System.out.println("");
        emple2.visuDatosEmpleado();
    }
    
}
