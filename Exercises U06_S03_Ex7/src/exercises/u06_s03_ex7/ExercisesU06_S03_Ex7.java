/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s03_ex7;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S03_Ex7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] origen = {10,50,16,40,37,45,90,76,78,81,58,32};
        int[] destino;
        int i;
        int c = 0;
        int d = 0;
        for(i = 0; i < origen.length; i++){
            if(origen[i] > 25 && origen[i] % 2 == 0){
                c++;
            }
        }
        destino = new int[c];
        for(i = 0; i < origen.length; i++){
            if(origen[i] > 25 && origen[i] % 2 == 0){
                destino[d] = origen[i];
                d++;
            }
        }
        for(i = 0;i < origen.length; i++){
            System.out.println("origen[" + i + "] = " + origen[i]);
        }
        System.out.println("");
        for(i = 0; i < destino.length; i++){
            System.out.println("destino[" + i + "] = " + destino[i]);
        }
    }
    
}
