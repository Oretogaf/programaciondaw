/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s01_6_ex41;

/**
 *
 * @author Mati
 */
public class Person {
    private String name;
    private Date born;
    private Date died;
    public Person(Person original)
    {
        if (original == null)
        {
            System.out.println("Fatal error.");
            System.exit(0);
        }
        name = original.name;
        born = new Date(original.born);
        if (original.died == null)
            died = null;
        else
            died = new Date(original.died);
    }
    public Person(String initialName, Date birthDate, Date deathDate)
    {
        if (consistent(birthDate, deathDate))
            { name = initialName;
            born = new Date(birthDate);
                if (deathDate == null)
                    died = null;
                else
                    died = new Date(deathDate);
               }
        else
            { System.out.println("Inconsistent dates.");
            System.exit(0);
            }
    }
    private static boolean consistent(Date birthDate, Date deathDate)
    {
        if (birthDate == null) return false;
        else if (deathDate == null) return true;
        else return (birthDate.precedes(deathDate) || birthDate.equals(deathDate));
    }
    public boolean equals(Person otherPerson)
    {
        if (otherPerson == null)
            return false;
        else
            return (name.equals(otherPerson.name) &&
                born.equals(otherPerson.born) &&
                datesMatch(died, otherPerson.died));
    }
    private static boolean datesMatch(Date date1, Date date2)
        {
        if (date1 == null)
            return (date2 == null);
        else if (date2 == null) //&& date1 != null
            return false;
        else // both dates are not null.
            return(date1.equals(date2));
        }
    @Override
    public String toString( )
    {
        String diedString;
        if (died == null)
            diedString = ""; //Empty string
        else
            diedString = died.toString( );
        return (name + ", " + born + "-" + diedString);
    }
    public void setName(String nam){
        name = nam;
    }
    public void setBorn(Date bor){
        if(consistent(bor, null)){
            born = new Date(bor);
        }else{
            System.out.println("Fatal Error");
            System.exit(0);
        }
    }
    public void setDied(Date die){
        if(consistent(born, die)){
            died = new Date(die);
        }else{
            System.out.println("Fatal error");
            System.exit(0);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Person p1 = new Person("Julio", new Date(06, 26, 1997), null);
        Person p2 = new Person(p1);
        p2.setBorn(new Date(8, 24, 1999));
        p2.setDied(new Date(1, 23, 2016));
        p2.setName("Macrina");
        System.out.println(p1.toString());
        System.out.println(p2.toString());
    }
    
}
