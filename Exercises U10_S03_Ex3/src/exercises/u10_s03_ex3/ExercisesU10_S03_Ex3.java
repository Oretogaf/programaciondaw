/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u10_s03_ex3;

import java.io.*;
import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU10_S03_Ex3 implements Serializable{
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File f = new File("palabra.dat");
        ObjectOutputStream outObj = null;
        ObjectInputStream inObj = null;
        Scanner uin = new Scanner(System.in);
        
        if(f.exists()){
            try{
                inObj = new ObjectInputStream(new FileInputStream(f));
            }
            catch (IOException ex)
            {
                System.out.println("Error al acceder al fichero");
            }
            
            String p = "";
            try{
                p = inObj.readUTF();
            }
            catch (IOException ex){
                System.out.println("Se produjo un error al intentar leer el fichero");
                System.out.println(ex);
            }
            
            System.out.println("La ultima palabra fue \"" + p + "\"");
            while(true){
                System.out.println("Introduce la nueva palabra");
                String np = uin.next();
                
                if(p.substring(p.length() - 1).equalsIgnoreCase(np.substring(0,1))){
                    writeFile(outObj, f, np);
                    System.exit(0);
                }else{
                    System.out.println("Error, elige otra palabra");
                }
            }
        }else{
            System.out.println("¿Palabra inicial?");
            String p = uin.next();
            
            writeFile(outObj, f, p);
        }
    }
    
    private static void writeFile(ObjectOutputStream outObj, File f, String p) {
        try{
            outObj = new ObjectOutputStream(new FileOutputStream(f));
        }
        catch (IOException ex)
        {
            System.out.println("Error al acceder al fichero");
        }
        
        try{
            outObj.writeUTF(p);
            outObj.flush();
            outObj.close();
        }
        catch (IOException ex)
        {
            System.out.println("No se pudo escribir en el fichero");
        }
    }
}
