/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package pruebas;

import objetosNegocio.*;

/**
 *
 * @author Mati
 */
public class Prueba {
    
    /** Creates a new instance of Prueba */
    public Prueba() {
    }
    
    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // Se crean dos canciones
        Cancion cancion1 = new Cancion("C0001", "The long and winding road",
                "Balada", "The Beatles", "John Lennon","Paul McCartney",
                "Let it be", "Apple",3, new Fecha(24, 3, 1970));
        
        Cancion cancion2 = new Cancion("C0002", "Garota de Ipanema","Bossanova",
                "Los Indios Tabajaras","Antonio Carlos Jobim",
                "Antonio Carlos Jobim","Bossanova Jazz Vol. 1", "Prodisc", 3,
                new Fecha(1, 12, 1970));
        
        Cancion cancion3 = new Cancion("C0003", "Desafinado", "Bossanova",
                "Joao Gilberto", "Joao Gilberto","Joao Gilberto",
                "Bossanova Jazz Vol. 1", "Prodisc", 3,new Fecha(3, 12, 1980));
        
        // Se despliegan los datos de la canción 1
        System.out.println("Cancion 1");
        System.out.println(cancion1);
        
        // Se despliegan los datos de la canción 2
        System.out.println("Cancion 2");
        System.out.println(cancion2);
        
        // Se despliegan los datos de la canción 3
        System.out.println("Cancion 3");
        System.out.println(cancion3);
        
        // Se despliega el titulo de la canción 1
        System.out.print("Titulo de la canción 1: ");
        System.out.println(cancion1.getTitulo());
        
        // Se cambia el autor de la música de la canción 3
        cancion3.setAutorMusica("Antonio Carlos Jobim");
        
        // Se despliegan los datos de la canción 3
        System.out.println("Cancion 3");
        System.out.println(cancion3);
        
    }
    
}
