/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package objetosNegocio;

import java.util.Vector;
import objetosNegocio.Fecha;

/**
 *
 * @author Mati
 */
public class Cancion extends Medio{
    protected String interprete;
    protected String autorLetra;
    protected String autorMusica;
    protected String album;
    protected String disquera;
    
    /** Creates a new instance of Medio */
    public Cancion() {
        super();
    }
    
    public Cancion(String clave, String titulo, String genero,String interprete, String autorLetra, String autorMusica,String album, String disquera, int duracion, Fecha fecha)
    {
        super(clave, titulo, genero, duracion, fecha);
        this.interprete = interprete;
        this.autorLetra = autorLetra;
        this.autorMusica = autorMusica;
        this.album = album;
        this.disquera = disquera;
    }
    public Cancion(String clave) {
        this(clave, null, null, null, null, null, null, null, 0, null);
    }
    
    @Override
    public String toString() {
        return super.toString() + ", " + getInterprete() + ", " + getAutorLetra() + ", "
                + getAutorMusica() + ", " + getAlbum() + ", " + getDisquera();
    }

    /**
     * @return the interprete
     */
    public String getInterprete() {
        return interprete;
    }

    /**
     * @return the autorLetra
     */
    public String getAutorLetra() {
        return autorLetra;
    }

    /**
     * @return the autorMusica
     */
    public String getAutorMusica() {
        return autorMusica;
    }

    /**
     * @return the album
     */
    public String getAlbum() {
        return album;
    }

    /**
     * @return the disquera
     */
    public String getDisquera() {
        return disquera;
    }

    /**
     * @param interprete the interprete to set
     */
    public void setInterprete(String interprete) {
        this.interprete = interprete;
    }

    /**
     * @param autorLetra the autorLetra to set
     */
    public void setAutorLetra(String autorLetra) {
        this.autorLetra = autorLetra;
    }

    /**
     * @param autorMusica the autorMusica to set
     */
    public void setAutorMusica(String autorMusica) {
        this.autorMusica = autorMusica;
    }

    /**
     * @param album the album to set
     */
    public void setAlbum(String album) {
        this.album = album;
    }

    /**
     * @param disquera the disquera to set
     */
    public void setDisquera(String disquera) {
        this.disquera = disquera;
    }
    
}
