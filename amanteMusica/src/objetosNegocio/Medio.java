/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package objetosNegocio;

import objetosNegocio.Fecha;

/**
 *
 * @author Mati
 */
public class Medio {
    protected String clave;
    protected String titulo;
    protected String genero;
    protected int duracion;
    protected Fecha fecha;
    
    /** Creates a new instance of Medio */
    public Medio() {}
    
    public Medio(String clave, String titulo, String genero, int duracion, Fecha fecha) {
        this.clave = clave;
        this.titulo = titulo;
        this.genero = genero;
        this.duracion = duracion;
        this.fecha = fecha;
    }
    
    @Override
    public String toString() {
        return getClave() + ", " + getTitulo() + ", " + getGenero() + ", " + getDuracion() + ", " + getFecha();
    }
    
    /**
     * @return the clave
     */
    public String getClave() {
        return clave;
    }
    
    /**
     * @param clave the clave to set
     */
    public void setClave(String clave) {
        this.clave = clave;
    }
    
    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }
    
    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    /**
     * @return the genero
     */
    public String getGenero() {
        return genero;
    }
    
    /**
     * @param genero the genero to set
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    /**
     * @return the duracion
     */
    public int getDuracion() {
        return duracion;
    }
    
    /**
     * @param duracion the duracion to set
     */
    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
    
    /**
     * @return the fecha
     */
    public Fecha getFecha() {
        return fecha;
    }
    
    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Fecha fecha) {
        this.fecha = fecha;
    }
    
}
