/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s05_ex5;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Gastos {
    private float gastosAlimentacion;
    private float gastosTransporte;
    private float gastosVivienda;
    private boolean gastosEnEuros;
    private float tipoCambio;
    
    //Inicialización de las variables
    
    //Creación de un objeto para la entrada de datos por teclado
    public static Scanner uin = new Scanner(System.in);
    
    //Creación de un constructor
    public Gastos(){
        gastosAlimentacion=0;
        gastosTransporte=0;
        gastosVivienda=0;
        gastosEnEuros=true;
        tipoCambio=(float)0.9;
    }
    
    //Metodos
        //Para añadir gastos
        public void añadeGastoAlimentacion(float cantidadAlimentacion){
            gastosAlimentacion+=cantidadAlimentacion;
        }
        public void añadeGastoTransporte(float cantidadTransporte){
            gastosTransporte+=cantidadTransporte;
        }
        public void añadeGastoVivienda(float cantidadVivienda){
            gastosVivienda+=cantidadVivienda;
        }
    
        //Para el valor de cambio
        public void cambiaTipoCambio(float nuevoValor){
            tipoCambio=nuevoValor;
        }
    
        //Para saber si esta en euros
        public boolean estaEnEuros(){
            return gastosEnEuros;
        }
    
        //Para reiniciar los variables a los valures por defecto
        public void eliminaTodosLosGastos(){
            gastosAlimentacion=0;
            gastosTransporte=0;
            gastosVivienda=0;
        }
    
        //Para calcular el total de los gastos
            //En euros
            public float calculaTotalGastosEuros(){
                float gastos;
                float inter;
                gastos = 0;
                if(estaEnEuros()==true){
                    gastos=gastosAlimentacion+gastosTransporte+gastosVivienda;
                }else{
                    inter=gastosAlimentacion+gastosTransporte+gastosVivienda;
                    gastos=inter*tipoCambio;
                }
                return gastos;
            }
    
            //En dolares
            public float calculaTotalGastosDolares(){
                float gastos;
                float inter;
                gastos=0;
                if(estaEnEuros()==true){
                    inter=gastosAlimentacion+gastosTransporte+gastosVivienda;
                    gastos=inter/tipoCambio;
                }else{
                    gastos=gastosAlimentacion+gastosTransporte+gastosVivienda;
                }
                return gastos;
            }
    
        //Para cambiar los datos
            //A euros
            public void cambiaAEuros(){
                float inter;
                if(estaEnEuros()==true){
                    System.out.println("Los gastos ya estan en Euros");
                }
                else{
                    inter=gastosAlimentacion;
                    gastosAlimentacion=inter*tipoCambio;
                    inter=gastosTransporte;
                    gastosTransporte=inter*tipoCambio;
                    inter=gastosVivienda;
                    gastosVivienda=inter*tipoCambio;
                }
            }
            
            //A dolores
            public void cambiaADolares(){
                float inter;
                if(estaEnEuros()==false){
                    System.out.println("Los gastos ya estan en Dolares");
                }
                else{
                    inter=gastosAlimentacion;
                    gastosAlimentacion=inter/tipoCambio;
                    inter=gastosTransporte;
                    gastosTransporte=inter/tipoCambio;
                    inter=gastosVivienda;
                    gastosVivienda=inter/tipoCambio;
                }
            }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float cantidadAlimentacion;
        float cantidadTransporte;
        float cantidadVivienda;
        Gastos g1 = new Gastos();
        System.out.println("Introduce los gastos de alimentación");
        cantidadAlimentacion=uin.nextFloat();
        g1.añadeGastoAlimentacion(cantidadAlimentacion);
        System.out.println("Introduce los gastos de transporte");
        cantidadTransporte=uin.nextFloat();
        g1.añadeGastoTransporte(cantidadTransporte);
        System.out.println("Introduce los gastos de vivienda");
        cantidadVivienda=uin.nextFloat();
        g1.añadeGastoVivienda(cantidadVivienda);
        System.out.println(g1.calculaTotalGastosEuros());
        System.out.println(g1.calculaTotalGastosDolares());
        System.out.println(g1.estaEnEuros());
        g1.eliminaTodosLosGastos();
        System.out.println(g1.calculaTotalGastosEuros());
    }
    
}
