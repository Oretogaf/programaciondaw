/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s07_1_ex3;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S07_1_Ex3 {
public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int numeroFamilias;
        int numeroHijosFamilia;
        int edad;
        double sumaEdadFamilia;
        double sumaEdadTotal=0;
        int i;
        int j;
        int numeroHijosTotal=0;
        do{
            System.out.println("Introduce el numero de familias");
            numeroFamilias=uin.nextInt();
        }while(numeroFamilias<1);
        for(i=1;i<=numeroFamilias;i++){
            System.out.println("Introduce el numero de hijos de la familia "+i);
            numeroHijosFamilia=uin.nextInt();
            sumaEdadFamilia=0;
            for(j=1;j<=numeroHijosFamilia;j++){
                System.out.println("Introduce la edad del hijo "+j);
                edad=uin.nextInt();
                sumaEdadFamilia+=edad;
                numeroHijosTotal++;
            }
            System.out.println("La edad media de esta familia es "+(sumaEdadFamilia/numeroHijosFamilia));
            sumaEdadTotal+=sumaEdadFamilia;
        }
        
        System.out.println("La media de edad de todas las familias es "+(sumaEdadTotal/numeroHijosTotal));
    }
    
}
