/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u12_s04_ex2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class GestorCorreo {
    private ArrayList<Correo> buzon = new ArrayList();
    private Scanner uin = new Scanner(System.in, "iso-8859-1");
    public File fil = new File("salida.dat");

    
    public void menu(){
        System.out.println("Introduce la opcion deseada");
        System.out.println("1. Añadir mensaje");
        System.out.println("2. Eliminar mensaje");
        System.out.println("3. Imprimir mensaje");
        System.out.println("4. Salir");
        switch(uin.nextInt()){
            case 1:
                añadirMensaje(datos());
                writeMe();
                break;
            case 2:
                eliminarMensaje();
                writeMe();
                break;
            case 3:
                imprimirMensaje();
                break;
            case 4:
                System.out.println("Hasta luego");
                writeMe();
                System.exit(0);
                break;
            default:
                System.out.println("Opcion incorrecta");
                break;
        }
        uin.nextLine();
        System.out.println();
        menu();
    }
    private void añadirMensaje(Correo mail){
        buzon.add(mail);
    }
    private void eliminarMensaje(){
        verIterator();
        System.out.println();
        System.out.println("Introduce el indice del mensaje que deseas eliminar");
        int a = uin.nextInt();
        buzon.remove(a);
    }
    private Correo datos(){
        String origen, destino, asunto, mensaje;
        System.out.println("Introduce el origen");
        origen = uin.next();
        System.out.println("Introduce el destino");
        destino = uin.next();
        System.out.println("Introduce el asunto");
        asunto = uin.next();
        System.out.println("Introduce el mensaje");
        mensaje = uin.next();
        return new Correo(origen, destino, asunto, mensaje);
    }
    private void imprimirMensaje(){
        verIterator();
        
        System.out.println();
        System.out.println("Introduce el indice del mensaje que deseas ver");
        int k = uin.nextInt();
        System.out.println(buzon.get(k));
    }
    private void verIterator() {
        Iterator it = buzon.iterator();
        int i = 0;
        while (it.hasNext()) {
            Correo next = (Correo) it.next();
            System.out.println("Mensaje " + i + " {"
                    + next.getOrigen() + ", "
                    + next.getDestino() + ", "
                    + next.getAsunto() + "}");
            i++;
        }
    }
    
    private void writeMe(){
        if(!fil.exists()){
            try {
                fil.createNewFile();
            } catch (IOException ex) {
                System.out.println("Se produjo un error al crear el archivo");
                System.out.println(ex.getMessage());
            }
        }
        ObjectOutputStream out = null;
        try{
            out = new ObjectOutputStream(new FileOutputStream(fil));
        }
        catch (IOException ex)
        {
            System.out.println("Se produjo un error al intentar abrir el archivo");
            System.out.println(ex.getMessage());
        }
        
        try{
            out.writeObject(buzon);
            out.flush();
        }
        catch (IOException ex){
            System.out.println("Se produjo un error al intentar guardar los archivos");
            System.out.println(ex.getMessage());
        }
        finally
        {
            try{
                out.close();
            } catch (IOException ex){
                System.out.println("Se produjo un error al intentar cerrar el archivo");
                System.out.println(ex.getMessage());
            }
        }
    }
    public void readMe(){
        ObjectInputStream in = null;
        try{
            in = new ObjectInputStream(new FileInputStream(fil));
        }
        catch (IOException ex)
        {
            System.out.println("Se produjo un error al intentar abrir el archivo");
            System.out.println(ex.getMessage());
        }
        
        try{
            buzon = (ArrayList<Correo>) in.readObject();
        }
        catch (IOException ex){
            System.out.println("Se produjo un error al intentar leer los archivos");
            System.out.println(ex.getMessage());
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Se produjo un error al intentar encontrar la clase de los archivos");
            System.out.println(ex.getMessage());
        }
        finally
        {
            try{
                in.close();
            } catch (IOException ex){
                System.out.println("Se produjo un error al intentar cerrar el archivo");
                System.out.println(ex.getMessage());
            }
        }
    }
}
