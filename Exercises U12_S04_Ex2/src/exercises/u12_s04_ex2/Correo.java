/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s04_ex2;

import java.io.Serializable;

/**
 *
 * @author Mati
 */
public class Correo implements Serializable{
    private String origen;
    private String destino;
    private String asunto;
    private String mensaje;

    public Correo(String origen, String destino, String asunto, String mensaje) {
        this.origen = origen;
        this.destino = destino;
        this.asunto = asunto;
        this.mensaje = mensaje;
    }

    public String getOrigen() {
        return origen;
    }
    public String getDestino() {
        return destino;
    }
    public String getAsunto() {
        return asunto;
    }
    public String getMensaje() {
        return mensaje;
    }
    
    public void setOrigen(String origen) {
        this.origen = origen;
    }
    public void setDestino(String destino) {
        this.destino = destino;
    }
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
    @Override
    public String toString() {
        return "Correo{" + "origen=" + origen + ", destino=" + destino + ", asunto=" + asunto + ", mensaje=" + mensaje + '}';
    }
}
