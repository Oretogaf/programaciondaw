/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class DataSecondTry {
    private int day;
    private String month;
    private int year;
    private int numberMonth;
    
    public void writeOutput(){
        System.out.println(this.month+" "+day+", "+year);
    }
    public void readInput(){
        Scanner stdin = new Scanner(System.in);
        System.out.println("Introduce de this.month, day and year");
        this.month=stdin.next();
        day=stdin.nextInt();
        year=stdin.nextInt();
    }
    public int getDay(){
        return this.day;
    }
    public int getYear(){
        return this.year;
    }
    public int getMonth(){
        if(this.month.equalsIgnoreCase("january")){
            this.numberMonth=1;
            return 1;
        }else if(this.month.equalsIgnoreCase("february")){
            this.numberMonth=2;
            return 2;
        }else if(this.month.equalsIgnoreCase("march")){
            this.numberMonth=3;
            return 3;
        }else if(this.month.equalsIgnoreCase("april")){
            this.numberMonth=4;
            return 4;
        }else if(this.month.equalsIgnoreCase("may")){
            this.numberMonth=5;
            return 5;
        }else if(this.month.equalsIgnoreCase("june")){
            this.numberMonth=6;
            return 6;
        }else if(this.month.equalsIgnoreCase("july")){
            this.numberMonth=7;
            return 7;
        }else if(this.month.equalsIgnoreCase("august")){
            this.numberMonth=8;
            return 8;
        }else if(this.month.equalsIgnoreCase("september")){
            this.numberMonth=9;
            return 9;
        }else if(this.month.equalsIgnoreCase("october")){
            this.numberMonth=10;
            return 10;
        }else if(this.month.equalsIgnoreCase("november")){
            this.numberMonth=11;
            return 11;
        }else if(this.month.equalsIgnoreCase("december")){
            this.numberMonth=12;
            return 12;
        }else{
            System.out.println("Fatal error");
            System.exit(0);
            return 0;
        }
    }
    public int getNextYear(){
        return year+1;
    }
    public void happyGreeting(){
        int i;
        for(i=1;i<=day;i++){
            System.out.println("Happy Days!");
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DataSecondTry data = new DataSecondTry();
        int day;
        int year;
        int month;
        data.readInput();
        data.writeOutput();
        System.out.println(data.getDay());
        day=data.getDay();
        System.out.println("Day: "+day);
        month=data.getMonth();
        System.out.println("Month: "+month);
        year=data.getYear();
        System.out.println("Year: "+year);
        System.out.println(day+"/"+month+"/"+year);
        System.out.println("Next year: "+data.getNextYear());
        data.happyGreeting();
    }   
}
