/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s03_ex6;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S03_Ex6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] a1 = new int[10];
        int[] a2 = new int[10];
        int i;
        
        for(i = 0; i< a1.length; i++){
            a1[i] = 1 + (int)(Math.random() * 10);
        }
        for(i = 0; i< a1.length; i++){
            a2[i] = a1[i];
        }
        for(i = 0; i< a1.length; i++){
            System.out.println("a2[" + i + "] = " + a2[i]);
        }
    }
    
}
