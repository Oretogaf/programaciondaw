/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s02_1_ex10;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Computer {
    //Memory data
    private Integer memorySize;
    private String memoryType;
    private Integer memorySpeed;
    
    //Details of the hard drive
    private Integer hardDriveSize;
    private Integer numberRevolutionsDisk;
    
    //Data processor
    private String processorModel;
    private Integer processorSpeed;

    public void setMemoryType(String newType)
    {
        memoryType = newType;
    }
    public void setMemorySpeed(int newSpeed)
    {
        memorySpeed = newSpeed;
    }
    public void setMemorySize(int newSize)
    {
        memorySize = newSize;
    }
    public void setHardDriveSize(int newSize)
    {
        hardDriveSize = newSize;
    }
    public void setNumberRevolutionsDisk (int newValue)
    {
        numberRevolutionsDisk = newValue;
    }
    public void setProcessorModel(String newModel)
    {
        processorModel = newModel;
    }
    public void setProcessorSpeed(int newSpeed)
    {
        processorSpeed = newSpeed;
    }
    
    public String getMemoryType()
    {
        return memoryType;
    }
    public int getMemorySpeed()
    {
        return memorySpeed;
    }
    public int getMemorySize()
    {
        return memorySize;
    }
    public int getHardDriveSize()
    {
        return hardDriveSize;
    }
    public int getNumberRevolutionsDisk ()
    {
        return numberRevolutionsDisk;
    }
    public String getProcessorModel()
    {
        return processorModel;
    }
    public int getProcessorSpeed()
    {
        return processorSpeed;
    }
    @Override
    public String toString(){
        return ("Tipo de memoria "+memoryType+
                "\nTamaño de memoria "+memorySize+
                "\nVelocidad de memoria "+memorySpeed+
                "\nTamaño disco duro "+hardDriveSize+
                "\nRevoluciones disco duro "+numberRevolutionsDisk+
                "\nModelo procesador "+processorModel+
                "\nVelocidad del procesador "+processorSpeed);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int usuI;
        String usuS;
        Scanner uin = new Scanner(System.in);
        Computer obj1 = new Computer();
        System.out.println("Introduce el tipo de memoria");
        usuS = uin.next();
        obj1.setMemoryType(usuS);
        System.out.println("Introduce la velocidad de la memoria en MHz");
        usuI = uin.nextInt();
        obj1.setMemorySpeed(usuI);
        System.out.println("Introduce el tamaño de la memoria");
        usuI = uin.nextInt();
        obj1.setMemorySize(usuI);
        System.out.println("Introduce el tamaño del disco duro en GB");
        usuI = uin.nextInt();
        obj1.setHardDriveSize(usuI);
        System.out.println("Introduce las revoluciones del disco duro en rpm");
        usuI = uin.nextInt();
        obj1.setNumberRevolutionsDisk(usuI);
        System.out.println("Introduce el modelo de procesador");
        usuS = uin.next();
        obj1.setProcessorModel(usuS);
        System.out.println("Introduce la velocidad del procesador en GHz");
        usuI = uin.nextInt();
        obj1.setProcessorSpeed(usuI);
        System.out.println("Tipo de memoria "+obj1.getMemoryType());
        System.out.println("Tamaño de memoria "+obj1.getMemorySize());
        System.out.println("Velocidad de memoria "+obj1.getMemorySpeed());
        System.out.println("Tamaño disco duro "+obj1.getHardDriveSize());
        System.out.println("Revoluciones disco duro "+obj1.getNumberRevolutionsDisk());
        System.out.println("Modelo procesador "+obj1.getProcessorModel());
        System.out.println("Velocidad del procesador "+obj1.getProcessorSpeed());
        System.out.println("");
        System.out.println(obj1.toString());
    }
    
}