/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s05_2_ex2;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S05_2_Ex2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[][] diagonal = new int[5][5];
        int i,j;
        int x = 5;
        for (i = 0; i < diagonal.length; i++) {
            for (j = 0; j < diagonal[i].length; j++) {
                if(i==j){
                    diagonal[i][j] = 1;
                }else{
                    diagonal[i][j] = 0;
                }
                x--;
            }
        }
        for (i = 0; i < diagonal.length; i++) {
            for (j = 0; j < diagonal[i].length; j++) {
                System.out.print(diagonal[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
}
