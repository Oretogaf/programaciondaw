/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s06_1_ex5;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S06_1_Ex5 {
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float[] notas;
        int i,med,nMerit,nGood,nPass,nInsuf,nVerDef,nApro,nSusp,nAlu;
        float nota,apro,susp;
        do{
            System.out.println("Introduce el numero de alumnos");
            nAlu = uin.nextInt();
            if(nAlu < 0){
                System.out.println("El numero de estudiantes no puede ser menor a cero");
            }
        }while(nAlu<0);
        notas = new float[nAlu];
        for(i = 0; i < notas.length; i++){
            do{
                System.out.println("Introduce la nota del alumno " + (i + 1));
                nota = uin.nextFloat();
                if(nota < 0 || nota > 10){
                    System.out.println("Nota incorrecta");
                }
            }while(nota < 0 || nota > 10);
            notas[i] = nota;
        }
        nSusp = nApro = 0;
        for(i = 0; i < notas.length; i++){
            if(notas[i] <= 5){
                nApro++;
            }else{
                nSusp++;
            }
        }
        med = 0;
        for(i = 0; i < notas.length; i++){
            med += notas[i];
        }
        med /= nAlu;
        apro = (nApro * 100) / nAlu;
        susp = (nSusp * 100) / nAlu;
        nMerit = nGood = nVerDef = nPass = nInsuf = 0;
        for(i = 0; i < notas.length; i++){
            if(notas[i] >= 9){
                nMerit++;
            }else if(notas[i] >= 7){
                nGood++;
            }else if(notas[i] >= 5){
                nPass++;
            }else if(notas[i] >= 4){
                nInsuf++;
            }else{
                nVerDef++;
            }
        }
        System.out.println("El numero de alumnos es " 
                + nAlu + ", donde el " + apro + "% de los alumnos ha aprobado "
                + "y el " + susp + "% ha suspendido y la media es "
                + med);
        System.out.println("Existen " + nMerit + " alumnos"
                + " con meritos, " + nGood + " con buena nota, "
                + nPass + " que han aprobado, " + nInsuf
                + " con notas insuficientes, y "
                + nVerDef + " con notas muy deficientes");
    }
    
}
