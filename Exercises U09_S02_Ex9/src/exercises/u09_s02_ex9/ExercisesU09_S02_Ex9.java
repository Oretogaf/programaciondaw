/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u09_s02_ex9;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU09_S02_Ex9 {
    static Scanner uin = new Scanner(System.in);
    
    public static void menu(){
        int men = 0;
        while(true){
            System.out.println("Introduce la opcion que desees realizar");
            System.out.println("1. Calcular la división de dos números enteros");
            System.out.println("2. Calcular la raíz cuadrada de un número entero");
            System.out.println("3. Salir");
            try{
                men = uin.nextInt();
                System.out.println();
            }catch (InputMismatchException e){
                System.out.println("El numero introducido no es correcto");
            }finally{
                uin.nextLine();
            }
            switch(men){
                case 1:
                    Division.div();
                    break;
                case 2:
                    Raiz.sqrt();
                    break;
                case 3:
                    System.out.println("Hasta pronto");
                    System.exit(0);
                    break;
                default:
                    break;
            }
            System.out.println();
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        menu();
    }
    
}
