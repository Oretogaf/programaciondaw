/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u09_s02_ex9;

import static exercises.u09_s02_ex9.ExercisesU09_S02_Ex9.uin;
import java.util.InputMismatchException;

/**
 *
 * @author Mati
 */
public class Division {
    public static void div(){
        int dive = 0;
        int divo = 0;
        double res = -123456789;
        try{
            System.out.println("Introduce el dividendo");
            dive = uin.nextInt();
            System.out.println("Introduce el divisor");
            divo = uin.nextInt();
        }catch (InputMismatchException e){
            System.out.println("El numero no es correcto");
        }finally{
            uin.nextLine();
        }
        try{
            res = (double)dive/divo;
        }catch (ArithmeticException e){
            System.out.println(e.getMessage());
        }
        System.out.println("El resultado es " + res);
    }
}
