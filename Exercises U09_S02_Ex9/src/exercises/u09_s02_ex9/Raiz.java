/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u09_s02_ex9;

import static exercises.u09_s02_ex9.ExercisesU09_S02_Ex9.uin;
import java.util.InputMismatchException;

/**
 *
 * @author Mati
 */
public class Raiz {
    public static void sqrt(){
        int num = 0;
        double res = -1;
        try{
            System.out.println("Introduce el numero del que se desea conocer la raiz cuadrada");
            num = uin.nextInt();
            if(num < 0){
                throw new ExceptionCalculadora("No se puede realizar la raiz cuadrada de un numero negativo");
            }
        }catch (ExceptionCalculadora e){
            System.out.println(e.toString());
        }catch (InputMismatchException e){
            System.out.println("El numero introducido no es correcto");
        }
        try{
            res = Math.sqrt(num);
            System.out.println("El resultado es +- " + res);
        }catch(ArithmeticException e){
            System.out.println("El numero no es valido");
        }
    }
}
