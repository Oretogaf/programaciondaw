/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u09_s02_ex9;

/**
 *
 * @author Mati
 */
public class ExceptionCalculadora extends Exception{

    public ExceptionCalculadora() {
        super("Excepcion generada por un error de entrada salida en la calculadora");
    }
    public ExceptionCalculadora(String string) {
        super(string);
    }
    
    
}
