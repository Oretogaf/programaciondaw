/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s03_2_ex1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Mati
 */
public class ManagerCities implements Serializable{
    ArrayList<City> list = new ArrayList();
    File fil = new File("salida.dat");
    
    public void addCity(Object cit){
        if(cit instanceof City){
            list.add((City) cit);
        }else{
            System.out.println("El objeto no es de la clase City");
        }
    }
    
    public void removeCity(Object cit){
        if(cit instanceof City){
            if(list.contains((City) cit)){
                list.remove((City) cit);
            }else{
                System.out.println("No existe la ciudad");
            }
        }else{
            System.out.println("El objeto no es de la clase City");
        }
    }
    
    public void removeCity(String nam){
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getCityName().equalsIgnoreCase(nam)){
                list.remove(i);
                return;
            }
        }
        System.out.println("No existe una ciudad con ese nombre");
    }
    
    public void showNumberOfCities(){
        System.out.println(list.size());
    }
    
    public long calculateTotalPopulation(){
        long total = 0;
        for (City cit : list) {
            total += cit.getPopulation();
        }
        return total;
    }
    
    public void showCityWithMaxHabitants(){
        long maxHab = Long.MIN_VALUE;
        int ind = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getPopulation() > maxHab){
                maxHab = list.get(i).getPopulation();
                ind = i;
            }
        }
        try{
            System.out.println(list.get(ind));
        }
        catch (Exception ex){
            System.out.println("Se produjo un error al buscar la ciudad más habitada");
        }
    }
    
    public void showNameOfCitiesOfAProvince(int code){
        Iterator it = list.iterator();
        while (it.hasNext()) {
            City city = (City) it.next();
            if(city.getCodeOfProvince() == code){
                System.out.println(city);
            }
        }
    }
    
    public void writeMe(){
        if(!fil.exists()){
            try {
                fil.createNewFile();
            } catch (IOException ex) {
                System.out.println("Se produjo un error al crear el archivo");
                System.out.println(ex.getMessage());
            }
        }
        ObjectOutputStream out = null;
        try{
            out = new ObjectOutputStream(new FileOutputStream(fil));
        }
        catch (IOException ex)
        {
            System.out.println("Se produjo un error al intentar abrir el archivo");
            System.out.println(ex.getMessage());
        }
        
        try{
            out.writeObject(list);
            out.flush();
        }
        catch (IOException ex){
            System.out.println("Se produjo un error al intentar guardar los archivos");
            System.out.println(ex.getMessage());
        }
        finally
        {
            try{
                out.close();
            } catch (IOException ex){
                System.out.println("Se produjo un error al intentar cerrar el archivo");
                System.out.println(ex.getMessage());
            }
        }
    }
    
    public void readMe(){
        ObjectInputStream in = null;
        try{
            in = new ObjectInputStream(new FileInputStream(fil));
        }
        catch (IOException ex)
        {
            System.out.println("Se produjo un error al intentar abrir el archivo");
            System.out.println(ex.getMessage());
        }
        
        try{
            list = (ArrayList<City>) in.readObject();
        }
        catch (IOException ex){
            System.out.println("Se produjo un error al intentar leer los archivos");
            System.out.println(ex.getMessage());
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Se produjo un error al intentar encontrar la clase de los archivos");
            System.out.println(ex.getMessage());
        }
        finally
        {
            try{
                in.close();
            } catch (IOException ex){
                System.out.println("Se produjo un error al intentar cerrar el archivo");
                System.out.println(ex.getMessage());
            }
        }
    }
}
