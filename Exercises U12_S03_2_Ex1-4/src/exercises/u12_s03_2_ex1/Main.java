/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u12_s03_2_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Main {
    static Scanner uin = new Scanner(System.in);
    static ManagerCities manager = new ManagerCities();
    public static void menu(){
        int index;
        while(true){
            System.out.println("Introduce an option");
            System.out.println("1. Add city");
            System.out.println("2. Remove city");
            System.out.println("3. Show the number of cities");
            System.out.println("4. Calculate the total population");
            System.out.println("5. Show the city with the highest population");
            System.out.println("6. Show the cities of a province");
            System.out.println("7. Exit");
            index = uin.nextInt();
            uin.nextLine();
            switch (index){
                case 1:
                    manager.addCity(createCity());
                    manager.writeMe();
                    break;
                case 2:
                    System.out.println("Introduce the name of the city to remove");
                    String name = uin.nextLine();
                    manager.removeCity(name);
                    manager.writeMe();
                    break;
                case 3:
                    manager.showNumberOfCities();
                    break;
                case 4:
                    System.out.println(manager.calculateTotalPopulation());
                    break;
                case 5:
                    manager.showCityWithMaxHabitants();
                    break;
                case 6:
                    int code;
                    System.out.println("Introduce el numero de provincia");
                    code = uin.nextInt();
                    manager.showNameOfCitiesOfAProvince(code);
                    break;
                case 7:
                    manager.writeMe();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Number not valid");
                    break;
            }
            menu();
        }
    }
    
    public static void main(String[] args) {
        if(manager.fil.exists()){
            manager.readMe();
        }
        menu();
    }
    
    public static City createCity(){
        String name;
        long population;
        int codeProvince;
        
        System.out.println("Introduce the name of city");
        name = uin.nextLine();
        
        System.out.println("Introduce the number of population");
        population = uin.nextLong();
        
        System.out.println("Introduce the code of the province");
        codeProvince = uin.nextInt();
        
        return new City(name, population, codeProvince);
    }
}
