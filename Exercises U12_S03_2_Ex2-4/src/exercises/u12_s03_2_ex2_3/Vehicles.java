/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s03_2_ex2_3;

import java.io.Serializable;

/**
 *
 * @author Mati
 */
public class Vehicles implements Serializable{
    private String model;
    private float rentalPrice;
    private int numTimesRental;
    private float moneyRaised;
    private boolean rented;

    public Vehicles(String model, float rentalPrice, int numTimesRental, float moneyRaised, boolean rented) {
        this.model = model;
        this.rentalPrice = rentalPrice;
        this.numTimesRental = numTimesRental;
        this.moneyRaised = moneyRaised;
        this.rented = rented;
    }

    public String getModel() {
        return model;
    }
    public float getRentalPrice() {
        return rentalPrice;
    }
    public int getNumTimesRental() {
        return numTimesRental;
    }
    public float getMoneyRaised() {
        return moneyRaised;
    }
    public boolean isRented() {
        return rented;
    }

    public void setModel(String model) {
        this.model = model;
    }
    public void setRentalPrice(float rentalPrice) {
        this.rentalPrice = rentalPrice;
    }
    public void setNumTimesRental(int numTimesRental) {
        this.numTimesRental = numTimesRental;
    }
    public void setMoneyRaised(float moneyRaised) {
        this.moneyRaised = moneyRaised;
    }
    public void setRented(boolean rented) {
        this.rented = rented;
    }

    @Override
    public String toString() {
        return "Vehicles{" + "model=" + model + ", rentalPrice=" + rentalPrice + ", numTimesRental=" + numTimesRental + ", moneyRaised=" + moneyRaised + ", rented=" + rented + '}';
    }
}
