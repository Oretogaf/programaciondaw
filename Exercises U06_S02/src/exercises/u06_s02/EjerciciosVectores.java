/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s02;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class EjerciciosVectores {
    public static void mostrarArrayEnteros(int[] vi){
        int i;
        for(i = 0; i < vi.length; i++){
            System.out.println("vi[" + i + "] = " + vi[i]);
        }
    }
    public static void mostrarArrayCaracteres(char[] vc){
        int i;
        for(i = 0 ; i < vc.length ; i++){
            System.out.println("vc[" + i + "] = " + vc[i]);
        }
    }
    public static void mostrarArrayFloat(float[] vf){
        int i;
        for(i = 0; i < vf.length; i++){
            System.out.println("vf[" + i + "] = " + vf[i]);
        }
    }
    public static void leerArrayEnteros(){
        int i;
        int[] vi = new int[10];
        for(i = 0; i < vi.length; i++){
            System.out.println("Introduce el numero de la posicion " + i);
            vi[i] = uin.nextInt();
        }
        mostrarArrayEnteros(vi);
    }
    public static void leerArrayFloat(){
        int i;
        float proImpar = 1;
        float sumPar = 0;
        float[] vf = new float[4];
        for(i = 0; i < vf.length; i++){
            System.out.println("Introduce el numero de la posicion " + i);
            vf[i] = uin.nextFloat();
            if (i % 2 == 0){
                sumPar += vf[i];
            }else{
                proImpar *= vf[i];
            }
        }
        mostrarArrayFloat(vf);
        System.out.println("Suma de las posiciones pares: " + sumPar);
        System.out.println("Producto de las posiciones impares: " + proImpar);

    }
    public static void leerArrayCaracteres(){
        int i;
        char[] vc = new char[4];
        int pos = 0;
        boolean enc = false;
        for(i = 0; i < vc.length; i++){
            System.out.println("Introduce el caaracter de la posicion " + i);
            vc[i] = uin.next().charAt(0);
        }
        for(i = 0; i < vc.length && !enc; i++){
            if(vc[i] == 'a' || vc[i] == 'A'){
                pos = i;
                enc = true;
            }
        }
        if(enc){
            System.out.println("La primera vez que aparece el caracter 'a' o 'A' es en la posicion: " + pos);
        }else{
            System.out.println("No aparece el caracter 'a' o 'A'");
        }
    }
    public static Scanner uin = new Scanner(System.in);
    
    
}
