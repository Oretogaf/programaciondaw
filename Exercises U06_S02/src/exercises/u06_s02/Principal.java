/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s02;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Principal {
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int m;
        System.out.println("Introduce el numero para acceder a la funcion");
        System.out.println("1. Vector de enteros");
        System.out.println("2. Vector de coma flotante");
        System.out.println("3. Vector de caracteres");
        System.out.println("4. Salir");
        m = uin.nextInt();
        switch(m){
            case 1:
                EjerciciosVectores.leerArrayEnteros();
                break;
            case 2:
                EjerciciosVectores.leerArrayFloat();
                break;
            case 3:
                EjerciciosVectores.leerArrayCaracteres();
                break;
            case 4:
                System.exit(0);
                break;
            default:
                System.out.println("Fatal Error");
                System.exit(0);
                break;     
        }
    }
}
