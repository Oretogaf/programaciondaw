/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u11_s01_3_ex9;

import java.util.ArrayList;
/**
 *
 * @author Mati
 */
public class StringSelectionSort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<String> listString = new ArrayList();
        listString.add("j");
        listString.add("i");
        listString.add("h");
        listString.add("g");
        listString.add("f");
        listString.add("e");
        listString.add("d");
        listString.add("c");
        listString.add("b");
        listString.add("a");
        System.out.println("Colección desordenada");
        System.out.println(listString.toString());
        sort(listString, listString.size());
        
        System.out.println("Colección ordenada");
        System.out.println(listString.toString());
    }
    public static void sort(ArrayList<String> sal, int numberUsed){
        for (int i = 0; i < numberUsed; i++) {
            for (int j = 0; j < i; j++) {
                if(sal.get(i).compareTo(sal.get(j)) < 0){
                    String aux1, aux2;
                    aux1 = sal.get(i);
                    aux2 = sal.get(j);
                    sal.set(i, aux2);
                    sal.set(j, aux1);
                }
            }
        }
    }
}
