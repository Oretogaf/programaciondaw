/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s05_ex4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Persona {
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String dni;
    public static Scanner uin = new Scanner(System.in);
    public Persona(){
        
    }
    public Persona(String nom,String ape1,String ape2){
        nombre=nom;
        apellido1=ape1;
        apellido2=ape2;
    }
    public void setDNI(String nif){
        dni=nif;
    }
    public void visuPersona(){
        System.out.println(nombre);
        System.out.println(apellido1);
        System.out.println(apellido2);
        System.out.println(dni);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String nom;
        String ape1;
        String ape2;
        String nif;
        System.out.println("Introduce el nombre");
        nom=uin.next();
        System.out.println("Introduce los apellidos");
        ape1=uin.next();
        ape2=uin.next();
        Persona per1 = new Persona(nom,ape1,ape2);
        System.out.println("Introduce el DNI");
        nif=uin.next();
        per1.setDNI(nif);
        per1.visuPersona();
    }
    
}
