/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s02_1_ex3;

/**
 *
 * @author Mati
 */
public class Profesor {
    protected String nombre;
    protected int edad;
    protected float sueldo;

    public Profesor(String nombre, int edad, float sueldo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sueldo = sueldo;
    }
    public Profesor(String nombre) {
        this.nombre = nombre;
    }
    public Profesor(int edad) {
        this.edad = edad;
    }
    public Profesor(float sueldo) {
        this.sueldo = sueldo;
    }
    public Profesor() {
        
    }

    public String getNombre() {
        return nombre;
    }
    public int getEdad() {
        return edad;
    }
    public float getSueldo() {
        return sueldo;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }

    @Override
    public String toString() {
        return "Profesor{" + "nombre=" + nombre + ", edad=" + edad + ", sueldo=" + getSueldo() + '}';
    }
    
}
