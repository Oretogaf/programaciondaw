/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u07_s02_1_ex3;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class EquipoDirectivo extends Profesor {
    protected String cargo;
    protected int plus;
    
    public static Scanner uin = new Scanner(System.in);
    
    public EquipoDirectivo(String cargo, int plus, String nombre, int edad, float sueldo) {
        super(nombre, edad, sueldo);
        this.cargo = cargo;
        this.plus = plus;
    }
    
    @Override
    public float getSueldo(){
        return (sueldo + plus);
    }
    public String visualizarCargoDirectivo(){
        return cargo;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String nombre;
        String cargo;
        int plus;
        int edad;
        float sueldo;
        
        System.out.println("Introduce el nombre");
        nombre = uin.nextLine();
        System.out.println("Introduce el cargo que ocupa");
        cargo = uin.nextLine();
        System.out.println("Introduce la edad");
        edad = uin.nextInt();
        System.out.println("Introduce el sueldo");
        sueldo = uin.nextFloat();
        System.out.println("Introduce el plus");
        plus = uin.nextInt();
        
        EquipoDirectivo dir = new EquipoDirectivo(cargo, plus, nombre, edad, sueldo);
        
        System.out.println(dir.toString());
        System.out.println("Cargo que desarrolla = " + dir.visualizarCargoDirectivo());
    }
    
}
