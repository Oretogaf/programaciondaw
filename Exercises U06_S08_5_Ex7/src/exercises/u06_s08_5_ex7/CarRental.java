/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u06_s08_5_ex7;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class CarRental {
    private static Car[] rental;
    public static Scanner uin = new Scanner(System.in);
    
    private static void iniciateRental(){
        int index;
        do{
            System.out.println("Introduce el numero de vehiculos que existen");
            index = uin.nextInt();
            if(index < 0){
                System.out.println("El numero de coches no puede ser inferior a 0");
            }
        }while(index < 0);
        rental = new Car[index];
    }
    private static void introduceData(){
        String model;
        float rentalPrice;
        int numTimesRental;
        float moneyRaised;
        boolean rented;
        for (int i = 0; i < rental.length; i++) {
            uin.nextLine();
            System.out.println("Introduce el modelo de coche " + (i + 1));
            model = uin.nextLine();
            System.out.println("Introduce el coste por dia de alquilar el coche");
            rentalPrice = uin.nextFloat();
            System.out.println("Introduce el numero de veces que se ha alquilado el coche");
            numTimesRental = uin.nextInt();
            System.out.println("Introduce el dinero que ya se ha recaudado con el coche");
            moneyRaised = uin.nextFloat();
            rented = false;
            rental[i] = new Car(model, rentalPrice, numTimesRental, moneyRaised, rented);
            System.out.println();
        }
    }
    private static void menu(){
        int index;
        System.out.println("1.Alquilar un coche\n"
                + "2.Ver información de los coches no alquilados\n"
                + "3.Reemplazar un coche\n"
                + "4.Ver el coche mas alquilado\n"
                + "5.Salir");
        index = uin.nextInt();
        System.out.println();
        uin.nextLine();
        switch(index){
            case 1:
                alquilar();
                break;
            case 2:
                verInfo();
                break;
            case 3:
                modificarCoche();
                break;
            case 4:
                verMasRented();
                break;
            case 5:
                System.out.println();
                System.out.println("Goodbye!!");
                System.exit(0);
                break;
            default:
                System.out.println("Fatal error");
                System.exit(0);
                break;
        }
        System.out.println();
        menu();
    }
    private static void alquilar(){
        String model;
        int days;
        int var = 0;
        System.out.println("Introduce el modelo");
        model = uin.nextLine();
        for(int i = 0; i < rental.length; i++){
            if(model.equalsIgnoreCase(rental[i].getModel())){
                System.out.println("Introduce el numero de dias que deseas alquilarlo");
                days = uin.nextInt();
                rental[i].setRented(true);
                rental[i].setMoneyRaised(rental[i].getRentalPrice() * days);
                rental[i].setNumTimesRental(rental[i].getNumTimesRental() + 1);
            }else{
                var++;
            }
        }
        if(var == rental.length){
            System.out.println("No se ha encontrado ningun modelo llamado " + model);
        }
    }
    private static void modificarCoche(){
        String model;
        float rentalPrice;
        int numTimesRental;
        float moneyRaised;
        boolean rented;
        int var = 0;
        uin.nextLine();
        System.out.println("Introduce el modelo de coche");
        model = uin.nextLine();
        for (int i = 0; i < rental.length; i++) {
            if(model == rental[i].getModel()){
                System.out.println("Introduce el nuevo coste por dia de alquilar el coche");
                rentalPrice = uin.nextFloat();
                rental[i].setRentalPrice(rentalPrice);
                System.out.println("Introduce el numero de veces que se ha alquilado el coche");
                numTimesRental = uin.nextInt();
                rental[i].setNumTimesRental(numTimesRental);
                System.out.println("Introduce el dinero que ya se ha recaudado con el coche");
                moneyRaised = uin.nextFloat();
                rental[i].setMoneyRaised(moneyRaised);
            }else{
                var++;
            }
        }
        if(var == rental.length){
            System.out.println("No se ha encontrado ningun modelo llamado " + model);
        }
    }
    private static void verInfo(){
        for (Car rent : rental) {
            if(rent.isRented() == false){
                System.out.println(rent.toString());
            }
        }
    }
    private static void verMasRented(){
        int rentMax = Integer.MIN_VALUE;
        int indexRentalMax = Integer.MAX_VALUE;
        for (int i = 0; i < rental.length; i++){
            if(rentMax < rental[i].getNumTimesRental()){
                rentMax = rental[i].getNumTimesRental();
                indexRentalMax = i;
            }
        }
        System.out.println(rental[indexRentalMax].toString());
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        iniciateRental();
        introduceData();
        menu();
    }
    
}
