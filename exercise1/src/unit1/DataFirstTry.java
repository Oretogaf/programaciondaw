/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit1;

/**
 *
 * @author Mati
 */
public class DataFirstTry {
    private int day;
    private String month;
    private int year;
    public void writeOutput(){
        System.out.println(month+" "+day+", "+year);
    }
    public void makeItNewYears(){
        month="January";
        day=1;
    }
    public void yellIfNewYear(){
        if(month.equalsIgnoreCase("January")&&day==1){
            System.out.println("Hurrah!");
        }else{
            System.out.println("Not New Year's Day");
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DataFirstTry data1 = new DataFirstTry();
        
        data1.day = 22;
        data1.month = "October";
        data1.year = 2004;
        
        data1.writeOutput();
        data1.yellIfNewYear();
        
        
        System.out.println();
        System.out.println();
        
        
        DataFirstTry data2;
        data2 = new DataFirstTry();
        
        data2.day = 4;
        data2.month = "December";
        data2.year = 2012;
        
        data2.writeOutput();
        data2.makeItNewYears();
        data2.writeOutput();
        data2.yellIfNewYear();
    }
    
}
