/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_2_ex12;

/**
 *
 * @author Mati
 */
public class ConErrores {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int cont=0;
        String s1, s2 = new String("hola que tal ");
        s1 = s2.trim();
        s2 = s1;
        System.out.println("s1==s2: " + (s1==s2)); //true
        for (int i=1; i<=s1.length(); i++)
        if (s1.charAt(i)=='a' || s1.charAt(i)=='e' || s1.charAt(i) == 'i' ||
        s1.charAt(i)=='o' || s1.charAt(i)=='u')
        {
            cont++;
            s1 = s1.substring(1, i) + s1.substring(i,i).toUpperCase() +
            s1.substring(i+1, s1.length());
        }
        System.out.println("s1: " + s1 + ":");
        System.out.println("s2: " + s2 + ":");
        System.out.println("s1== s2 : " + (s1==s2));
        System.out.println("Número de vocales " + cont);
    }
    
}
