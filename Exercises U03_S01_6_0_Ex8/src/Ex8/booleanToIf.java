/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex8;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class booleanToIf {
public static Scanner stdin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n;
        System.out.println("Introduce el valor de n");
        n=stdin.nextInt();
        if (n<0){
            System.out.println("El valor es menor que cero");
        } else if (n<100){
            System.out.println("El valor esta entre el 0 y el 100, incluyendo al 0");
        } else{
            System.out.println("El numero es mayor o igual a 100");
        }
    }
    
}
