/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u10_s03_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU10_S03_Ex1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner uin = new Scanner(System.in, "iso-8859-1");
        String usu;
        while (true) {            
            usu = uin.nextLine();
            switch (usu){
                case "Hola":
                case "hola":
                    System.out.println("Bienvenido");
                    break;
                case "¿Que hay para comer?":
                case "¿que hay para comer?":
                case "Que hay para comer":
                case "que hay para comer":
                case "Que hay para comer?":
                case "que hay para comer?":
                    System.out.println("Macarrones");
                    break;
                case "Adios":
                case "adios":
                    System.out.println("Final del programa");
                    System.exit(0);
                    break;
                default:
                    break;
            }
        }
    }
    
}
