/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit4;

import java.util.Scanner;

/**
 *
 * @author eva
 */
public class BeerSong {
    private int bottles;
    
    public BeerSong(int bottles){
        // initializes the instance variable bottles to the value
	// of the parameter. If this number is less than 0, this method
        // initilizes the instance variable bottles to 0. If the parameter
        // is greather than 99, it initializes the instance variable bottles
        // to 99.
        if(bottles<0){
            this.bottles=0;
        }else if(bottles>99){
            this.bottles=99;
        }else{
            this.bottles=bottles;
        }
        
    }
   
    
    public void outputSong(){
        // outputs on the screen the entire song from the value of bottles
	// down to zero. You have to use a loop.
        for(;bottles>=0;bottles--){
            System.out.println(numberBottlesToString()+" bottles of beer on the wall,");
            System.out.println(numberBottlesToString()+" bottles of beer,");
            System.out.println("Take one down, pass it around,");
            System.out.println("");
        }
    }
    
    private String numberBottlesToString(){
        // returns a string representing the value of bottles in words.
        
        if ((units() != "") && (bottles > 9))
                return tens() + "-" + units();
        else if (bottles >= 9)
            return tens();
        else
            return units();
    }
    
    private String tens(){
        // returns the tens digit of the instance variable bottles in word form.
        // And the entire string for values 10 - 19.
        if (bottles > 89)
	    return "Ninety";
	else if (bottles > 79)
	    return "Eighty";
        else if (bottles > 69)
            return "Seventy";
        else if (bottles > 59)
            return "Sixty";
        else if (bottles > 49)
            return "Fifty";
        else if (bottles > 39)
            return "Fourty";
        else if (bottles > 29)
            return "Thirty";
        else if (bottles > 19)
            return "Twenty";
        else if (bottles > 18)
            return "Nineteen";
        else if (bottles > 17)
            return "Eighteen";
        else if (bottles > 16)
            return "Seventeen";
        else if (bottles > 15)
            return "Sixteen";
        else if (bottles > 14)
            return "Fiveteen";
        else if (bottles > 13)
            return "Fourteen";
        else if (bottles > 12)
            return "Thirteen";
        else if (bottles > 11)
            return "Twelve";
        else if (bottles > 10)
            return "Eleven";
        else if (bottles == 10)
	    return "Ten";
	else if (bottles % 10 == 9)
	    return "Nine";
        return "";
    }
    
    private String units(){
        // returns the ones digit of the instance variable bottles in word form.
        if ( (bottles >= 10) && (bottles < 20) )
	    return "";
	
	else if (bottles % 10 == 8)
	    return "Eight";
	else if (bottles % 10 == 7)
	    return "Seven";
	else if (bottles % 10 == 6)
	    return "Six";
	else if (bottles % 10 == 5)
	    return "Five";
	else if (bottles % 10 == 4)
	    return "Four";
	else if (bottles % 10 == 3)
	    return "Three";
	else if (bottles % 10 == 2)
	    return "Two";
	else if (bottles % 10 == 1)
	    return "One";
	else if (bottles % 10 == 0 && bottles != 0)
	    return "";
	else if (bottles % 10 == 0 && bottles == 0)
	    return "Zero";
        return "";
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int boBeer;
        System.out.println("Introduce el numero de botellas de cerveza");
        boBeer=uin.nextInt();
        BeerSong beerS1 = new BeerSong(boBeer);
        beerS1.outputSong();
    }
    
}
