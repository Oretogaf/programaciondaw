package Clases;
/**
 *
 * @author Eva 
 */
/*
 * Cree un programa que represente una urna de la que se pueden sacar y meter 
 * bolas.
 */
import java.util.HashSet;
import java.util.Iterator;
public class Urna 
{
   private int numBolas;  //Representa el número de bolas que hay en la urna
                          //en un momento determinado.
   private HashSet urna; //Como en la urna no pueden haber bolas repetidas
                         //podemos usar la interfaz HashSet
   
   public Urna()
   {
       numBolas = 0;
       urna = new HashSet();
   }
   
   public void introducirBola(Integer bola)
   {
       boolean bolaRepetida;
       
       bolaRepetida = !(urna.add(bola));
       if (!bolaRepetida)
         numBolas++;
   }
   
   public Integer sacarBola()
   {
       Integer bola = null;
       Iterator it = urna.iterator();
       if (it.hasNext())
       {
          bola = (Integer) it.next();
          it.remove();
          numBolas--;
       }
       return bola;
   }
   
   public int getNumBolas()
   {
       return numBolas;
   }
   
   public String toString()
    {
        Iterator it;
        String res = "";
        
        if(!urna.isEmpty())
        {
            
            it = urna.iterator();
            while(it.hasNext())
            {
                res = res + (Integer) it.next() + " ";
            }
        }
        else
            System.out.println("Urna vacía");
        return res;
    }
   
}
