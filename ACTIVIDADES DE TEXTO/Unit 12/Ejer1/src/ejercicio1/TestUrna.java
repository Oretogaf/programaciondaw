package ejercicio1;

/**
 *
 * @author Eva 
 */
import Clases.Urna;
public class TestUrna 
{
    public static void main(String[] args) 
    {
        Urna objUrna = new Urna();
        
        System.out.println("Introduiendo las bolas en la urna");
        objUrna.introducirBola(new Integer(8));
        objUrna.introducirBola(new Integer(28));
        objUrna.introducirBola(new Integer(7));
        objUrna.introducirBola(new Integer(34));
        objUrna.introducirBola(new Integer(12));
        objUrna.introducirBola(new Integer(51));
        objUrna.introducirBola(new Integer(34));
        System.out.println("Las bolas introducidas en la urna son : " + objUrna.toString());
        System.out.println("Introducidas " + objUrna.getNumBolas() + " bolas en la urna.");
        System.out.println("Bola : " + objUrna.sacarBola());
        System.out.println("Bola : " + objUrna.sacarBola());
        System.out.println("Quedan " + objUrna.getNumBolas() + " bolas en la urna.");
    }

}
