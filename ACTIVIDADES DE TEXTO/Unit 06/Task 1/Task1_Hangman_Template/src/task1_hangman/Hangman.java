/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package task1_hangman;

import java.util.*;  // Random
import java.io.*;    // Lectura de datos - excepción

/*
 * Hangman.java
 */
/**
  * Class that executes the hangman game, that consist of trying to guess a word,
  * with a maximum number of errors.
  * @version 2.0
  * @author Eva
  */
public class Hangman {
        public static Scanner stdin = new Scanner(System.in);
        
	//************************** CONSTANTS ****************************//
	/** Maximum number of errors allowed*/
	public static final int INUNMAXERR = 8;
	/** Possible words */
	public static final String[] ASWORDS = {"hello", "ugly", "monster","head"};
	//** I create a space to better visualize the doll
	public static final String AWHITESPACE = ("               ");
	
	/**
	 * Main method of input to our game.
	 * @param args
	 */
	public static void main(String[] args)throws IOException{
		//I define an object of my own class to call non-static methods.

                //Returns array of chars as word solution.

                //Returns array of chars as user word.

            
                //Show: welcome to the game.
		

		//Initialize the number of errors and successes to zero.
		

		do{
			//I show the word to the user.
			
                        //I ask the letter to the user.
			
			//I check if the letter is in the word solution and keep the number of successes in this round.
			
			//Increase the number of successes in this round to the general successes of the game.
			
			//If the successes in this round are zero.
			
				//Increase the number of general errors.
				
				//Show the picture of a hangman.
				
			}
                //It is repeated while remaining errors and the number of successes does not match the length of the solution.
		}while();

		//If we left it is because the number of successes matches the length of the solution.
		
			//Show the congratulatory message.
			
		}
		//We leave the game.
		
	}
	/**
	 * Returns an array of chars with a random word.
	 * @return char[] with the random word found.
	 */
	public char[] randomWordSearch(){
		  //First I select a random number from 0 to ASWORDS-1
	    
	}

	/**
	 * Returns a new array of chars with so many
	 * hyphens as letters have the input array.
	 * @param inacWord
	 * @return acWordUsu
	 */
	public char[] createUserWord(char[] inacWord){
        }
	/**
         * Method that checks whether the letter entered is in the array introduced.
         * If there is such a letter, changes in the user word the hiphen for the letter
         * in the position found.
	 * @param incLetter
	 * @param inacWordSolution
	 * @param inacWordUsu
	 * @return el número de letras acertadas.
	 */
	public int comprobarLetra(char incLetter,char[] inacWordSolution, char[] inacWordUsu){
		int iNumHits = 0;
		//I check that the letter does not exist in the word of the user.
		
		//If you have not found in the user's word, then we look in the solution.
		
		return iNumHits;
	}

	/**
	 * Method shows welcome screen.
	 */
	public void welcomeGameShow(){
		
	}
	/**
	 * Method shows goodby screen.
	 */
	public void showGoodbyeMessage(){
	
	}
	/**
	 * Solicita una letra al usuario
	 * @return char with the letter specified by the user.
	 */
	public char requestLetter()throws IOException{
	
	}
	/**
	 * Show the picture depending on the errors that come as a parameter
	 * @param iniNumErr
	 */
	public void showDrawing(int iniNumErr){
	}
	/**
	 * Shows the array of char introduced as input.
	 * @param inacharWord
	 */
	public void wordUserDisplay(char[] inacharWord){
	}
	/**
	 * Shows the congratulatory message if you have completely matched.
	 */
	public void showCongratulatoryMessage(){
	}


}
