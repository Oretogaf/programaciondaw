/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package objetosNegocio;

/**
 *
 * @author Eva
 */
public class Fecha {
    protected int dia;
    protected int mes;
    protected int año;

    /** Creates a new instance of Fecha */
    public Fecha() {}

    public Fecha(int dia, int mes, int año) {
        this.dia = dia;
        this.mes = mes;
        this.año = año;
    }
    public String toString() {
        return dia + " / " + mes + " / " + año;
    }

    /**
     * @return the dia
     */
    public int getDia() {
        return dia;
    }

    /**
     * @param dia the dia to set
     */
    public void setDia(int dia) {
        this.dia = dia;
    }

    /**
     * @return the mes
     */
    public int getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(int mes) {
        this.mes = mes;
    }

    /**
     * @return the año
     */
    public int getAño() {
        return año;
    }

    /**
     * @param año the año to set
     */
    public void setAño(int año) {
        this.año = año;
    }

}
