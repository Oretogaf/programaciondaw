/*
  Utilizar los métodos de la clase File para indicar las propiedades del archivo
  autoexec.bat en un sistema Windows y para listar el contenido del directorio 
  actual.
 */

/**
 *
 * @author Eva 
 */
import java.io.*;
public class CFile {

    
    public static void main(String[] args) {
        File f1, f2, f3;
        
        String[] listaArchivos;
        int i;
        
        f1 = new File("c:\\eva", "prueba.txt");
        f2 = new File("eva.txt"); //No crea físicamente el archivo.
        f3 = new File(".");
        
        System.out.println("Nombre(f1): " + f1.getName());
        System.out.println("Path(f1): " + f1.getPath());
        System.out.println("Absolute Path(f1): " + f1.getAbsolutePath());
        System.out.println("Parent(f1): " + f1.getParent());
        System.out.println("Existe(f2): " + f2.exists());
        System.out.println("Permiso de escritura(f1): " + f1.canWrite());
        System.out.println("Permiso de lectura(f1): " + f1.canRead());
        System.out.println("Permiso de ejecución(f1): " + f1.canExecute());
        System.out.println("Es directorio(f1): " + f1.isDirectory());
        System.out.println("Es archivo(f1): " + f1.isFile());
        System.out.println("Es absoluto(f1): " + f1.isAbsolute());
        System.out.println("Tamaño(f1): " + f1.length());
        listaArchivos = f3.list();
        //Si f3 no es un directorio da error
        System.out.println("Contenido del directorio actual");
        for(i=0; i<listaArchivos.length; i++)
        {
            System.out.println("listaArchivos["+i+"] = " + listaArchivos[i]);
        }
    }

}
