/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Ex4;

/**
 *
 * @author Mati
 */

import java.util.Scanner;

public class Media {
public static Scanner stdin = new Scanner (System.in);

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        int notaTrimestre1;
        int notaTrimestre2;
        int notaTrimestre3;
        int notaCurso;
        double notaMedia;
        System.out.println("Introduce nota primer trimestre");
        notaTrimestre1= stdin.nextInt();
        System.out.println("Introduce nota segundo trimestre");
        notaTrimestre2= stdin.nextInt();
        System.out.println("Introduce nota tercer trimestre");
        notaTrimestre3= stdin.nextInt();
        notaCurso = notaTrimestre1 + notaTrimestre2 + notaTrimestre3;
        notaMedia = notaCurso/3.0;
        System.out.println("La nota media es "+notaMedia);
    }

}
