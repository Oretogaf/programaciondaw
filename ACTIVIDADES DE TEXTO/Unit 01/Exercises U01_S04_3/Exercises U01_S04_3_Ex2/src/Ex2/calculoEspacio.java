/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Ex2;

import java.util.*;

/**
 *
 * @author Mati
 */
public class calculoEspacio {
    public static Scanner stdin = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final double SPEED=3.2;
        double space;
        double time;
        System.out.println("Introduce el tiempo transcurrido en segundos");
        time=stdin.nextDouble();
        space=time*SPEED+5.5;
        System.out.println("El espacio recorrido son "+space+" metros.");
    }

}
