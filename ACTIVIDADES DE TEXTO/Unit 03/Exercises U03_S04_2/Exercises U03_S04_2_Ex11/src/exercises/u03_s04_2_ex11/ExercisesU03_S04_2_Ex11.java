/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s04_2_ex11;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S04_2_Ex11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int i;
        int o;
        for(i=1;i<=10;i++){
            for(o=0;o<=10;o++)
                System.out.println(i+" * "+o+" = "+(i*o));
            System.out.println();
        }
    }
    
}
