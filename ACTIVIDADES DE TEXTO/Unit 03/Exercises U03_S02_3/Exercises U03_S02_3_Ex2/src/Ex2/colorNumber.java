/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex2;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class colorNumber {
public static Scanner stdin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        char color;
        System.out.println("Introduce color");
        color=stdin.next().charAt(0);
        switch(color){
            case'r':
            case'R':
                System.out.println("RED");
                break;
            case'v':
            case'V':
                System.out.println("GREEN");
                break;
            case'a':
            case'A':
                System.out.println("BLUE");
                break;
            default:
                System.out.println("BLACK");
                break;
        }
    }
    
}
