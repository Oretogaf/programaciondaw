/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s05_1_ex4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S05_1_Ex4 {
public static Scanner std = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int i;
        int u;
        int s=0;
        System.out.println("Introduce un numero");
        u=std.nextInt();
        for(i=1;i<=u;i++){
            s+=i;
        }
        System.out.println("La suma es "+s);
        System.out.println();
        i=1;
        s=0;
        while(i<=u){
            s+=i;
            i++;
        }
        System.out.println("La suma es "+s);
        System.out.println();
        i=1;
        s=0;
        do{
            s+=i;
            i++;
        }while(i<=u);
        System.out.println("La suma es "+s);
    }
    
}
