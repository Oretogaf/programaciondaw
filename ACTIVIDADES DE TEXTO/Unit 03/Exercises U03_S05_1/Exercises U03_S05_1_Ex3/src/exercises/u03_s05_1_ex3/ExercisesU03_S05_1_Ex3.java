/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s05_1_ex3;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S05_1_Ex3 {
public static Scanner std = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int i;
        for(i=1;i<=10;i++){
            System.out.println(i);
        }
        System.out.println();
        i=1;
        while(i<=10){
            System.out.println(i);
            i++;
        }
        System.out.println();
        i=1;
        do{
            System.out.println(i);
            i++;
        }while(i<=10);
    }
    
}
