/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s03_2_ex17;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S03_2_Ex17 {
public static Scanner stdin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int i;
        int u;
        int odd=0;
        int even=0;
        for(i=1;i<=10;i++){
            System.out.println("Introduce numero");
            u=stdin.nextInt();
            if(u%2==0)
                even++;
            else
                odd++;
        }
        System.out.println("Even = "+even+" and odd = "+odd);
    }
    
}
