/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewexercisesarrays_ex2;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class CountPoor{
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double income;
        int size;
        double housingCost;
        double foodCost;
        boolean poor;
        
        System.out.println("Introduce el numero de familias");
        Family[] count = new Family[uin.nextInt()];
        for (int i = 0; i < count.length; i++) {
            do{
                System.out.println("Introduce los ingresos de la familia " + (i + 1));
                income = uin.nextDouble();
            }while(income < 0);
            do{
                System.out.println("Introduce los miembros de la familia " + (i + 1));
                size = uin.nextInt();
            }while(size < 0);
            count[i] = new Family(income, size);
            
            do{
                System.out.println("Introduce los gasto de vivienda de la familia " + (i + 1));
                housingCost = uin.nextDouble();
            }while(housingCost < 0);
            do{
                System.out.println("Introduce los gasto de alimentacion medios para cada miembro de la familia " + (i + 1));
                foodCost = uin.nextDouble();
            }while(foodCost < 0);
            poor = count[i].isPoor(housingCost, foodCost);
            if(poor){
                System.out.println("La familia "  + (i + 1) + " es pobre");
            }
            System.out.println();
        }
    }    
}
