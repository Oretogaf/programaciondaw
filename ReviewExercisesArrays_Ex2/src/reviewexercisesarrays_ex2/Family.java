/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewexercisesarrays_ex2;

/**
 *
 * @author Mati
 */
public class Family {
    protected double income;
    protected int size;

    public Family(double income, int size) {
        this.income = income;
        this.size = size;
    }
    public boolean isPoor(double housingCost, double foodCost){
        double cost;
        double half;
        cost = housingCost + (foodCost * size);
        half = income / 2;
        return (half - cost) <= 0;
    }
    @Override
    public String toString() {
        return "Family{" + "income=" + income + ", size=" + size + '}';
    }    
}
