/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u09_s05_ex1;

/**
 *
 * @author Mati
 */
public class Ejercicio1 {
    public static void dividirEntreArray(int num, int[] ar){
        for (int b : ar) {
            try{
                System.out.println(num + " / " + b + " = " + (num/b));
            }catch (ArithmeticException e){
                System.out.println("No se ha podido realizar la division. Error:\" " + e.getMessage() + " \"");
            }
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int num = 5;
        int[] ar = {2,1,0,-1,-2};
        dividirEntreArray(num, ar);
    }
    
}
