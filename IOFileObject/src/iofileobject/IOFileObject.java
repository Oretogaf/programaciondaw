/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package iofileobject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Mati
 */
public class IOFileObject {
    public static void main(String[] args) {
        Person person = new Person("Julio", new Date(06, 26, 1997), null);
        ObjectOutputStream buff;
        ObjectInputStream buffIn;
        try {
            buff = new ObjectOutputStream(new FileOutputStream("person.txt"));
            buff.writeObject(person);
            buff.close();
            Person per;
            buffIn = new ObjectInputStream(new FileInputStream("person.txt"));
            per = (Person) buffIn.readObject();
            System.out.println(per);
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex);
        }
    }
}
