/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s01_1_ex2;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Mati
 */
public class ExercisesU12_S01_1_Ex2<T> {
    public T getFirst(List<T> a){
        if(a.isEmpty()){
            return null;
        }
        return a.get(0);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ExercisesU12_S01_1_Ex2 exercisesU12_S01_1_Ex2 = new ExercisesU12_S01_1_Ex2();
        LinkedList<String> linkedList = new LinkedList();
        
        linkedList.add("hello");
        linkedList.add("Goodbye");
        
        System.out.println(exercisesU12_S01_1_Ex2.getFirst(linkedList));
    }
    
}
