/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s04_2_3_ex7;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S04_2_3_Ex7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        float[] array = {26F,45F,6.7F,59F,56F,23F,2.83F,54F,12F,2.55F,3.67F,43F,34F,21F,34F,56F,76F,6.7F,56F,85F};
        int i;
        float aux = 0;
        float aux2 = 0;
        for(i = 0; i < array.length && aux == 0; i++){
            if(array[i] == aux2){
                aux++;
                System.out.println("The first position of number " + aux2 + " is " + i);
            }
        }
        if(aux == 0){
            System.out.println("There is not the number " + aux2 + " in the execution parameters");
        }
    }
    
}
