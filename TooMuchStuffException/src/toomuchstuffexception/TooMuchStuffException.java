/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toomuchstuffexception;

/**
 *
 * @author Mati
 */
public class TooMuchStuffException extends Exception{
    private String message;
    public TooMuchStuffException(){
        super("Too much stuff!");
        message = "Too much stuff!";
    }
    public TooMuchStuffException(String message){
        super(message);
        this.message = message;
    }
    public void setMessage(String message){
        this.message = message;
    }
    @Override
    public String getMessage(){
        return message;
    }
}
