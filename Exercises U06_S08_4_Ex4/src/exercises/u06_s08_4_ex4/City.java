/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_4_ex4;

/**
 *
 * @author Mati
 */
public class City {
    private String cityName;
    private long population;
    private int codeProvince;
    public City(String cityName, long population, int codeProvince) {
        this.cityName = cityName;
        this.population = population;
        this.codeProvince = codeProvince;
    }
    public City() {
        
    }

    public String getCityName() {
        return cityName;
    }
    public long getPopulation() {
        return population;
    }
    public int getCodeProvince() {
        return codeProvince;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public void setPopulation(long population) {
        this.population = population;
    }
    public void setCodeProvince(int codeProvince) {
        this.codeProvince = codeProvince;
    }

    @Override
    public String toString() {
        return "City{" + "cityName=" + cityName + ", population=" + population + ", codeProvince=" + codeProvince + '}';
    }
}
