/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_4_ex4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class MainCity {
    private static City[] cities;
    private static void inicioArray(){
        int inicio;
        do{
            System.out.println("Introduce el numero de ciudades");
            inicio = uin.nextInt();
            if(inicio > 50){
                System.out.println("El valor es superior a 50");
            }else if(inicio <= 0){
                System.out.println("El valor es inferior o igual a 0");
            }
        }while(inicio > 50 || inicio <= 0);
        cities = new City[inicio];
    }
    private static void totalPopulation(){
        int provinceIndex;
        double totalPopulation = 0;
        System.out.println("Introduce el numero de provincia");
        provinceIndex = uin.nextInt();
        for (City citie : cities) {
            if(citie.getCodeProvince() == provinceIndex){
                totalPopulation += citie.getPopulation();
            }
        }
        System.out.println(totalPopulation);
    }
    private static void menu() {
        System.out.println();
        int index;
        System.out.println("1.Introducir datos\n"
                + "2.Ver poblacion de una provincia\n"
                + "3.Ver datos\n"
                + "4.Exit");
        index = uin.nextInt();
        switch(index){
            case 1:
                System.out.println();
                for (int i = 0; i < cities.length; i++){
                    introduceData(i);
                    System.out.println();
                }
                menu();
                break;
            case 2:
                System.out.println();
                totalPopulation();
                menu();
                break;
            case 3:
                for (int i = 0; i < cities.length; i++) {
                    System.out.println(cities[i].toString());
                }
                System.out.println();
                menu();
                break;
            case 4:
                System.out.println();
                System.out.println("Goodbye!!");
                System.exit(0);
                break;
            default:
                System.out.println("Fatal error");
                System.exit(0);
                break;
        }
    }
    private static void introduceData(int index){
        String nameCity;
        long population;
        int codeProvince;
        System.out.println("Introduce el nombre de la ciudad");
        nameCity = uin.next();
        System.out.println("Introduce la poblacion");
        population = uin.nextInt();
        System.out.println("Introduce el codigo de la provincia");
        codeProvince = uin.nextInt();
        cities[index] = new City(nameCity, population, codeProvince);
    }
    
    public static Scanner uin = new Scanner(System.in);
    public static void main(String[] args){
        inicioArray();
        menu();
    }
}
