/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumvaluesdemo_ex21;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class EnumValuesDemo_Ex21
{

    enum WorkDay {MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY};

    public static void main(String[] args)
    {
        WorkDay[] day = WorkDay.values( );

        Scanner keyboard = new Scanner(System.in);
        double hours = 0, sum = 0;

        for (WorkDay wor : day)
        {
            System.out.println("Enter hours worked for " + wor);
            hours = keyboard.nextDouble( );
             sum = sum + hours;
        }

        System.out.println("Total hours work = " + sum);
    }
}
