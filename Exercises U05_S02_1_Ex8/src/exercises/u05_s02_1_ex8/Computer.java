/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s02_1_ex8;

/**
 *
 * @author Mati
 */
public class Computer {
    //Memory data
    private int memorySize;
    private String memoryType;
    private int memorySpeed;
    
    //Details of the hard drive
    private int hardDriveSize;
    private int numberRevolutionsDisk;
    
    //Data processor
    private String processorModel;
    private int processorSpeed;
    
    //Mutators
    public void setMemoryType(String newType)
    {
        memoryType = newType;
    }
    public void setMemorySpeed(int newSpeed)
    {
        memorySpeed = newSpeed;
    }
    public void setMemorySize(int newSize)
    {
        memorySize = newSize;
    }
    public void setHardDriveSize(int newSize)
    {
        hardDriveSize = newSize;
    }
    public void setNumberRevolutionsDisk (int newValue)
    {
        numberRevolutionsDisk = newValue;
    }
    public void setProcessorModel(String newModel)
    {
        processorModel = newModel;
    }
    public void setProcessorSpeed(int newSpeed)
    {
        processorSpeed = newSpeed;
    }
    
    public String getMemoryType()
    {
        return memoryType;
    }
    public int getMemorySpeed()
    {
        return memorySpeed;
    }
    public int getMemorySize()
    {
        return memorySize;
    }
    public int getHardDriveSize()
    {
        return hardDriveSize;
    }
    public int getNumberRevolutionsDisk ()
    {
        return numberRevolutionsDisk;
    }
    public String getProcessorModel()
    {
        return processorModel;
    }
    public int getProcessorSpeed()
    {
        return processorSpeed;
    }
    @Override
    public String toString(){
        return ("Tipo de memoria "+memoryType+
                "\nTamaño de memoria "+memorySize+
                "\nVelocidad de memoria "+memorySpeed+
                "\n\nTamaño disco duro "+hardDriveSize+
                "\nRevoluciones disco duro "+numberRevolutionsDisk+
                "\n\nModelo procesador "+processorModel+
                "\nVelocidad del procesador "+processorSpeed);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
