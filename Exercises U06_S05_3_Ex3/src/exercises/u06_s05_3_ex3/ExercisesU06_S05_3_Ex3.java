/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s05_3_ex3;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S05_3_Ex3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[][] array = new int[10][10];
        int i,j,aux;
        for (i = 0; i < array.length; i++) {
            aux = i;
            for (j = 0; j < array[i].length; j++) {
                array[i][j] = i;
                aux++;
            }
        }
        for (i = 0; i < array.length; i++) {
            for (j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
}
