/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Documentos;

/**
 *
 * @author Mati
 */
public class Factura {
    private String comprador;
    private double importe;
    public Factura(String nuevoComprador, double nuevoImporte)
    {
        comprador = nuevoComprador;
        importe = nuevoImporte;
    }
    public void imprimirInfo()
    {
        System.out.print("Factura a nombre de: ");
        System.out.println(comprador);
        System.out.print("Por un importe de: ");
        System.out.print(importe);
        System.out.println("");
    }
}
