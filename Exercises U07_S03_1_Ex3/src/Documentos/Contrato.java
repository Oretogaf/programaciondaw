/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Documentos;

/**
 *
 * @author Mati
 */
public class Contrato {
    private String nombre;
    private int duracionMeses;
    public Contrato(String nuevoNombre, int nuevaDuracion)
    {
        nombre = nuevoNombre;
        duracionMeses = nuevaDuracion;
    }
    public void imprimirInfo()
    {
        System.out.print("El contrato está a nombre de: ");
        System.out.println(nombre);
        System.out.print("Con una duración de ");
        System.out.print(duracionMeses);
        System.out.print(" meses");
        System.out.println("");
    }
}
