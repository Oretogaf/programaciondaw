/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Ex3;

/**
 *
 * @author Mati
 */

import java.util.Scanner;

public class DosNumeros {
public static Scanner stdin = new Scanner (System.in);

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        int numero1;
        int numero2;
        int suma;
        int resta;
        int producto;
        float division;
        System.out.println("Introduce un numero");
        numero1=stdin.nextInt();
        System.out.println("Introduce otro numero");
        numero2=stdin.nextInt();

        suma=numero1+numero2;
        System.out.println("La suma de "+numero1+" + "+numero2+" = "+suma);

        resta=numero1-numero2;
        System.out.println("La resta de "+numero1+" - "+numero2+" = "+resta);

        producto=numero1*numero2;
        System.out.println("El producto de "+numero1+" * "+numero2+" = "+producto);

        if (numero2!=0){
            division= (float)numero1/numero2;
            System.out.println("La división de "+numero1+" entre "+numero2+" = "+division);
        }
        else{
            System.out.println("No se puede dividir entre cero");
        }
    }
}
