/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s04_2_ex2;

/**
 *
 * @author Mati
 */
public class Team {
    private String teamName;
    private String name1;
    private String name2;
    private String name3;
    private String name4;
    private Competition competition1;
    private Competition competition2;
    public Team(){
        
    }
    public Team(String nameTeam, String nameP1, String nameP2, String nameP3, String nameP4, Competition comp1, Competition comp2){
        teamName = nameTeam;
        name1 = nameP1;
        name2 = nameP2;
        name3 = nameP3;
        name4 = nameP4;
        competition1 = comp1;
        competition2 = comp2;
    }
    public Team(Team other){
        if(other == null){
            System.out.println("Fatal error");
            System.exit(0);
        }else{
            teamName = other.teamName;
            name1 = other.name1;
            name2 = other.name2;
            name3 = other.name3;
            name4 = other.name4;
            competition1 = other.competition1;
            competition2 = other.competition2;
        }
    }
    public void setTeamName(String nameTeam){
        teamName = nameTeam;
    }
    public void setName1(String nameP1){
        name1 = nameP1;
    }
    public void setName2(String nameP2){
        name2 = nameP2;
    }
    public void setName3(String nameP3){
        name3 = nameP3;
    }
    public void setName4(String nameP4){
        name4 = nameP4;
    }
    public void setCompetition1(Competition comp1){
        competition1 = comp1;
    }
    public void setCompetition2(Competition comp2){
        competition2 = comp2;
    }
    public String getTeamName(){
        return teamName;
    }
    public String getName1(){
        return name1;
    }
    public String getName2(){
        return name2;
    }
    public String getName3(){
        return name3;
    }
    public String getName4(){
        return name4;
    }
    public Competition getCompetition1(){
        return competition1;
    }
    public Competition getCompetition2(){
        return competition2;
    }
    public boolean equals(Team other){
        return ((teamName == other.teamName) &&
                (name1 == other.name1) &&
                (name2 == other.name2) &&
                (name3 == other.name3) &&
                (name4 == other.name4) &&
                (competition1 == other.competition1) &&
                (competition2 == other.competition2));
    }
    @Override
    public String toString(){
        return ("Team name: " + teamName +
                "\nName of pilot 1: " + name1 +
                "\nName of pilot 2: " + name2 +
                "\nName of pilot 3: " + name3 +
                "\nName of pilot 4: " + name4 +
                "\nCompetition 1: " + competition1 +
                "\ncompetition 2: " + competition2 +
                "\n");
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Competition comp1 = new Competition("Piston cup", "Toro rosso", "maclaren", 2014);
        Competition comp2 = new Competition(comp1);
        System.out.println(comp1.equals(comp2));
        System.out.println("");
        comp2.setNamecompetition("Grand Prix of Monaco");
        comp2.setNameWinner("Renault");
        comp2.setNameRunner_up("Red Bull");
        comp2.setYearCompetition(2005);
        System.out.println(comp1.toString());
        System.out.println(comp2.toString());
        System.out.println(comp1.equals(comp2));
        System.out.println("");
        System.out.println(comp1.getNameCompetition());
        System.out.println(comp1.getNameWinner());
        System.out.println(comp1.getNameRunner_up());
        System.out.println(comp1.getYearCompetition());
        
        Team t1 = new Team("Red Willie", "Marcos", "Pepe", "Juan", "Ricardo", comp1, comp2);
        Team t2 = new Team(t1);
        System.out.println(t1.equals(t2));
        System.out.println("");
        t2.setTeamName("HRT");
        t2.setName1("Felipe");
        t2.setName2("Eduardo");
        t2.setName3("Pedro");
        t2.setName4("Francisco");
        t2.setCompetition1(comp2);
        t2.setCompetition2(comp1);
        System.out.println(t1.toString());
        System.out.println(t2.toString());
        System.out.println(t1.equals(t2));
        System.out.println("");
        System.out.println(t2.getTeamName());
        System.out.println(t2.getName1());
        System.out.println(t2.getName2());
        System.out.println(t2.getName3());
        System.out.println(t2.getName4());
        System.out.println(t2.getCompetition1());
        System.out.println(t2.getCompetition2());
    }
    
}
