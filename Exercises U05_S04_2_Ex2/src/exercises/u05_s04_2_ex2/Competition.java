/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s04_2_ex2;

/**
 *
 * @author Mati
 */
public class Competition {
    private String nameCompetition;
    private String nameWinner;
    private String nameRunner_up;
    private int yearCompetition;
    public Competition(){
        
    }
    public Competition(String nameComp, String nameWin, String nameRun, int yearComp){
        nameCompetition = nameComp;
        nameWinner = nameWin;
        nameRunner_up = nameRun;
        yearCompetition = yearComp;
    }
    public Competition(Competition other){
        if(other == null){
            System.out.println("Fatal error");
            System.exit(0);
        }else{
            nameCompetition = other.nameCompetition;
            nameWinner = other.nameWinner;
            nameRunner_up = other.nameRunner_up;
            yearCompetition = other.yearCompetition;
        }
    }
    public void setNamecompetition(String nameComp){
        nameCompetition = nameComp;
    }
    public void setNameWinner(String nameWin){
        nameWinner = nameWin;
    }
    public void setNameRunner_up(String nameRun){
        nameRunner_up = nameRun;
    }
    public void setYearCompetition(int yearComp){
        yearCompetition = yearComp;
    }
    public String getNameCompetition(){
        return nameCompetition;
    }
    public String getNameWinner(){
        return nameWinner;
    }
    public String getNameRunner_up(){
        return nameRunner_up;
    }
    public int getYearCompetition(){
        return yearCompetition;
    }
    public boolean equals(Competition other){
        return ((nameCompetition.equalsIgnoreCase(other.nameCompetition)) &&
                (nameWinner == other.nameWinner) &&
                (nameRunner_up == other.nameRunner_up) &&
                (yearCompetition == other.yearCompetition));
    }
    @Override
    public String toString(){
        return "Name of competition: " + nameCompetition +
                "\nName of winner: " + nameWinner +
                "\nName of runner up: " + nameRunner_up +
                "\nYear of competition: " + yearCompetition +
                "\n";
    }
}
