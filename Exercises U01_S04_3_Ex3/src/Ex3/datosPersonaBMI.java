/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Ex3;

import java.util.*;

/**
 *
 * @author Mati
 */
public class datosPersonaBMI {
    public static  Scanner stdin = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String name;
        String surname;
        char sex;
        char maritalStatus;
        int numberChildren;
        String address;
        String city;
        int postalCode;
        String country;
        double weightKilograms;
        double heightCentimeters;
        double heightMeters;
        double BMI;
        System.out.println("Introduzca su nombre");
        name=stdin.nextLine();
        System.out.println("Introduzca sus apellidos");
        surname=stdin.nextLine();
        System.out.println("Introduzca su sexo (M o F)");
        sex=stdin.nextLine().charAt(0);
        System.out.println("Introduzca su estado civil (S,M,W,D)");
        maritalStatus=stdin.nextLine().charAt(0);
        System.out.println("Introduzca numero de hijos");
        numberChildren=stdin.nextInt();
        stdin.nextLine();
        System.out.println("Introduzca dirección");
        address=stdin.nextLine();
        System.out.println("Introduzca ciudad");
        city=stdin.nextLine();
        System.out.println("Introduzca codigo postal");
        postalCode=stdin.nextInt();
        stdin.nextLine();
        System.out.println("Introduzca pais");
        country=stdin.next();
        System.out.println("Introduzca peso en KILOS");
        weightKilograms=stdin.nextInt();
        System.out.println("Introduzca altura en CENTIMETROS");
        heightCentimeters=stdin.nextInt();
        heightMeters=heightCentimeters/100.0;
        BMI=weightKilograms/(heightMeters*heightMeters);
        System.out.println("Hola me llamo "
                + name
                + " "
                + surname
                + ", soy "
                + sex
                + ", estoy "
                + maritalStatus
                + ", tengo "
                + numberChildren
                + " hijos.\nVivo en "
                + address
                + "  en "
                + city
                + ", mi numero postal es "
                + postalCode
                + ", soy de "
                + city
                + "."
                + ""
                + "\nPeso "
                + weightKilograms
                + " kilos, y mido "
                + heightCentimeters
                + " centimetros, mi BMI es "
                + BMI);
    }

}
