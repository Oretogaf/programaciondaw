/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mati
 */
public class Point2D {
    int pointX;
    int pointY;
    public Point2D(){
        pointX=0;
        pointY=0;
    }
    @Override
    public String toString(){
        return "("+pointX+","+pointY+")";
    }
    private boolean equals(Point2D otroPunto){
        return ((this.pointX==otroPunto.pointX)&&(this.pointY==otroPunto.pointY));
    }
    
    public static void main(String[] args){
        Point2D obj1 = new Point2D();
        System.out.println(obj1.toString());
        Point2D obj2 = new Point2D();
        System.out.println("Are equals: "+obj1.equals(obj2));
    }
}
