/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s07_1_ex2;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S07_1_Ex2 {
    static float[][][] notas = new float[20][5][4];
    public static Scanner uin = new Scanner(System.in);
    static void leerNotas(){
        for (int i = 0; i < notas.length; i++) {
            for (int j = 0; j < notas[i].length; j++) {
                for (int k = 0; k < (notas[i][j].length - 1); k++) {
                    System.out.println("Introduce la nota de la asignatura "
                            + (j + 1) + " del alumno " + (i + 1) + " en la "
                            + "evaluacion " + (k + 1));
                    notas[i][j][k] = uin.nextFloat();
                }
            }
        }
    }
    public static void calcularMediaAlumnoPorAsignatura(){
        float media;
        for (int i = 0; i < notas.length; i++) {
            for (int j = 0; j < notas[i].length; j++) {
                media = 0;
                for (int k = 0; k < notas[i][j].length; k++) {
                    if(k < (notas[i][j].length - 1)){
                        media += notas[i][j][k];
                    }else{
                        notas[i][j][k] = (media / (notas[i][j].length - 1));
                    }
                }
            }
        }
    }
    public static void calcularMedia1Alumno(){
        int alumno;
        System.out.println("De que alumno dese saber la nota");
        alumno = uin.nextInt();
        int i = alumno;
        i--;
        for (int j = 0; j < notas[i].length ; j++) {
            int index = (notas[i][j].length - 1);
            System.out.println("El alumno " + (i + 1)
                    + " tiene una media de "
                    + notas[i][j][index]
                    + " en la asignatura " + (j + 1));
        }
    }
    public static void calcularMediaTotal(){
        float media = 0;
        int aux = 0;
        for (int i = 0; i < notas.length; i++) {
            for (int j = 0; j < notas[i].length; j++) {
                media += notas[i][j][3];
                aux++;
            }
        }
        media /= aux;
        System.out.println("La media total es " + media);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        leerNotas();
        calcularMediaAlumnoPorAsignatura();
        calcularMedia1Alumno();
        calcularMediaTotal();
    }
    
}
