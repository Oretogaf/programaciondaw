/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package test;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/**
 *
 * @author Mati
 */
public class InnerListenersDemo extends JFrame {
    public static final int WIDTH = 300;
    public static final int HEIGHT = 200;
    
    private JPanel redPanel;
    private JPanel whitePanel;
    private JPanel bluePanel;
    
    private class ColorListener implements ActionListener {
        JPanel jp;
        Color clr;
        
        public ColorListener(JPanel jp, Color clr) {
            this.jp = jp;
            this.clr = clr;
        }
        
        @Override
        public void actionPerformed(ActionEvent ae) {
            jp.setBackground(clr);
        }
    }
    
    public static void main(String[] args) {
        InnerListenersDemo gui = new InnerListenersDemo();
        gui.setVisible(true);
    }
    
    public InnerListenersDemo(){
        super("Menu demostration");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(3);
        setLayout(new GridLayout(1, 3));
        
        redPanel = new JPanel();
        redPanel.setBackground(Color.LIGHT_GRAY);
        add(redPanel);
        
        whitePanel = new JPanel();
        whitePanel.setBackground(Color.LIGHT_GRAY);
        add(whitePanel);
        
        bluePanel = new JPanel();
        bluePanel.setBackground(Color.LIGHT_GRAY);
        add(bluePanel);
        
        JMenu colorMenu = new JMenu("Add colors");
        
        JMenuItem redChoice = new JMenuItem("Red");
        redChoice.addActionListener(new ColorListener(redPanel, Color.RED));
        colorMenu.add(redChoice);
        
        JMenuItem whiteChoice = new JMenuItem("White");
        whiteChoice.addActionListener(new ColorListener(whitePanel, Color.WHITE));
        colorMenu.add(whiteChoice);
        
        JMenuItem blueChoice = new JMenuItem("Blue");
        blueChoice.addActionListener(new ColorListener(bluePanel, Color.BLUE));
        colorMenu.add(blueChoice);
        
        JMenuBar bar = new JMenuBar();
        bar.add(colorMenu);
        setJMenuBar(bar);
    }
}
