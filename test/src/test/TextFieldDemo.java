package test;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JTextArea;


public class TextFieldDemo extends JFrame implements ActionListener
{
    public static final int WIDTH = 400;
    public static final int HEIGHT = 200;
    public static final int NUMBER_OF_ROWS = 30;
    public static final int NUMBER_OF_COLUMS = 5;

    private JTextArea story;
    private int lineCount;


    public static void main(String[] args)
    {
        TextFieldDemo gui = new TextFieldDemo( );
        gui.setVisible(true);
    }


    public TextFieldDemo( )
    {
        super("Text Area Demo");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(2, 1));

        JPanel namePanel = new JPanel( ); 
        namePanel.setLayout(new BorderLayout( ));
        namePanel.setBackground(Color.WHITE); 

        story = new JTextArea(NUMBER_OF_ROWS, NUMBER_OF_COLUMS);

        namePanel.add(story, BorderLayout.CENTER);
        JLabel nameLabel = new JLabel("Enter your story here:");
        namePanel.add(nameLabel, BorderLayout.NORTH);

        add(namePanel);
        
        JPanel buttonPanel = new JPanel( );
        buttonPanel.setLayout(new FlowLayout( ));
        buttonPanel.setBackground(Color.PINK); 
        JButton actionButton = new JButton("Click me"); 
        actionButton.addActionListener(this);
        buttonPanel.add(actionButton);

        JButton clearButton = new JButton("Clear"); 
        clearButton.addActionListener(this);
        buttonPanel.add(clearButton); 

        add(buttonPanel);
    }


    public void actionPerformed(ActionEvent e) 
    {
        String actionCommand = e.getActionCommand( );

        if (actionCommand.equals("Click me")){
            lineCount = story.getLineCount();
            story.setText("Your story is " + lineCount + " lines long.");
        }else if (actionCommand.equals("Clear")){
            story.setText("");
        }else{
            story.setText("Unexpected error.");
        }
    } 

}