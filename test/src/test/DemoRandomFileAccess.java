/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.DecimalFormat;

/**
 *
 * @author Mati
 */
public class DemoRandomFileAccess extends RandomAccessFile implements Serializable{
    DecimalFormat form = new DecimalFormat("#.#");
    
    public DemoRandomFileAccess(File file, String string) throws FileNotFoundException {
        super(file, string);
    }
    public DemoRandomFileAccess(String string, String string1) throws FileNotFoundException {
        super(string, string1);
    }
    
    public void writeObject(Obj obj) throws IOException{
        writeFloat(obj.getNum1());
        writeInt(obj.getNum2());
    }
    
    public Obj readObject() throws IOException{
        Obj fin;
        String str = form.format(readFloat());
        str = str.replace(",", ".");
        Float flo = Float.parseFloat(str);
        fin = new Obj(flo, readInt());
        return fin;
    }
}
