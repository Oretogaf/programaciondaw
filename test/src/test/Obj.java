/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.Serializable;

/**
 *
 * @author Mati
 */
public class Obj implements Serializable{
    private float num1;
    private int num2;

    public Obj(float num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public float getNum1() {
        return num1;
    }
    public void setNum1(float num1) {
        this.num1 = num1;
    }
    public int getNum2() {
        return num2;
    }
    public void setNum2(int num2) {
        this.num2 = num2;
    }

    @Override
    public String toString() {
        return "obj{" + "num1=" + num1 + ", num2=" + num2 + '}';
    }
}
