/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u09_s05_ex4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Mati
 */
public class Ejercicio4 {
    public static void leerPrimeraLineaArchivo(String uri) throws FileNotFoundException, IOException {
        FileReader fil = new FileReader(uri);
        BufferedReader buf = new BufferedReader(fil);
        System.out.println(buf.readLine());
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        try{
            leerPrimeraLineaArchivo("\\\\2-12-00\\Carpeta compartida\\PRO\\Unit 9\\Exercises\\Ejercicio6\\hola.txt");
        }catch (FileNotFoundException e){
            System.out.println("La ruta de archivo no es correcta");
        }catch (IOException e){
            System.out.println("Error de entrada-escritura");
        }
    }
    
}
