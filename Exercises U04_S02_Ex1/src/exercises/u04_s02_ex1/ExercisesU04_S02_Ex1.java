/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s02_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU04_S02_Ex1 {
    public static void evenOdd(int num){
        if(num%2==0){
            System.out.println("Even");
        }else{
            System.out.println("Odd");
        }
    }
    public static void positiveNegative(int num){
        if(num>=0){
            System.out.println("Positive");
        }else{
            System.out.println("Negative");
        }
    }
    public static double square(int num){
        return (num*num);
        
    }
    public static double cube(int num){
        return num*num*num;
    }
    public static int count(int num){
        return (num+1);
    }
    public static Scanner stdin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int num;
        System.out.println("Introduce el numero");
        num=stdin.nextInt();
        evenOdd(num);
        positiveNegative(num);
        System.out.println(square(num));
        System.out.println(cube(num));
        System.out.println(count(num));
    }
    
}
