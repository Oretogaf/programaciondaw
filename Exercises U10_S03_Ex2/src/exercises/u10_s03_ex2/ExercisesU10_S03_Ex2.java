/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u10_s03_ex2;

import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;

/**
 *
 * @author Mati
 */
public class ExercisesU10_S03_Ex2 {
    static SimpleDateFormat form = new SimpleDateFormat("yyyyMMdd");
    static Scanner uin = new Scanner(System.in, "iso-8859-1");
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<String> texto = new ArrayList<>();
        String line;
        System.out.println("¿Que quieres almacenar hoy?");
        System.out.println("Introduce GUARDAR para salir del programa y guardar");
        System.out.println("Y SALIR para salir del programa sin guardar");
        while (true) {
            line = uin.nextLine();
            if(line.equals("GUARDAR")){
                break;
            }else if(line.equals("SALIR")){
                fileActions();
                System.exit(0);
            }else{
                texto.add(line);
            }
        }
        File f = fileActions();
        
        PrintWriter pw = null;
        try{
            pw = new PrintWriter(f);
        }
        catch (FileNotFoundException ex){
            System.out.println("No se pudo acceder al fichero");
        }
        
        pw.write(texto.toString());
        pw.flush();
        pw.close();
    }

    private static File fileActions() {
        Date d = new Date();
        String date = form.format(d);
        date += ".txt";
        File f = new File(date);
        if(f.exists()){
            f.delete();
        }
        try{
            f.createNewFile();
        }
        catch (IOException ex)
        {
            System.out.println("No se pudo crear el fichero");
        }
        return f;
    }
    
}
