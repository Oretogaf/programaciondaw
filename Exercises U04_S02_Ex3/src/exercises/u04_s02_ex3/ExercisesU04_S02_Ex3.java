/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s02_ex3;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU04_S02_Ex3 {
    public static int Add(){
        int n1;
        int n2;
        System.out.print("Number 1:");
        n1=uin.nextInt();
        System.out.println("");
        System.out.print("Number 2:");
        n2=uin.nextInt();
        System.out.println("");
        return (n1+n2);
    }
    public static int Subtract(){
        int n1;
        int n2;
        System.out.print("Number 1:");
        n1=uin.nextInt();
        System.out.println("");
        System.out.print("Number 2:");
        n2=uin.nextInt();
        System.out.println("");
        return (n1-n2);
    }
    public static int Multiply(){
        int n1;
        int n2;
        System.out.print("Number 1:");
        n1=uin.nextInt();
        System.out.println("");
        System.out.print("Number 2:");
        n2=uin.nextInt();
        System.out.println("");
        return (n1*n2);
    }
    public static int Divide(){
        int n1;
        int n2;
        System.out.print("Number 1:");
        n1=uin.nextInt();
        System.out.println("");
        System.out.print("Number 2:");
        n2=uin.nextInt();
        System.out.println("");
        return (n1/n2);
    }
    public static void menu(){
        int op;
        int n1;
        int n2;
        System.out.println("1-Add");
        System.out.println("2-Subtract");
        System.out.println("3-Multiply");
        System.out.println("4-Divide");
        System.out.println("5-Get out");
        System.out.println("Select the desired operation: ");
        op=uin.nextInt();
        switch(op){
            case 1:
                System.out.println(Add());
                break;
            case 2:
                System.out.println(Subtract());
                break;
            case 3:
                System.out.println(Multiply());
                break;
            case 4:
                System.out.println(Divide());
                break;
            default:
                System.exit(0);
                break;
        }
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       menu(); 
    }
    
}
