/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s10_1_ex4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S10_1_Ex4 {
public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int s;
        System.out.println("Introduce el numero de Bucle seguido de 1 para while y 2 para do while");
        s=uin.nextInt();
        switch(s){
            case 11:
                System.out.println("");
                ex11();
            break;
            case 12:
                System.out.println("");
                ex12();
            break;
            case 21:
                System.out.println("");
                ex21();
            break;
            case 22:
                System.out.println("");
                ex22();
            break;
            case 31:
                System.out.println("");
                ex31();
            break;
            case 32:
                System.out.println("");
                ex32();
            break;
            case 41:
                System.out.println("");
                ex41();
            break;
            case 42:
                System.out.println("");
                ex42();
            break;
            default:
            break;
        }
    }
    
    static void ex11(){
        int i;
        i=0;
        while(i<20){
            if((i%2)==0){
                System.out.println(i*2);
            }else{}
            i++;
        }
    }
    static void ex12(){
        int i;
        i=0;
        do{
            if((i%2)==0){
                System.out.println(i*2);
            }else{}
            i++;
        }while(i<20);
    }
    
    static void ex21(){
        int i;
        i=20;
        while(i>0){
            if((i%2)==0){
                System.out.println(i);
            }else{}
            i--;
        }
    }
    static void ex22(){
        int i;
        i=20;
        do{
            if((i%2)==0){
                System.out.println(i);
            }else{}
            i--;
        }while(i>0);
    }

    static void ex31(){
        int i;
        int mysteryInt;
        mysteryInt=100;
        i=5;
        while(i>0){
            mysteryInt-=i;
            System.out.println(mysteryInt);
            i--;
        }
    }
    static void ex32(){
        int i;
        int mysteryInt;
        mysteryInt=100;
        i=5;
        do{
            mysteryInt-=i;
            System.out.println(mysteryInt);
            i--;
        }while(i>0);
    }

    static void ex41(){
        int i;
        int mysteryInt;
        i=5;
        while(i>0){
            mysteryInt=100;
            mysteryInt-=i;
            System.out.println(mysteryInt);
            i--;
        }
    }
    static void ex42(){
        int i;
        int mysteryInt;
        i=5;
        do{
            mysteryInt=100;
            mysteryInt-=i;
            System.out.println(mysteryInt);
            i--;
        }while(i>0);
    }
}
