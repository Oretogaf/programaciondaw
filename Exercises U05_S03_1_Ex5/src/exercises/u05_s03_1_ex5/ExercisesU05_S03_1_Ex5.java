/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_1_ex5;

/**
 *
 * @author Mati
 */
public class ExercisesU05_S03_1_Ex5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String s = "Uno dosuno";
        int i;
        System.out.println("Posiciones del caracter o");
        for(i = 0 ; i < s.length() ; i++){
            if(s.charAt(i) == 'o'){
                System.out.println(i);
            }
        }
        System.out.println("");
        System.out.println("Posiciones de la cadena no");
        i = s.indexOf("no");
        if (i == -1){
            System.exit(0);
        }
        System.out.println(i);
        i++;
        for(int c = 0; c <= 10; c++){
            i++;
            i = s.indexOf("no",i);
            if (i == -1){
                System.exit(0);
            }
            System.out.println(i);
        }
    }
    
}
