/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s03_ex3;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Number {
    private int elNumero;
    public Number(int ini){
        elNumero=ini;
    }
    public void add(int add){
        elNumero+=add;
    }
    public void subtract(int subtract){
        elNumero-=subtract;
    }
    public int getValue(){
        return elNumero;
    }
    public int getTwice(){
        return elNumero*2;
    }
    public int getTriple(){
        return elNumero*3;
    }
    public void setValue(){
        
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int ini;
        int add;
        int subtract;
        System.out.println("Introduce el valor inicial del numero");
        ini=uin.nextInt();
        Number eN1 = new Number(ini);
        System.out.println(eN1.getValue());
        System.out.println(eN1.getTwice());
        System.out.println(eN1.getTriple());
        
        System.out.println("Introduce el valor que quieres aumentar el numero");
        add=uin.nextInt();
        eN1.add(add);
        System.out.println(eN1.getValue());
        System.out.println(eN1.getTwice());
        System.out.println(eN1.getTriple());
        
        System.out.println("Introduce el valor que quieres reducir el numero");
        subtract=uin.nextInt();
        eN1.subtract(subtract);
        System.out.println(eN1.getValue());
        System.out.println(eN1.getTwice());
        System.out.println(eN1.getTriple());
    }
    
}
