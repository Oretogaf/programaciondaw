/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s03_ex3;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S03_Ex3 {
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int numAlu;
        int i;
        float[] notasAlu;
        float media;
        int supMedia;
        int noSupMedia;
        do{
            System.out.println("Introduce el numero de alumnos de la clase");
            numAlu = uin.nextInt();
            if(numAlu <= 0){
                System.out.println("Valor incorrecto.");
            }
        }while(numAlu <= 0);
        notasAlu = new float[numAlu];
        media = 0;
        for(i = 0; i < notasAlu.length; i++){
            do{
                System.out.println("Introduce la nota del alumno " + i);
                notasAlu[i] = uin.nextFloat();
                if(notasAlu[i] < 0){
                    System.out.println("Valor incorrecto. Valor inferior a 0");
                }else if(notasAlu[i] > 10){
                    System.out.println("Valor incorrecto. Valor superior a 10");
                }
            }while(notasAlu[i] < 0 || notasAlu[i] > 10);
            media += notasAlu[i];
        }
        media /= notasAlu.length;
        supMedia = 0;
        noSupMedia = 0;
        for(i = 0; i < notasAlu.length; i++){
            if(notasAlu[i] >= media){
                supMedia++;
            }else{
                noSupMedia++;
            }
        }
        System.out.println("La media de la clase es " + media);
        if(supMedia == 1){
            System.out.println("Existe un alumno que supera o iguala la media");
        }else{
            System.out.println("Existen " + supMedia + " alumnos que superan o igualan la media");
        }
        if(noSupMedia == 1){
            System.out.println("Existe un alumno que no supera ni iguala la media");
        }else{
            System.out.println("Existen " + noSupMedia + " alumnos que no superan ni igualan la media");
        }
    }
    
}
