/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit4;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class DateThirdTry {
    private int day;
    private String month;
    private int year;
    
    public void readInput(){
        Scanner stdin = new Scanner(System.in);
        System.out.println("Introduce de month, day and year");
        month=stdin.next();
        day=stdin.nextInt();
        year=stdin.nextInt();
    }
    public void writeOutput(){
        System.out.println(month+" "+day+", "+year);
    }
    public int getMonth(){
        if(month.equalsIgnoreCase("january")){
            return 1;
        }else if(month.equalsIgnoreCase("february")){
            return 2;
        }else if(month.equalsIgnoreCase("march")){
            return 3;
        }else if(month.equalsIgnoreCase("april")){
            return 4;
        }else if(month.equalsIgnoreCase("may")){
            return 5;
        }else if(month.equalsIgnoreCase("june")){
            return 6;
        }else if(month.equalsIgnoreCase("july")){
            return 7;
        }else if(month.equalsIgnoreCase("august")){
            return 8;
        }else if(month.equalsIgnoreCase("september")){
            return 9;
        }else if(month.equalsIgnoreCase("october")){
            return 10;
        }else if(month.equalsIgnoreCase("november")){
            return 11;
        }else if(month.equalsIgnoreCase("december")){
            return 12;
        }else{
            System.out.println("Fatal error");
            System.exit(0);
            return 0;
        }
    }
    public String monthString(int numberMonth){
        switch(numberMonth){
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                System.out.println("Fatal Error");
                System.exit(0);
        }
        return "fatal error";
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DateThirdTry data = new DateThirdTry();
        int numberMonth;
        String stringMonth;
        data.readInput();
        data.writeOutput();
        numberMonth=data.getMonth();
        stringMonth=data.monthString(numberMonth);
        System.out.println(stringMonth);
    }
    
}
