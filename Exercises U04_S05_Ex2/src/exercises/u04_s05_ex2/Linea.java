/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s05_ex2;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Linea {
    private int tamaño;
    private char caracter;
    public Linea(int tam){
        tamaño=tam;
        caracter='*';
    }
    public Linea(int tam, char car){
        tamaño=tam;
        caracter=car;
    }
    public void visualizarLinea(){
        int i;
        for(i=1;i<=tamaño;i++){
            System.out.print(caracter);
        }
        System.out.println();
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int tam;
        char car;
        System.out.println("Introduce el tamaño de la linea 1");
        tam=uin.nextInt();
        Linea lin1 = new Linea(tam);
        System.out.println("Introduce el tamaño de la linea 2");
        tam=uin.nextInt();
        System.out.println("Introduce el caracter de la linea 2");
        car=uin.next().charAt(0);
        Linea lin2 = new Linea(tam, car);
        System.out.println("");
        System.out.println("Linea 1");
        lin1.visualizarLinea();
        System.out.println("Linea 2");
        lin2.visualizarLinea();
    }
    
}
