/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u07_s03_2_ex18;

/**
 *
 * @author Mati
 */
public class Disco {
    protected String fabricante; protected int numSerie;
    public Disco (String nuevoFabricante, int nuevoNumSerie) {
        fabricante = nuevoFabricante;
        numSerie = nuevoNumSerie;
    }
    public void imprimirFabricante() {
        System.out.println(fabricante);
    }
    public void imprimirNumeroDeSerie() {
        System.out.println(numSerie);
    }
}