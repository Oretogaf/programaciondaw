/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s03_2_ex18;

/**
 *
 * @author Mati
 */
public class DiscoDatos extends Disco{
    protected float capacidad;
    protected String tecnologia;
    public DiscoDatos(String nuevoFabricante, int nuevoNumSerie, float capacidad, String tecnologia) {
        super(nuevoFabricante, nuevoNumSerie);
        this.capacidad = capacidad;
        this.tecnologia = tecnologia;
    }
     public void imprimirCapacidad(){
         System.out.println(capacidad);
     }
     public void imprimirTecnologia(){
         System.out.println(tecnologia);
     }
}
