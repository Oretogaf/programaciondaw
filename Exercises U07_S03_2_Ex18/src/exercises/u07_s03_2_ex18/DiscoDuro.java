/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u07_s03_2_ex18;

/**
 *
 * @author Mati
 */
public class DiscoDuro extends DiscoDatos{
    protected String interfaz;
    protected int numeroPlatos;
    protected int revoluciones;
    
    public DiscoDuro(String interfaz, int numeroPlatos, int revoluciones, String nuevoFabricante, int nuevoNumSerie, float capacidad, String tecnologia) {
        super(nuevoFabricante, nuevoNumSerie, capacidad, tecnologia);
        this.interfaz = interfaz;
        this.numeroPlatos = numeroPlatos;
        this.revoluciones = revoluciones;
    }
    public void imprimirInterfaz(){
        System.out.println(interfaz);
    }
    public void imprimirNumPlatos(){
        System.out.println(numeroPlatos);
    }
    public void imprimirRevoluciones(){
        System.out.println(revoluciones);
    }
    
    public static void main(String[] args) {
        DiscoDuro discoDuro;
        String interfaz;
        int numeroPlatos;
        int revoluciones;
        String nuevoFabricante;
        int nuevoNumSerie;
        float capacidad;
        String tecnologia;
        interfaz = "SATA";
        numeroPlatos = 5;
        revoluciones = 7200;
        nuevoFabricante = "Seagate";
        nuevoNumSerie = 10427614;
        capacidad = 900.45f;
        tecnologia = "Magnetico";
        discoDuro = new DiscoDuro(interfaz, numeroPlatos, revoluciones, nuevoFabricante, nuevoNumSerie, capacidad, tecnologia);
        discoDuro.imprimirInterfaz();
        discoDuro.imprimirNumPlatos();
        discoDuro.imprimirRevoluciones();
        discoDuro.imprimirFabricante();
        discoDuro.imprimirNumeroDeSerie();
        discoDuro.imprimirCapacidad();
        discoDuro.imprimirTecnologia();
    }
}
