/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reviewexercisesarrays_ex5;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class CharacterFrequency {
    public static int[] numberOfNumbers = new int[10];
    public static Scanner uin = new Scanner(System.in);
    
    public static void count(String telf){
        for(char dig : telf.toCharArray()){
            numberOfNumbers[Integer.parseInt(Character.toString(dig))]++;
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String telf;
        do{
            System.out.println("Introduce el numero de telefono");
            telf = uin.next();
        }while(telf.length() != 9);
        count(telf);
        for (int i = 0; i < numberOfNumbers.length; i++) {
            System.out.println("El numero " + i + " aparece " + numberOfNumbers[i] + " veces");
        }
    }
    
}
