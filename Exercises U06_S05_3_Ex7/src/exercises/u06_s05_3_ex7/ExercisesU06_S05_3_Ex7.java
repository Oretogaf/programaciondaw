/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s05_3_ex7;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S05_3_Ex7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[][] v = new int[10][3];
        int i,j,k,a;
        for(i = 0; i < v.length; i++){
            k = 1;
            a = i + 1;
            for (j = 0; j < v[i].length; j++) {
                v[i][j] = (int) Math.pow(a, k);
                k++;
            }
        }
        for(i = 0; i < v.length; i++){
            for (j = 0; j < v[i].length; j++) {
                System.out.print(v[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
}
