/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package exercises.u12_s03_ex3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ArtGallery {
    static LinkedList<Picture> list = new LinkedList();
    static Scanner uin = new Scanner(System.in);
    static File fil = new File("salida.dat");
    
    private static void menu(){
        int index;
        System.out.println("1.Introducir datos de una obra\n"
                + "2.Ver información de una obra\n"
                + "3.Vender una obra\n"
                + "4.Salir");
        index = uin.nextInt();
        System.out.println();
        switch(index){
            case 1:
                introducirDatos();
                writeMe();
                break;
            case 2:
                imprimirDatos();
                break;
            case 3:
                venderObra();
                writeMe();
                break;
            case 4:
                System.out.println();
                System.out.println("Goodbye!!");
                writeMe();
                System.exit(0);
                break;
            default:
                System.out.println("Fatal error");
                System.exit(0);
                break;
        }
        System.out.println();
        menu();
    }
    private static void introducirDatos(){
        int pictureCode;
        String title;
        String autor;
        int year;
        int width;
        int height;
        int numUnits;
        float price;
        
        uin.nextLine();
        System.out.println("Introduce el codigo de la obra ");
        pictureCode = uin.nextInt();
        uin.nextLine();
        System.out.println("Introduce el nombre de la obra");
        title = uin.nextLine();
        System.out.println("Introduce el autor");
        autor = uin.nextLine();
        System.out.println("Introduce el año");
        year = uin.nextInt();
        System.out.println("Introduce el ancho en centimetros");
        width = uin.nextInt();
        System.out.println("Introduce la altura en centimetros");
        height = uin.nextInt();
        System.out.println("Introduce el numero de ejemplares");
        numUnits = uin.nextInt();
        System.out.println("Introduce el precio");
        price = uin.nextFloat();
        list.add(new Picture(pictureCode, title, autor, year, width, height, numUnits, price));
        System.out.println();
    }
    private static void imprimirDatos(){
        for(Picture pic : list){
            System.out.println(pic.toString());
        }
    }
    private static void venderObra(){
        int cod;
        char comp;
        System.out.println("Introduce el codigo de la obra que desea comprar");
        cod = uin.nextInt();
        for(Picture pic : list){
            if(pic.getPictureCode() == cod){
                if(pic.getNumUnits() > 0){
                    System.out.println("El precio de la obra son " + pic.getPrice() + "€, ¿Desea comprarlo?");
                    comp = uin.next().charAt(0);
                    comp = Character.toUpperCase(comp);
                    if(comp == 'S'){
                        System.out.println("Muchas gracias por su compra");
                        pic.setNumUnits(pic.getNumUnits() - 1);
                    }
                }else{
                    System.out.println("Lo sentimos. No nos quedan ejemplares de esa obra");
                }
            }
        }
    }
    
    private static void writeMe(){
        if(!fil.exists()){
            try {
                fil.createNewFile();
            } catch (IOException ex) {
                System.out.println("Se produjo un error al crear el archivo");
                System.out.println(ex.getMessage());
            }
        }
        ObjectOutputStream out = null;
        try{
            out = new ObjectOutputStream(new FileOutputStream(fil));
        }
        catch (IOException ex)
        {
            System.out.println("Se produjo un error al intentar abrir el archivo");
            System.out.println(ex.getMessage());
        }
        
        try{
            out.writeObject(list);
            out.flush();
        }
        catch (IOException ex){
            System.out.println("Se produjo un error al intentar guardar los archivos");
            System.out.println(ex.getMessage());
        }
        finally
        {
            try{
                out.close();
            } catch (IOException ex){
                System.out.println("Se produjo un error al intentar cerrar el archivo");
                System.out.println(ex.getMessage());
            }
        }
    }
    private static void readMe(){
        ObjectInputStream in = null;
        try{
            in = new ObjectInputStream(new FileInputStream(fil));
        }
        catch (IOException ex)
        {
            System.out.println("Se produjo un error al intentar abrir el archivo");
            System.out.println(ex.getMessage());
        }
        
        try{
            list = (LinkedList<Picture>) in.readObject();
        }
        catch (IOException ex){
            System.out.println("Se produjo un error al intentar leer los archivos");
            System.out.println(ex.getMessage());
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("Se produjo un error al intentar encontrar la clase de los archivos");
            System.out.println(ex.getMessage());
        }
        finally
        {
            try{
                in.close();
            } catch (IOException ex){
                System.out.println("Se produjo un error al intentar cerrar el archivo");
                System.out.println(ex.getMessage());
            }
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if(fil.exists()){
            readMe();
        }
        menu();
    }
    
}
