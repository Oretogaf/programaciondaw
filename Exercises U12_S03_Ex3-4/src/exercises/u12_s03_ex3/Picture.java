/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u12_s03_ex3;

import java.io.Serializable;

/**
 *
 * @author Mati
 */
public class Picture implements Serializable{
    private int pictureCode;
    private String title;
    private String autor;
    private int year;
    private int width;
    private int height;
    private int numUnits;
    private float price;

    public Picture(int pictureCode, String title, String autor, int year, int width, int height, int numUnits, float price) {
        this.pictureCode = pictureCode;
        this.title = title;
        this.autor = autor;
        this.year = year;
        this.width = width;
        this.height = height;
        this.numUnits = numUnits;
        this.price = price;
    }

    public int getPictureCode() {
        return pictureCode;
    }
    public String getTitle() {
        return title;
    }
    public String getAutor() {
        return autor;
    }
    public int getYear() {
        return year;
    }
    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
    public int getNumUnits() {
        return numUnits;
    }
    public float getPrice() {
        return price;
    }

    public void setPictureCode(int pictureCode) {
        this.pictureCode = pictureCode;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setAutor(String autor) {
        this.autor = autor;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    public void setNumUnits(int numUnits) {
        this.numUnits = numUnits;
    }
    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Picture{" + "pictureCode=" + pictureCode + ", title=" + title + ", autor=" + autor + ", year=" + year + ", width=" + width + ", height=" + height + ", numUnits=" + numUnits + ", price=" + price + '}';
    }
}
