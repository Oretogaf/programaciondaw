/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_2_ex10;

/**
 *
 * @author Mati
 */
public class ConvierteAString2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Integer n3 = new Integer("32");
        String s1 = new String(n3.toString());
        Float r3 = new Float(3.1416);
        String s2 = new String(r3.toString());
        Character car = new Character('A');
        String s3 = new String (car.toString());
    }
    
}
