/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s05_ex3;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Autores {
    static int IRPF;
    private String nombre;
    private float ganancias;
    public static Scanner uin = new Scanner(System.in);
    public Autores(){
        
    }
    public void pedirDatos(){
        pedirNombre();
        pedirGananciasAutor();
    }
    public String pedirCadena(){
        return uin.nextLine();
    }
    public void pedirNombre(){
        System.out.println("Introduce el nombre del autor");
        nombre=pedirCadena();
    }
    public void pedirGananciasAutor(){
        System.out.println("Introduce las ganancias del autor");
        ganancias=uin.nextFloat();
        uin.nextLine();
    }
    public void pedirIRPF(){
        System.out.println("Introduce el IRPF");
        IRPF=uin.nextInt();
        uin.nextLine();
    }
    public void visualizarDatosAutor(){
        System.out.println("El autor se llama "+nombre
                + " las ganancias "+ganancias
                +" y el IRPF "+IRPF);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Autores autor1 = new Autores();
        autor1.pedirIRPF();
        autor1.pedirDatos();
        
        Autores autor2 = new Autores();
        autor2.pedirNombre();
        autor2.pedirGananciasAutor();
        
        Autores autor3 = new Autores();
        autor3.pedirDatos();
        
        autor1.visualizarDatosAutor();
        autor2.visualizarDatosAutor();
        autor3.visualizarDatosAutor();
        
        autor2.pedirIRPF();
        
        autor1.visualizarDatosAutor();
        autor2.visualizarDatosAutor();
        autor3.visualizarDatosAutor();
    }
    
}
