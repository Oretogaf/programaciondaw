/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s03_ex6;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class Discount {
    public Discount(){
        
    }
    public float viewReducedPrice(int originalPrice, int discountRate){
        return discountRate*originalPrice/100;
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int originalPrice;
        int discountRate;
        Discount des1 = new Discount();
        System.out.println("Introduce el precio inicial");
        originalPrice=uin.nextInt();
        System.out.println("Introduce el porcentaje de descuento");
        discountRate=uin.nextInt();
        System.out.println(des1.viewReducedPrice(originalPrice, discountRate));
    }
    
}
