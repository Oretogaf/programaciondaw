/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package countfamiles;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class CountFamiles {
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int k;
        double maxInc = 0;
        double lim;
        int min = 0;
        
        do{
            System.out.println("Introduce el numero de familias");
            k = uin.nextInt();
        }while(k <= 0);
        double[] family = new double[k];
        for (int i = 0; i < family.length; i++){
            System.out.println("Introduce los ingresos de la familia " + (i + 1));
            family[i] = uin.nextDouble();
            if(family[i] > maxInc){
                maxInc = family[i];
            }
        }
        lim = maxInc * 0.1;
        for(double fam : family){
            if(fam < lim){
                min++;
            }
        }
        switch(min){
            case 0:
                System.out.println("No exiten familias con ingresos inferiores al 10% de la familia con los ingresos máximos");
                break;
            case 1:
                System.out.println("Existe 1 familia con ingresos inferiores al 10% de la familia con los ingresos máximos donde los ingresos han sido: ");
                break;
            default:
                System.out.print("Existen " + min + " familias con ingresos inferiores al 10% de la familia con los ingresos máximos donde los ingresos ha sido: ");
                break;
        }
        boolean first = false;
        for(double fam : family){
            if(fam < lim){
                if(!first){
                    System.out.print(fam);
                    first = true;
                }else{
                    System.out.print(" , " + fam);
                }
            }
        }
        System.out.print(".\n");
    }  
}
