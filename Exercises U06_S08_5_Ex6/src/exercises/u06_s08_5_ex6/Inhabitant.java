/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_5_ex6;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Mati
 */
public class Inhabitant {
    private int idNumber;
    private String name;
    private String townName;
    private Calendar born;

    public Inhabitant(int idNumber, String name, String townName, Calendar born) {
        this.idNumber = idNumber;
        this.name = name;
        this.townName = townName;
        this.born = born;
    }

    public int getIdNumber() {
        return idNumber;
    }
    public String getName() {
        return name;
    }
    public String getTownName() {
        return townName;
    }
    public Calendar getBorn() {
        return born;
    }

    public void setIdNumber(int idNumber) {
        this.idNumber = idNumber;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setTownName(String townName) {
        this.townName = townName;
    }
    public void setBorn(GregorianCalendar born) {
        this.born = born;
    }

    @Override
    public String toString() {
        return "Inhabitant{" + "IdNumber=" + idNumber + ", name=" + name + ", townName=" + townName + ", born=" + born.get(Calendar.DAY_OF_MONTH) + "/" + born.get(Calendar.MONTH) + "/" + born.get(Calendar.YEAR) + '}';
    }
    
}
