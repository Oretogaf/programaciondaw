/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s08_5_ex6;

import java.util.Scanner;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Mati
 */
public class TvProgram {
    public static Scanner uin = new Scanner(System.in);
    public static Inhabitant[] oldMan;
    
    public static void inicialiceArray(){
        int inMax;
        do{
            System.out.println("Introduce el numero de pueblos");
            inMax = uin.nextInt();
        }while(inMax > 100 || inMax < 0);
        oldMan = new Inhabitant[inMax];
    }
    private static void menu(){
        char index;
        System.out.println("A.Introducir datos de un habitante\n"
                + "B.Ver el hombre mas viejo\n"
                + "C.Ver información de los habitantes\n"
                + "D.Ordenar de más joven a más mayor\n"
                + "E.Salir");
        index = uin.next().charAt(0);
        index = Character.toUpperCase(index);
        System.out.println();
        switch(index){
            case 'A':
                introduceData();
                break;
            case 'B':
                verHombreMasViejo();
                break;
            case 'C':
                verVector();
                break;
            case 'D':
                ordenarVector();
                break;
            case 'E':
                System.out.println();
                System.out.println("Goodbye!!");
                System.exit(0);
                break;
            default:
                System.out.println("Fatal error");
                System.exit(0);
                break;
        }
        System.out.println();
        menu();
    }
    public static void introduceData(){
        int idNumber;
        String name;
        String townName;
        Calendar born;
        int year;
        int month;
        int day;
        String fecNac;
        for (int i = 0; i < oldMan.length; i++) {
            System.out.println("Introduce el id del habitante " + (i + 1));
            idNumber = uin.nextInt();
            System.out.println("Introduce el nombre del habitante " + (i + 1));
            name = uin.next();
            System.out.println("Introduce el nombre del pueblo o ciudad del habitante " + (i + 1));
            townName = uin.next();
            System.out.println("Introduce la fecha de nacimiento con el formato dd/mm/aaaa");
            fecNac = uin.next();
            String[] fec = fecNac.split("/");
            day = Integer.parseInt(fec[0]);
            month = Integer.parseInt(fec[1]);
            year = Integer.parseInt(fec[2]);
            born = new GregorianCalendar(year, month, day);
            oldMan[i] = new Inhabitant(idNumber, name, townName, born);
            System.out.println();
        }
    }
    public static void verHombreMasViejo(){
        Calendar olderMan;
        olderMan = new GregorianCalendar();
        Inhabitant olderMen = null;
        if(oldMan != null){
            for(Inhabitant older : oldMan){
                if(olderMan.compareTo(older.getBorn()) > 0){
                    olderMan = older.getBorn();
                    olderMen = older;
                }
            }
            System.out.println(olderMen);
        }else{
            System.out.println("No existe ninguna persona. Rellena los datos primero");
        }
    }
    public static void ordenarVector(){
        Inhabitant temp = null;
        for (int i = 1; i < (oldMan.length - 1); i++) {
            for (int j = 0; j < i; j++) {
                if(oldMan[j].getBorn().compareTo(oldMan[(j + 1)].getBorn()) > 0){
                    temp = oldMan[j];
                    oldMan[j] = oldMan[(j + 1)];
                    oldMan[(j + 1)] = temp;
                }
            }
        }
    }
    public static void verVector(){
        for(Inhabitant hab : oldMan){
            System.out.println(hab);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        inicialiceArray();
        menu();
    }
    
}
