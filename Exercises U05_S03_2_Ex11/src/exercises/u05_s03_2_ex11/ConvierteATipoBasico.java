/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_2_ex11;

/**
 *
 * @author Mati
 */
public class ConvierteATipoBasico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int entero = new Integer("23"); //Convierte la String “23" a int
        long entLong = new Long("34");
        float realFloat = new Float("45.6");
        double realDouble = new Double("56.78e3");
        //Imprime :23:34:45.6:56780.0
        System.out.println(":"+entero+":"+entLong+":"+realFloat+":"+realDouble);
    }
    
}
