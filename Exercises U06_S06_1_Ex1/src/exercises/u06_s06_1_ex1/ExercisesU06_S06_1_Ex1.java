/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u06_s06_1_ex1;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU06_S06_1_Ex1 {
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        char[] letra = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
        int IDnumber,i;
        System.out.println("Introduce el numero del dni");
        do{
            IDnumber = uin.nextInt();
            if(IDnumber <= 0 || IDnumber > 99999999){
                System.out.println("El valor introducido es incorrecto");
            }
        }while(IDnumber <= 0 || IDnumber > 99999999);
        i = IDnumber % 23;
        System.out.println("La letra es " + letra[i]);
    }
    
}
