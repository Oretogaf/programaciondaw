/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s03_2_ex16;

/**
 *
 * @author Mati
 */
public class Almacen {
    private static Object[] almacen = new Object[10];
    
    public static void guardar(Object objeto){
        for (int i = 0; i < almacen.length; i++) {
            if (almacen[i] == null){
                almacen[i] = objeto;
            }
        }
    }
    public static Object sacar(int indiceArray){
        
        return almacen[indiceArray];
    }
    public static void listar(){
        for (Object almacen1 : almacen) {
            if (almacen1 != null){
                almacen1.toString();
            }
        }
    }
    public static int dameNumeroObjetosAlmacenados(){
        int i;
        i = 0;
        for (Object almacen1 : almacen) {
            if (almacen1 != null){
                i++;
            }
        }
        return i;
    }
    public static int dameNumeroHuecos(){
        int i;
        i = 0;
        for (Object almacen1 : almacen) {
            if(almacen1 == null){
                i++;
            }
        }
        return i;
    }
    public static boolean estaVacio(int indice){
        return almacen[indice] == null;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
