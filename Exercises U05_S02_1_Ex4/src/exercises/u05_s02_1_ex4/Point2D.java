/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s02_1_ex4;

/**
 *
 * @author Mati
 */
public class Point2D {
    int pointX;
    int pointY;
    public Point2D(){
        pointX=100;
        pointY=100;
    }
    public Point2D(int coorX, int coorY){
        pointX=coorX;
        pointY=coorY;
    }
    @Override
    public String toString(){
        return "("+pointX+","+pointY+")";
    }
    public boolean equals(Point2D otroPunto){
        return ((this.pointX==otroPunto.pointX)&&(this.pointY==otroPunto.pointY));
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Point2D obj1 = new Point2D();
        System.out.println(obj1.toString());
        Point2D obj2 = new Point2D(110, 150);
        System.out.println(obj2.toString());
        System.out.println("Are equals: "+obj1.equals(obj2));
    }
    
}
