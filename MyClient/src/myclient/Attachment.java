package myclient;

import java.nio.ByteBuffer;
import java.nio.channels.*;

/**
 *
 * @author Rubén López Valero
 */
class Attachment {
    AsynchronousSocketChannel channel;
    ByteBuffer buffer;
    Thread mainThread;
    boolean isRead;
}
