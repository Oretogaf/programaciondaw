/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u04_s03_ex5;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ConverterLongitud {
    private double converterMillesToMeters;
    public ConverterLongitud(){
        converterMillesToMeters=1.852;
    }
    public double millesToMeters(double milles){
        return milles*converterMillesToMeters;
    }
    public double metersToMilles(double meters){
        return meters/converterMillesToMeters;
    }
    public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double milles;
        double meters;
        ConverterLongitud converter1 = new ConverterLongitud();
        System.out.println("Introduce millas para convertir");
        milles=uin.nextDouble();
        System.out.println(converter1.millesToMeters(milles));
        System.out.println("Introduce metros para convertir");
        meters=uin.nextDouble();
        System.out.println(converter1.metersToMilles(meters));
    }
    
}
