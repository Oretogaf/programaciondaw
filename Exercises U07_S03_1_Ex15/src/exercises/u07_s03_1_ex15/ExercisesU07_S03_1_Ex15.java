/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u07_s03_1_ex15;

/**
 *
 * @author Mati
 */
public class ExercisesU07_S03_1_Ex15 {
    private static final double factor = 166.386;
    public static double convertirEuros(double euros){
        return euros * factor;
    }
    public static double convertirPesetas(double pesetas){
        return pesetas / factor;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(convertirEuros(1));
        System.out.println(convertirPesetas(166.386));
        System.out.println(convertirPesetas(convertirEuros(100)));
    }
    
}
