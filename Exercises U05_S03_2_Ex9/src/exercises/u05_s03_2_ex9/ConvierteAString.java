/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u05_s03_2_ex9;

/**
 *
 * @author Mati
 */
public class ConvierteAString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Obtén el valor int del objeto unNumero
        int unNumero = new Integer(32);
        Integer.toString(unNumero);
        //Obtén el valor long del objeto unLong
        long unLargo = new Long(45);
        Long.toString(unLargo);
        //Imprime 90 String
        System.out.println(unLargo*2);
        String pi2 = new String("3.1416");
        //Obtén el valor float del objeto unReal
        float unReal = new Float(pi2);
        //Obtén el valor boolean del objeto unBoolean
        boolean unBoolean = new Boolean(true);
        //imprimirá true
        if (unBoolean)
            System.out.println(unBoolean);
        else
            System.out.println("FALSE");   
    }
    
}
