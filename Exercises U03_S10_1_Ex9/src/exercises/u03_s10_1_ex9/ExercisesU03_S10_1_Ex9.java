/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises.u03_s10_1_ex9;

import java.util.Scanner;

/**
 *
 * @author Mati
 */
public class ExercisesU03_S10_1_Ex9 {
public static Scanner uin = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        char u;
        char a='a';
        System.out.println("What is the command keyword to exit a loop in Java?");
        System.out.println("a. int");
        System.out.println("b. continue");
        System.out.println("c. break");
        System.out.println("d. exit");
        do{
            System.out.print("Enter your choice: ");
            u=uin.next().charAt(0);
            if(u=='c'){
                System.out.println("Correct!");
            }else{
                System.out.println("Incorrect!");
                System.out.print("Again? press y to continue:");
                a=uin.next().charAt(0);
            }
        }while((u!='c')&&(a=='y'));
    }
    
}
